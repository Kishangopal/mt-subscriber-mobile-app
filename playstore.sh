#/bin/sh

rm -rf releases/*.apk 
cp -rf android/app/build/outputs/apk/release/app-*.apk ./releases/
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./my-app-key.keystore releases/app-x86_64-release.apk alias_name
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./my-app-key.keystore releases/app-arm64-v8a-release.apk alias_name
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./my-app-key.keystore releases/app-x86-release.apk alias_name
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./my-app-key.keystore releases/app-armeabi-v7a-release.apk alias_name



/Users/rajesh/Library/Android/sdk/build-tools/30.0.1/zipalign -v 4 releases/app-x86_64-release.apk releases/mt-signed-x86_64-1.0.0.prod-gps.apk
/Users/rajesh/Library/Android/sdk/build-tools/30.0.1/zipalign -v 4 releases/app-arm64-v8a-release.apk releases/mt-signed-arm64-v8a-1.0.0.prod-gps.apk
/Users/rajesh/Library/Android/sdk/build-tools/30.0.1/zipalign -v 4 releases/app-x86-release.apk releases/mt-signed-x86-1.0.0.prod-gps.apk
/Users/rajesh/Library/Android/sdk/build-tools/30.0.1/zipalign -v 4 releases/app-armeabi-v7a-release.apk releases/mt-signed-armeabi-v7a-1.0.0.prod-gps.apk