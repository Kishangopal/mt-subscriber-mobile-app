import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AppRoute} from './app-routes';
import {ChangeYourPassword, OtpVerification, ResetPasswordScreen, SignInScreen, SignUpScreen,TermsAndConditions,OtpVerifyPassword} from '../screens/auth';
const AuthStack = createStackNavigator();
export const AuthNavigator = () => {
    return  (
        <AuthStack.Navigator headerMode='none'>
            <AuthStack.Screen name={AppRoute.SIGN_IN} component={SignInScreen}/>
            <AuthStack.Screen name={AppRoute.SIGN_UP} component={SignUpScreen}/>
            <AuthStack.Screen name={AppRoute.RESET_PASSWORD} component={ResetPasswordScreen}/>
            <AuthStack.Screen name={AppRoute.OTP_VERIFICITION} component={OtpVerification}/>
            <AuthStack.Screen name={AppRoute.OTP_VERIFY_PASSWORD} component={OtpVerifyPassword}/>
            <AuthStack.Screen name={AppRoute.CHANGE_YOUR_PASSWORD} component={ChangeYourPassword}/>
            <AuthStack.Screen name={AppRoute.TERMS_AND_CONDITIONS} component={TermsAndConditions}/>
        </AuthStack.Navigator>
    );
};
