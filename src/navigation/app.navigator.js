import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AuthNavigator } from './auth.navigator';
import { ProfileScreen } from '../screens/profile';
import { UpdateProfile } from '../screens/profile/update-profile';
import { ChangePassword } from '../screens/profile/change-password';
import { UpdateApp } from '../screens/home/update-app';
import { HomeScreen } from '../screens/home/home';
import { Help } from '../screens/home/help';
import { Faq } from '../screens/home/faq';
import { TermsConditions } from '../screens/home/terms-conditions';
import { PrivacyPolicy } from '../screens/home/Privacy-policy';
import { DetailsScreen } from '../screens/details';
import {
  EditVehicleDetails,
  FindVehicleDetails,
  HourlyReports,
  LiveVehicle,
  LiveVehiclelist,
  MovementsReports,
  NearestVehicle,
  RateCard,
  SpeedReports,
  StoppageReport,
  VehicleList,
  VehicleOnMapScreen,
  VehicleRegister,
  VehicleReports,
  VehicleSearchScreen,
  NeedLoad,
  NeedLoadList,
  VehicleTypeSearch
} from '../screens/vehicle'
import { BusinessProfile, PackersAndMovers, Transporter } from '../screens/listing'
import { Kyc, Pricing } from '../screens/subscription'
import { PurchaseHistory, Transactions } from '../screens/transactions'
import { EditPostLoad, MemberPostList, PostLoad, PostLoadList, PostLoadView } from '../screens/load'
import { AppRoute } from './app-routes';
import { Notification } from '../screens/notificaiton';
import { Icon } from 'react-native-elements'
import Config from '../constants/config'
import { DrawerMenu } from '../screens/drawer-menu'
import Toast from 'react-native-toast-message';

const AppStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const defaultStackNavOptions = (props, headerTitle) => {
  return {
    headerStyle: {
      backgroundColor: Config.topMenuHeaderColor
    },
    headerTitle,
    headerTitleStyle: {
      fontFamily: 'montserrat-bold'
    },
    headerBackTitleStyle: {
      fontFamily: 'montserrat'
    },
    headerTintColor: 'white',
    headerLeft: () => (
      <Icon type="ionicon" name={'ios-menu'} iconStyle={{ padding: 10 }} size={28} color="white" onPress={() => props.navigation.toggleDrawer()} />
    ),
    headerRight: () => (
      <Icon type="ionicon" name="ios-notifications-outline" iconStyle={{ padding: 10 }} size={28} color="white" onPress={() => props.navigation.push(AppRoute.NOTIFICATION)} />
    )
  }
};
const DrawerScreen = (props) => (
  <Drawer.Navigator
    initialRouteName="Home"
    drawerContent={(props) => <DrawerMenu {...props} />}>
    <Drawer.Screen name={AppRoute.HOME} component={HomeStackNavigator} />
    <Drawer.Screen name="Reports" component={ReportsStackNavigator} />
    <Drawer.Screen name="Vehicle" component={VehicleStackNavigator} />
    <Drawer.Screen name="Pricing" component={SubscriptionStackNavigator} />
  </Drawer.Navigator>
);
const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator screenOptions={({ route }) => ({
    tabBarIcon: ({ focused, color, size }) => {
      let iconName;
      if (route.name === 'Home') {
        iconName = focused
          ? 'ios-home'
          : 'ios-home';
      } else if (route.name === 'Vehicle') {
        iconName = focused ? 'md-bus' : 'md-bus';
      } else if (route.name === 'Reports') {
        iconName = focused ? 'list-outline' : 'list-outline';
      }

      // You can return any component that you like here!
      return <Icon type="ionicon" name={iconName} size={size} color={color} />;
    },
  })}
    tabBarOptions={{
      activeTintColor: Config.accentColor,
      inactiveTintColor: 'gray',
    }}>
    <Tabs.Screen name="Home" component={HomeStackNavigator} />
    <Tabs.Screen name="Vehicle" component={VehicleStackNavigator} />
    <Tabs.Screen name="Reports" component={ReportsStackNavigator} />

  </Tabs.Navigator>
);

const VehicleStack = createStackNavigator();
const VehicleStackNavigator = (props) => (
  <VehicleStack.Navigator>
    <VehicleStack.Screen
      name={AppRoute.LIVE_VEHICLE_LIST}
      component={LiveVehiclelist}
      options={defaultStackNavOptions(props, 'Live Vehicle list')}
    />
    <VehicleStack.Screen name={AppRoute.FIND_VEHICLE} component={VehicleSearchScreen} />
    <VehicleStack.Screen name={AppRoute.VEHICLE_REGISTER} component={VehicleRegister} />
    <VehicleStack.Screen name={AppRoute.EDIT_VEHICLE_REGISTER} component={EditVehicleDetails} />
    <VehicleStack.Screen name={AppRoute.VEHICLE_LIST} component={VehicleList} />
    <VehicleStack.Screen name={AppRoute.VEHICLE_TYPE_LIST} component={VehicleTypeSearch} />
    <VehicleStack.Screen name={AppRoute.POST_LAOD} component={PostLoad} />
    <VehicleStack.Screen name={AppRoute.POST_LOAD_LIST} component={PostLoadList} />
    <VehicleStack.Screen name={AppRoute.POST_LOAD_VIEW} component={PostLoadView} />
    <VehicleStack.Screen name={AppRoute.MEMBER_POST_LAOD} component={MemberPostList} />
    <VehicleStack.Screen name={AppRoute.EDIT_POST_LAOD} component={EditPostLoad} />
    <VehicleStack.Screen name={AppRoute.FIND_VEHICLE_DETAILS} component={FindVehicleDetails} />
    <VehicleStack.Screen name={AppRoute.RATECARD} component={RateCard} />
    <VehicleStack.Screen name={AppRoute.VEHICLE_ON_MAP} component={VehicleOnMapScreen} />
    <VehicleStack.Screen name={AppRoute.LIVE_VEHICLE} component={LiveVehicle} />
    <VehicleStack.Screen name={AppRoute.NEAREST_VEHICLE} component={NearestVehicle} />
    <VehicleStack.Screen name={AppRoute.RATECARD} component={RateCard} />
    <VehicleStack.Screen name={AppRoute.TRANSPORTER}
      component={Transporter} />
  </VehicleStack.Navigator>
);
const SubscriptionStack = createStackNavigator();
const SubscriptionStackNavigator = (props) => (
  <SubscriptionStack.Navigator>
    <SubscriptionStack.Screen
      name={AppRoute.PRICING}
      component={Pricing}
      options={defaultStackNavOptions(props, 'Pricing')}
    />
    <SubscriptionStack.Screen name={AppRoute.KYC}
      component={Kyc} />
    <SubscriptionStack.Screen name={AppRoute.TRANSACTIONS}
      component={Transactions} />
  </SubscriptionStack.Navigator>
);
const ReportsStack = createStackNavigator();
const ReportsStackNavigator = (props) => (
  <ReportsStack.Navigator>
    <ReportsStack.Screen
      name="Reports"
      component={VehicleReports}
      options={defaultStackNavOptions(props, 'Vehicle Reports')}
    />
    <ReportsStack.Screen name={AppRoute.VEHICLE_REPORTS} component={VehicleReports} />
    <ReportsStack.Screen name={AppRoute.HOURLY_REPORTS} component={HourlyReports} />
    <ReportsStack.Screen name={AppRoute.STOPPAGE_REPORT} component={StoppageReport} />
    <ReportsStack.Screen name={AppRoute.SPEED_REPORTS} component={SpeedReports} />
    <ReportsStack.Screen name={AppRoute.MOVEMENTS_REPORTS} component={MovementsReports} />
  </ReportsStack.Navigator>
);
const HomeStack = createStackNavigator();
const HomeStackNavigator = (props) => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name="Home"
      options={defaultStackNavOptions(props, 'Member Dashboard')}
      component={HomeScreen}
    />
    <HomeStack.Screen
      name="Details"
      component={DetailsScreen}
      options={({ route }) => defaultStackNavOptions(props, route.params.name)}
    />
    <HomeStack.Screen name={AppRoute.POST_LAOD}
      component={PostLoad} />
    <HomeStack.Screen name={AppRoute.PROFILE}
      component={ProfileScreen} />
    <HomeStack.Screen name={AppRoute.UPDATE_PROFILE}
      component={UpdateProfile} />
    <HomeStack.Screen name={AppRoute.CHANGE_PASSWORD}
      component={ChangePassword} />
    <HomeStack.Screen name={AppRoute.PACKERS_AND_MOVERS}
      component={PackersAndMovers} />
    <HomeStack.Screen name={AppRoute.TRANSPORTER}
      component={Transporter} />
    <HomeStack.Screen name={AppRoute.PURCHASE_HISTORY}
      component={PurchaseHistory} />
    <HomeStack.Screen name={AppRoute.NOTIFICATION}
      component={Notification} />
    <HomeStack.Screen name={AppRoute.BUSINESS_PROFILE}
      component={BusinessProfile} />
    <HomeStack.Screen name={AppRoute.FAQ}
      component={Faq} />
    <HomeStack.Screen name={AppRoute.PRIVACY_POLICY}
      component={PrivacyPolicy} />
    <HomeStack.Screen name={AppRoute.MEMBER_POST_LAOD}
      component={MemberPostList} />
    <HomeStack.Screen name={AppRoute.HELP}
      component={Help} />
    <HomeStack.Screen name={AppRoute.NEED_LOAD} component={NeedLoad} />
    <HomeStack.Screen name={AppRoute.NEED_LOAD_LIST} component={NeedLoadList} />
    <HomeStack.Screen name={AppRoute.POST_LOAD_LIST} component={PostLoadList} />
    <HomeStack.Screen name={AppRoute.POST_LOAD_VIEW} component={PostLoadView} />
    <HomeStack.Screen name={AppRoute.TERMS_CONDITIONS} component={TermsConditions} />
    <HomeStack.Screen name={AppRoute.UPDATE_APP} component={UpdateApp} />
    <HomeStack.Screen name={AppRoute.RATECARD} component={RateCard} />
  </HomeStack.Navigator>
);
//
export const AppNavigator = ({ userToken }) => (
  <>
    <AppStack.Navigator headerMode="none">
      {userToken ? (
        <AppStack.Screen
          name="App"
          component={DrawerScreen}
          options={{
            animationEnabled: false
          }}
        />
      ) : (
        <AppStack.Screen
          name="Auth"
          component={AuthNavigator}
          options={{
            animationEnabled: false
          }}
        />
      )}
    </AppStack.Navigator>
    <Toast ref={(ref) => Toast.setRef(ref)} />
  </>
);