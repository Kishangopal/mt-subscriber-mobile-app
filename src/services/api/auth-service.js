import apiKit from "./axios-base";
import {removeLocalData, storeLocalData} from '../global-storage'

const login = (username, password) => {
  return new Promise((resolve, reject) => {
    apiKit
      .post("/MtUsers/login?include=user", { username, password })
      .then(function (response) {
        storeLocalData("accessToken", response.data.id);
        storeLocalData("userId", response.data.userId);
        storeLocalData("userObject", response.data.user);
        console.log(response.data)
        resolve(response.data.id);
        
      })
      .catch(function (error) {
        console.error(`Error:${error}`)
        reject(error);
      });
  })
}
const signup = (contactName,contactNumber,password,email,username,isTermsAgreed,userType,state,businessTypeTag,referrald) => {
  const data = {contactName,contactNumber,password,email,username,isTermsAgreed,userType,state,businessTypeTag,referrald};
  //console.log(data)
  return new Promise((resolve, reject) => {
    apiKit.post("/MtUsers", data)
    .then(function (response) {
      resolve(response.data);
      self.props.history.push("/login");
    })
    .catch(function (error) {
      console.error(`Error:${JSON.stringify(error)}`)
      reject(error);
    });
  })
}
const logout = () => {
  console.log("logout");
  removeLocalData("accessToken");
}
export const authenticationService = {
  login,
  signup,
  logout
};