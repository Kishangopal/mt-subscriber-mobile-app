import { create, find, upsertPatch, findOne } from './core-service'
import apiKit, { axiosRequest } from "./axios-base";
import { filter } from 'lodash-es';
import { getLocalData } from '../global-storage';
import _ from 'lodash';
export const getSystemList = (type) => {
    return new Promise((resolve, reject) => {
        find('MtSystemLists', { "where": { "listType": type }, order: "label asc" })
            .then(data => {
                // storeCachedData(`${type}List`, data)
                resolve(data);
            })
    })
}
export const getPublicList = (type) => {
    return new Promise((resolve, reject) => {
        find('MtPublicLists', { "where": { "listType": type }, order: "label asc" })
            .then(data => {
                // storeCachedData(`${type}List`, data)
                resolve(data);
            })
    })
}
export const createBusinessListing = (data) => {
    return create('MtBusinessProfiles', data)
}
export const createLoadPost = (data) => {
    return create('MtVehicleLoads', data)
}
export const getLoadPostList = (location, mtUserId) => {
    const filter = { where: {}, order: "id desc" };
    if (location) {
        filter.where.or = [{ pickupLocation: location }, { destination: location }];
    }
    if (mtUserId) filter.where = [{ mtUserId: mtUserId }];
    return find('MtVehicleLoads', filter)
}
export const getUserGetLoadPostList = (mtUserId) => {
    return find('MtVehicleLoads', { "where": { "mtUserId": mtUserId }, order: "id desc" })
}
export const getBusinessList = (type, city) => {
    return find('MtBusinessProfiles', { "where": { "businessTypeTag": type, "city": city }, order: "companyName asc" })
}
export const getBusinessListState = (type, state) => {
    return find('MtBusinessProfiles', { "where": { "businessTypeTag": type, "state": state }, order: "companyName asc" })
}
export const getRateCardList = (Type) => {
    return find('MtRateCards', { "where": { "vehicleType": Type } })
}
export const getPricingList = (status) => {
    return find('MtSubscriptionPlans', { "where": { "status": status } })
}
export const getVehicleList = (mtUserId) => {
    return find('MtVehicles', { "where": { "mtUserId": mtUserId } })
}
export const getVehicleDetailsList = (vehicleNo) => {
    return find('MtVehicles', { "where": { "vehicleNumber": vehicleNo } })
}
export const getBusinessProfileView = (mtUserid) => {
    return find('MtBusinessProfiles', { "where": { "mtUserId": mtUserid } })
}
export const getBusinessProfileViewListing = (id) => {
    return find('MtBusinessProfiles', { "where": { "id": id } })
}
export const UpdateLoadPost = (data) => {
    return upsertPatch('MtVehicleLoads', data)
}
export const CreateVehicleRegister = (data) => {
    return create('MtVehicles', data)
}
export const UpdateVehicleRegister = (data) => {
    return upsertPatch('MtVehicles', data)
}
export const CreateKycRegister = (data) => {
    return create('MtKYCs', data)
}
export const UpdateKycRegister = (data) => {
    return upsertPatch('MtKYCs', data)
}
export const getVehicleLocations = (filter, vehicleNumber) => {
    return axiosRequest(
        'GET',
        `${["MtVehicles", 'findAllDevices'].join('/')}`,
        undefined,
        undefined,
        { filter, vehicleNumber },
        true
    )
}
export const getVehicleLocationStats = () => {
    return axiosRequest(
        'GET',
        ["MtVehicles", 'findAllDeviceStats'].join('/')
    )
}
export const changeUserPassword = (payload) => {
    return axiosRequest(
        'POST',
        `${["MtUsers", 'change-password'].join('/')}`,
        undefined,
        payload,
        undefined,
        true
    )
}
export const getVehicleActiveList = () => {
    return axiosRequest(
        'GET',
        ["MtVehicles", 'allActiveVehicleList'].join('/')
    )
}
export const getHourlyStats = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'hourlyStats'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const getHourlyDetail = (vehicleNumber, dateRange) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'hourlyDetail'].join('/'),
        undefined,
        undefined,
        { vehicleNumber, dateRange },
        true
    )
}
export const getAvgSpeedStats = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'avgSpeedStats'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const getAvgSpeedDetail = (vehicleNumber, dateRange) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'avgSpeedDetail'].join('/'),
        undefined,
        undefined,
        { vehicleNumber, dateRange },
        true
    )
}
export const getStoppageStats = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'stoppageStats'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const getStoppageDetail = (vehicleNumber, dateRange) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'stoppageDetail'].join('/'),
        undefined,
        undefined,
        { vehicleNumber, dateRange },
        true
    )
}
export const getMovementStats = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'movementStats'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const getMovementDetail = (vehicleNumber, dateRange) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'movementDetail'].join('/'),
        undefined,
        undefined,
        { vehicleNumber, dateRange },
        true
    )
}
export const getNotificationList = (status) => {
    return find('MtNotifications', { "where": { "status": status } })
}
export const getVehicleSummary = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'vehicleSummary'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const getTravelStats = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'travelStats'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const getIgnitionAlert = (vehicleNumber) => {
    return axiosRequest(
        'GET',
        ["MtDeviceData", 'ignitionAlert'].join('/'),
        undefined,
        undefined,
        { vehicleNumber },
        true
    )
}
export const sendOtp = (handle, mode) => {
    return axiosRequest(
        'POST',
        ["MtUsers", 'sendOTP'].join('/'),
        undefined,
        undefined,
        { handle, mode },
        true
    )
}
export const verifyOtp = (phone, otp) => {
    return axiosRequest(
        'POST',
        ["MtUsers", 'verifyOTP'].join('/'),
        undefined,
        undefined,
        { phone, otp },
        true
    )
}
export const resetUserPassword = (handle, otp, newPassword) => {
    return axiosRequest(
        'POST',
        ["MtUsers", 'resetUserPassword'].join('/'),
        undefined,
        undefined,
        { handle, otp, newPassword },
        true
    )
}
export const getNearbyVehicle = (vehicleNumber, distance) => {
    return axiosRequest(
        'GET',
        ["MtVehicles", 'nearbyVehicle'].join('/'),
        undefined,
        undefined,
        { vehicleNumber, distance },
        true
    )
}
export const getFaq = (type) => {
    return find('MtFaqs', { "where": { "faqType": type } })
}
export const getUniqueLocations = (businessType, state) => {
    return axiosRequest(
        'GET',
        ["MtBusinessProfiles", 'getUniqueLocations'].join('/'),
        undefined,
        undefined,
        { businessType, state },
        true
    )
}
export const getUser = (userid) => {
    return find('MtUsers', { "where": { "id": userid } })
}
export const createNeedLoad = (data) => {
    return create('MtNeedLoads', data)
}
export const getSystemListType = (type) => {
    return find('MtSystemLists', { "where": { "listType": type }, order: "orderValue asc" })
}
export const getLoadPostListFilter = (location) => {
    return find('MtVehicleLoads', { "where": { "pickupLocation": location }, order: "id asc" })
}
export const getPostLoad = (postId) => {
    return findOne('MtVehicleLoads', { "where": { "id": postId } })
}
export const getNeedLoadList = (Location) => {

    return Location == "All" ? find('MtNeedLoads', { order: "id desc" }) : find('MtNeedLoads', { "where": { "fromLocation": Location }, order: "id desc" })
}
export const getSystemListCityLabel = (dataValue) => {
    return find('MtSystemLists', { "where": { "value": dataValue } })
}
export const getSystemListCity = (state) => {
    return find('MtSystemLists', { "where": { "state": state }, order: "id asc" })
}
export const createHelpDesks = (data) => {
    return create('MtHelpDesks', data)
}
export const UpdateUser = (id, data) => {
    return upsertPatch(`MtUsers/${id}`, data)
}
export const uploadFile = (fileData, bucketName, folder = '') => {
    let formData = new FormData();
    let _fileData = fileData.assets[0];
    // Update the formData object 
    const today = new Date();
    const datestring = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    formData.append(
        "myFile",
        {
            uri: _fileData.uri,
            name: `${folder}/${_fileData.fileName}`,
            type: 'image/jpeg'
        }
    );
    return apiKit.post(`/AWSContainers/${bucketName}/upload?access_token=${getLocalData('accessToken')}`, formData)
}
export const UpdateBusinessProfile= (id,data) => {
    return upsertPatch(`MtBusinessProfiles/${id}`, data)
}
export const getAllVehicle = () => {
    const filter = { where: {},order: "id desc" };
    return find('MtVehicles', filter)
}
export const findRateCard = (locationValue,vehicleTypeValue) => {
    return find('MtRateCards', { "where": { "location": locationValue,"vehicleType":vehicleTypeValue }, order: "id asc" })
}
export const getDestance = (fromCityId, toCityId) => {
    return axiosRequest(
        'GET',
        ["MtRateCards", 'findRoadDistance'].join('/'),
        undefined,
        undefined,
        { fromCityId, toCityId },
        true
    )
}