import {store} from '../store/store';
import _ from 'lodash';
const removeItem = key => {
  // SyncStorage.remove(key);
};
const hasUserAlreadyLoggedIn = () => {
  //FixMe#1: accessToken should be checked for login state
  return !_.isEmpty(getLocalData('accessToken'));
};
const getUserData = (arr, key) => {
  let userData;
  if (!_.isEmpty(arr) && _.size(arr))
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].key == key) {
        userData = arr[i];
      }
    }
  return userData ? userData.data : false;
};

const storeLocalData = (key, value) => {
  let keyValObject = {};
  keyValObject[`${key}`] = value;
  store.dispatch({type: 'STORE_LOCAL_DATA', localData: keyValObject});
};

const removeLocalData = (key) => {
  store.dispatch({type: 'REMOVE_LOCAL_DATA', key: key});
};
const clearLocalData = (key) => {
  store.dispatch({type: 'REMOVE_LOCAL_DATA', key: key});
};
const getLocalData = (key) => {
  return _.get(store.getState(), ['localData', key]);
};
const getTenantConfig = (key) => {
  return _.get(store.getState(), 'tenantConfig', {});
};
const getAccessToken = async () => {
  return await _.get(store.getState(), ['localData', 'accessToken']);
};
const storeCachedData = (key, value) => {
  let keyValObject = {};
  keyValObject[`${key}`] = value;
  store.dispatch({type: 'LOAD_CACHE_DATA', cacheData: keyValObject});
};

const removeCachedData = (key) => {
  store.dispatch({type: 'REMOVE_CACHED_DATA', key: key});
};

const clearCachedData = (key) => {
  store.dispatch({type: 'CLEAR_CACHED_DATA', key: key});
};

const getCachedData = (key) => {
  return _.get(store.getState(), ['cacheData', key]);
};

export {
  removeItem,
  hasUserAlreadyLoggedIn,
  getUserData,
  clearLocalData,
  storeLocalData,
  getLocalData,
  removeLocalData,
  getAccessToken,
  getTenantConfig,
  storeCachedData,
  removeCachedData,
  getCachedData,
  clearCachedData
};
