import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, Linking, View } from 'react-native';
import { GlobalStyles, MemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import _ from 'lodash'
import { Image,Icon } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { MtActivityIndicator, MtBodyText, MtIconLinkButton, MtLink } from '../../components/ui-elements';
import {getBusinessProfileViewListing } from '../../services/api/api-service';
export const BusinessProfile = ({ route }) => {
  const [buinessProfileView, setbuinessProfileView] = useState([]);
  const [profileStatus, setProfileStatus] = useState(true);
  const visitingCard = "https://moverstrip-prod.s3.us-east-1.amazonaws.com/business-cards/defulte-visiting-card.png";
  const bId = route.params.bId;
  useEffect(() => {
    Toast.show({
      text1: 'Business Profile',
      text2: 'View Business Profile Details'
    });
    getBusinessProfileViewListing(bId).then(data => {
      if (!_.isEmpty(data)) {
        setbuinessProfileView(data)
        console.log(data);
        setProfileStatus(false)
      }
    }).catch(e => {
      console.log("Error in Transporter list:", e);
    })
  }, [])
  return (
    <>
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
        {!profileStatus &&
          <View style={{...MemberStyles.meanBox,...styles.buttomMargin}}>
            <MtBodyText style={MemberStyles.headingTitleStyle}>{buinessProfileView[0].businessTypeTag}</MtBodyText>
            <View style={MemberStyles.rowflex}>
              {!_.isEmpty(buinessProfileView[0]) &&
                <Image
                  source={{ uri: _.get(buinessProfileView[0], "businessCard") ? buinessProfileView[0].businessCard : visitingCard }}
                  style={{ height: 150, width: Dimensions.get('window').width - 20, resizeMode: 'stretch' }}
                />
              }
              {_.isEmpty(buinessProfileView[0]) &&
                <MtActivityIndicator />
              }
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Company Name:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{buinessProfileView[0].companyName}</MtBodyText>
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Contact Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{buinessProfileView[0].contactNumber}</MtBodyText>
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>E-Mail:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('mailto:' + buinessProfileView[0].businessEmail); }}>{buinessProfileView[0].businessEmail.length > 0 ? buinessProfileView[0].businessEmail : "N/A"}</MtLink >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Website:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('http://' + _.get(buinessProfileView[0], "website") ? buinessProfileView[0].website : ""); }}>{_.get(buinessProfileView[0], "website") ? buinessProfileView[0].website : "N/A"}</MtLink >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Owner Name:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtLink style={MemberStyles.userSubText}>{buinessProfileView[0].ownerName.length > 0 ? buinessProfileView[0].ownerName : "N/A"}</MtLink >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Owner Phone:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('tel:' + `${_.get(buinessProfileView[0], "ownerPhone") ? buinessProfileView[0].ownerPhone : ""}`); }}>{_.get(buinessProfileView[0], "ownerPhone") ? buinessProfileView[0].ownerPhone : "N/A"}</MtLink >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Address:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{buinessProfileView[0].address},{buinessProfileView[0].city},{buinessProfileView[0].state} {buinessProfileView[0].pin}</MtBodyText >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Vehicle Available For:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{_.get(buinessProfileView[0], "routeVehicleLoaded") ? buinessProfileView[0].routeVehicleLoaded : "N/A"}</MtBodyText >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Special Services:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{_.get(buinessProfileView[0], "specialServices") ? buinessProfileView[0].specialServices : "N/A"}</MtBodyText >
              </View>
            </View>
            <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>GST No.:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{_.get(buinessProfileView[0], "GSTNo") ? buinessProfileView[0].GSTNo : "N/A"}</MtBodyText >
              </View>
            </View>

          </View>

        }
        {profileStatus &&
          <MtActivityIndicator />
        }
      </ScreenLayout>
      <View style={MemberStyles.FixFooterBox}>
        <View style={MemberStyles.rowView}>
        <MtIconLinkButton
          onPressHandler={() => { Linking.openURL('tel:' + buinessProfileView[0].contactNumber); }}
          buttonIcon={<Icon type="fontawesome" name="phone" size={30} style={{ padding: 6, }} color="#fff" />}
          buttonLabel="Call now"
          bgColor={['#cc2c04', '#cc1b04']}
        />
        <MtIconLinkButton
          onPressHandler={() => { Linking.openURL('https://wa.me/91' + buinessProfileView[0].contactNumber); }}
          buttonIcon={<Icon type="font-awesome-5" name="whatsapp" size={30} style={{ padding: 6, }} color="#fff" />}
          buttonLabel="WhatsApp"
          bgColor={['#239929', '#0e5e12']}
        />
        </View>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  buttomMargin: {
    marginBottom:100,
  }
});