import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import { DefaultScreen } from '../vehicle/vehicleForm';
import { GlobalStyles, MemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import { Icon } from 'react-native-elements'
import _ from 'lodash'
import { MtBodyText, MtDropDown, MtActivityIndicator } from '../../components/ui-elements';
import { getBusinessList, getUniqueLocations } from '../../services/api/api-service';
export const PackersAndMovers = (props) => {
  const businessType = "Packers And Movers";
  const [businessProfileList, setBusinessProfileList] = useState([]);
  const [stateValue, setStateValue] = useState(null);
  const [stateList, setStateList] = useState([]);
  const [cityList, setCityList] = useState([]);
  const [listData, setListData] = useState(true);
  const [showListStatus, setShowListStatus] = useState(false);
  useEffect(() => {
    getUniqueLocations(businessType).then(data => {
      if (!_.isEmpty(data)) {
        setStateList(data.state)
      }
    }).catch(e => {
      console.log("Error in State List:", e);
    })
    if (stateValue) {
      getUniqueLocations(businessType, stateValue).then(data => {
        if (!_.isEmpty(data)) {
          setCityList(data.city);
          setListData(false);
          setShowListStatus(false);
        }
      })
    }
  }, [stateValue])
  const getCityListHandler = (data) => {
    getBusinessList(businessType, data.label).then(data => {
      if (!_.isEmpty(data)) {
        setShowListStatus(true);
        setBusinessProfileList(data)
      }
    })
  }
  return (
    <>
      <View style={MemberStyles.headerReportBox}>
        <View style={MemberStyles.resultBox}>
          <MtBodyText style={MemberStyles.labelHeadtext}>Result {businessProfileList.length} </MtBodyText>
        </View>
        <View style={styles.FilterHeadBox}>
          <View style={styles.rowBox}>
            <MtDropDown
              items={stateList}
              name="state"
              onValueChangeHandler={value => {
                setStateValue(value)
              }}
              selectedvalue={stateValue}
            />
          </View>
        </View>
      </View>
      <ScreenLayout style={MemberStyles.scrollViewBox}>
      {!listData &&
        <>
        {showListStatus == true ? (
          <>
          
              <View style={styles.container}>
                {businessProfileList.map((item, key) => (
                  <TouchableOpacity key={key} style={MemberStyles.cardBox} onPress={() => props.navigation.push(AppRoute.BUSINESS_PROFILE, { bId: item.id })} >
                    <View>
                      <MtBodyText style={MemberStyles.cardTitleStyle}>{item.companyName} {_.get(item, 'specialServices') ? `(${item.specialServices})` : null}</MtBodyText>
                      <View style={MemberStyles.cardbodystyle}>
                        <MtBodyText style={MemberStyles.cardtext}>{item.city},{item.state} {item.pin}</MtBodyText>
                        <MtBodyText style={MemberStyles.cardtext}>{_.get(item, 'routeVehicleLoaded') ? item.routeVehicleLoaded : null}</MtBodyText>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))}
                {!businessProfileList &&
                  <View>
                    <MtActivityIndicator />
                  </View>
                }
              </View>
           
            
          </>
        ) :
          (
            <View style={styles.container}>
              {cityList.map((item, key) => (
                <TouchableOpacity key={key} style={MemberStyles.cityListType} onPress={() => getCityListHandler(item)} >
                  <View>
                    <MtBodyText style={MemberStyles.labelHeadtext}>{item.label}</MtBodyText>
                  </View>
                </TouchableOpacity>
              ))}
            </View>
          )
        }
        </>
        }
        {listData &&
              <View>
                <DefaultScreen
                  imagePath={<Icon type="antdesign" name="barschart" size={60} color="#66ccff" />}
                  headingMessage={`${businessType} List`}
                  subheading="Search your State and City get list" />
              </View>
            }
      </ScreenLayout>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  cardHeadBox:
  {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 1,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
  },
  FilterHeadBox:
  {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 1,
  },
  rowBox: {
    flex: 1,
    padding: 1,
    marginVertical: 10,
  },
});