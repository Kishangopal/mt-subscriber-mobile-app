import React from 'react';
import {View} from 'react-native';
import {MemberStyles} from '../../../styles';
import {EzButton} from "../../../components/ui-elements/EzButton"
import {MtBodyText} from '../../../components/ui-elements';

export const KycStep3 = props => {
    const { values, handleSubmit, back } = props;
    return (<>
        <MtBodyText style={MemberStyles.subHeadingStyle}>Member Kyc Detail</MtBodyText>
        <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Full Name:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{values.fullName}</MtBodyText>
            </View>
        </View>
        <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>State:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{values.state}</MtBodyText>
            </View>
        </View>
        <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>City:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{values.city}</MtBodyText>
            </View>
        </View>
        <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Pin Code:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{values.pinCode}</MtBodyText>
            </View>
        </View>
        <MtBodyText style={MemberStyles.subHeadingStyle}>Document Detail</MtBodyText>

        <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Pin Card Number:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{values.panNumber}</MtBodyText>
            </View>
        </View>
        <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
                <MtBodyText style={MemberStyles.userText}>Aadhar Card Number:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
                <MtBodyText style={MemberStyles.userSubText}>{values.aadharCardNumber}</MtBodyText>
            </View>
        </View>
        <EzButton
            buttonType='outline'
            OnPressHandler={back}
            title='Back'
        />
        <EzButton
            buttonType='outline'
            OnPressHandler={handleSubmit}
            title='Submit'
            buttonColor='#039BE5'
        />

    </>);
};