import React, {useEffect, useState} from 'react';
import {EzButton} from "../../../components/ui-elements/EzButton"
import {MemberStyles} from '../../../styles';
import {MtBodyText, MtDropDown, MtInput} from '../../../components/ui-elements';
import {getSystemList} from '../../../services/api/api-service'

export const KycStep1 = props => {
  const { values, handleChange, handleSubmit, back,errors, touched, handleBlur,next} = props;
  const [stateValue, setStateValue] = useState(null);
  const [stateList, setstateList] = useState([]);
  useEffect(()=>{
    getSystemList('state').then(setstateList)
  },[])
   return (
    <>
      <MtBodyText style={MemberStyles.labeltext}>Full Name</MtBodyText>
      <MtInput
        placeholder="Full Name"
        name="fullName"
        onChangeText={handleChange('fullName')}
        value={values.fullName}
        errorMessage={touched.fullName && errors.fullName}
        onBlur={handleBlur('fullName')}
      />
     <MtBodyText style={MemberStyles.labeltext}>State</MtBodyText>
      <MtDropDown
        items={stateList}
        name="state"
        onValueChangeHandler={value => {
          setStateValue(value)
        }}
        value={stateValue}
      />
      <MtBodyText style={MemberStyles.labeltext}>City</MtBodyText>
      <MtInput
        placeholder="City"
        name="city"
        onChangeText={handleChange('city')}
        value={values.city}
        errorMessage={touched.city && errors.city}
        onBlur={handleBlur('city')}
      />
      <MtBodyText style={MemberStyles.labeltext}>Pin Code</MtBodyText>
      <MtInput
        placeholder="Pin Code"
        name="pinCode"
        onChangeText={handleChange('pinCode')}
        value={values.pinCode}
        errorMessage={touched.pinCode && errors.pinCode}
        onBlur={handleBlur('pinCode')}
      />
      <EzButton
            buttonType='outline'
            OnPressHandler={next}
            title='Next'
            btnWidth='95%'
            buttonColor='#039BE5'
        />
    </>
    );
};