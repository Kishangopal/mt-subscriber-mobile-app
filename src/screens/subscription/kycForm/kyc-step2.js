import React from 'react';
import {MemberStyles} from '../../../styles';
import {EzButton} from "../../../components/ui-elements/EzButton"
import {MtBodyText, MtImagePicker, MtInput} from '../../../components/ui-elements';

export const KycStep2 = props => {
  const { values, handleChange, handleSubmit, back, errors, touched, handleBlur, next } = props;
  return (
  <>
    <MtBodyText style={MemberStyles.labeltext}>PAN Number</MtBodyText>
    <MtInput
      placeholder="PAN Number"
      name="panNumber"
      onChangeText={handleChange('panNumber')}
      value={values.panNumber}
      errorMessage={touched.panNumber && errors.panNumber}
      onBlur={handleBlur('panNumber')}
    />
    <MtBodyText style={MemberStyles.labeltext}>Aadhar Card Number</MtBodyText>
    <MtInput
      placeholder="Aadhar Card Number"
      name="aadharCardNumber"
      onChangeText={handleChange('aadharCardNumber')}
      value={values.aadharCardNumber}
      errorMessage={touched.aadharCardNumber && errors.aadharCardNumber}
      onBlur={handleBlur('aadharCardNumber')}
    />
    <MtBodyText style={MemberStyles.labeltext}>Pan Card</MtBodyText>
    <MtImagePicker />
    <MtBodyText style={MemberStyles.labeltext}>Aadhar Card</MtBodyText>
    <MtImagePicker />
    <MtBodyText style={MemberStyles.labeltext}>Profile Image</MtBodyText>
    <MtImagePicker />
    <MtBodyText style={MemberStyles.labeltext}>Profile Image</MtBodyText>
    <MtImagePicker />
    <EzButton
      buttonType='outline'
      OnPressHandler={back}
      title='Back'
    />
    <EzButton
      buttonType='outline'
      OnPressHandler={next}
      title='Preview'
      buttonColor='#039BE5'
    />
  </>
  );
};