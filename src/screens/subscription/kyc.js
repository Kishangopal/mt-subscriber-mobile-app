import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import KycMultiStep from './multistepkyc';

export const Kyc=() => {
    return (
      <>
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
       <View style={MemberStyles.meanBox}>
       <KycMultiStep />
       </View>
      </ScreenLayout>
      </>
     )
};
