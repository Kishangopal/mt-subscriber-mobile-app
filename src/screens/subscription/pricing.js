import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {PricingCard} from 'react-native-elements';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtBodyText} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {getPricingList} from '../../services/api/api-service';

export const Pricing = () => {
  const [pricingList, setPricingList] = useState([]);
  useEffect(()=>{
    getPricingList('Active').then(setPricingList)
  },[])
    return (
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      <View style={MemberStyles.HeadingSection}>
        <MtBodyText style={MemberStyles.headingTitleStyle}>Choose your Pricing plan</MtBodyText>
        <MtBodyText>Choice the plan that best suits your business</MtBodyText>
      </View>
      {
      pricingList.map(item => (
      <PricingCard key={item.id}
      color="#2948df"
      title={item.planName}
      titleStyle={MemberStyles.cardTitle}
      price={"INR "+item.price}
      pricingStyle={MemberStyles.pringStyle}
      info={['Near By Vehicle Location', 'Vehicle Location', 'Available Load','Add Business Listing','Register Truck']}
      button={{ title: 'Buy Now' }}
    />
     ))}
     </ScreenLayout>
    );
  };
