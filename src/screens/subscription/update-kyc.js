import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import MultiSetpUpdate from './multi-step-update';

export const UpdateKyc = props => {
    return (
      <>
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
       <View style={MemberStyles.meanBox}>
       <MultiSetpUpdate
       userid={props.kycid}
        />
       </View>
      </ScreenLayout>
      </>
    );
  };