import React, {useState} from "react";
import {Formik} from "formik";
import {KycStep1, KycStep2, KycStep3} from "./kycForm";
import {KycValidationSchema} from '../../components/ui-elements'
import {SafeAreaView, View} from 'react-native';

const renderStep = (step, values, errors, touched, handleChange, handleSubmit, next, back,signupSuccess, handleBlur) => {
  switch (step) {
    case 1:
      return (
        <KycStep1
          values={values}
          errors={errors}
          touched={touched}
          handleChange={handleChange}
          next={next}
          handleBlur={handleBlur}
        />);
    case 2:
      return (
        <KycStep2
          values={values}
          errors={errors}
          touched={touched}
          handleChange={handleChange}
          back={back}
          next={next}
          handleSubmit={handleSubmit}
          handleBlur={handleBlur}
        />);
    case 3:
      return <KycStep3
      values={values}
      errors={errors}
      touched={touched}
      handleChange={handleChange}
      back={back}
      handleSubmit={handleSubmit}
      handleBlur={handleBlur}
      />;
    default:
      return <KycStep1 errors={errors} touched={touched} />;
  }
};
const MultiSetpUpdate = (props) => {
  const [step, setStep] = useState(1);
  const [signupSuccess, setSignupSuccess] = useState(false)
  const kycRegId = props.params.userid;
  const formData = {
    fullName:'Kishan',
    state:'Rajasthan',
    city:'Ajmer',
    pinCode:'305001',
    panNumber:'5656525636',
    aadharCardNumber:'454585856585685',
    referenceId:'10001',
    referenceType:'user',
    kycName:'Subscription',
    kycType:'Golden',
    govtDocType:'GST'
  }
  const kycData={...formData,...{id:kycRegId}}
  const next = () => {
    // update state.step by adding to previous state
    setStep(s => s + 1)
  }
  // process to previous step
  const back = () => {
    // update state.step by minus 1 from previous state
    setStep(s => s - 1)
  }
  const handleSubmit = (values, actions) => {
    console.log(JSON.stringify(values))
    UpdateKycRegister(values).then(result=>{
      actions.setSubmitting(false);
     }).catch(alert);
 }
  return (
    <SafeAreaView>
    <View>
      <Formik
        enableReinitialize
        initialValues={{ ...kycData}}
        validationSchema={KycValidationSchema }
        onSubmit={(values,actions) => {handleSubmit(values, actions) }}
      >
        {({ values, errors, touched, handleChange, handleSubmit, handleBlur }) => (
          <>
            {renderStep(step, values, errors, touched, handleChange, handleSubmit, next, back, signupSuccess, handleBlur)}
          </>
        )}
      </Formik>
    </View>
    </SafeAreaView>
  );
};
export default MultiSetpUpdate