import React from 'react';
import {Text,} from "react-native";
import {ScreenLayout} from '../screen-layout';

export const VehicleSearchScreen = ({ route }) => (
    <ScreenLayout>
      <Text>Vehicle Search Screen</Text>
      {route.params.name && <Text>{route.params.name}</Text>}
    </ScreenLayout>
);