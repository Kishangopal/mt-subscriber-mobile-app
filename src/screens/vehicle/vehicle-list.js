import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import {AppRoute} from '../../navigation/app-routes';
import {MtBodyText, MtLink} from '../../components/ui-elements';
import { Icon } from 'react-native-elements'
import {getVehicleList} from '../../services/api/api-service';

export const VehicleList = (props) => {
  const [vehicleList, setVehicleList] = useState([]);
useEffect(()=>{
  getVehicleList('10001').then(setVehicleList)
},[])
  const navigateViewDetails  = () => {
    props.navigation.navigate(AppRoute.VEHICLE_DETAILS_VIEW);
  };
  const navigatemap  = () => {
    props.navigation.navigate(AppRoute.VEHICLE_ON_MAP);
  };
  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
     <View style={MemberStyles.cardHeadBox}>
      <MtBodyText style={MemberStyles.labelHeadtext}>Result {vehicleList.length} </MtBodyText>
    </View>
     {
    vehicleList.map(item => (
     <View style={MemberStyles.cardBox} key={item.id}>
      <MtBodyText style={MemberStyles.cardTitleStyle}>{item.vehicleNumber}({item.vehicleType})</MtBodyText>
      <View style={MemberStyles. cardbodystyle}>
        <MtBodyText style={MemberStyles.cardtext}><Icon type="font-awesome" name="vcard-o" size={16} color="black" /> RC/ {item.rcNumber}</MtBodyText>
      </View>
      <View style={MemberStyles.cardFooterColumn}>
       <View style={MemberStyles.footetItem}>
       <MtLink  onPressHandler={navigateViewDetails}><Icon type="font-awesome-5" name="eye" color={"#50a5f1"} size={18} /></MtLink>
       </View>
       <View style={MemberStyles.footetItem}>
       <MtLink onPressHandler={() =>props.navigation.push(AppRoute.EDIT_VEHICLE_REGISTER) } ><Icon type="font-awesome-5" name="edit" color={"#50a5f1"} size={18} /></MtLink>
       </View>
       <View style={MemberStyles.footetItem}>
       <MtLink onPressHandler={navigatemap}><Icon type="font-awesome-5" name="map-marked-alt" color={"#50a5f1"} size={18} /></MtLink>
       </View>
      </View>
      </View>
        ))}
    </ScreenLayout>
);
};