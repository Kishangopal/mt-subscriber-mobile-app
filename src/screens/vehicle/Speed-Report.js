import React, {useEffect, useState} from 'react';
import {Dimensions, ScrollView, StyleSheet, View} from "react-native";
import _ from "lodash"
import {MemberStyles} from '../../styles';
import {MtActivityIndicator, MtBodyText, MtDropDown} from '../../components/ui-elements';
import {getAvgSpeedDetail, getSystemList} from '../../services/api/api-service';
import {BarChart} from "react-native-chart-kit";
import { DefaultScreen } from '../../screens/vehicle/vehicleForm';
const screenWidth = Dimensions.get("window").width-20;
export const SpeedReports = ({ route }) => {
  const [reportsTypeValue, setReportsTypeValue] = useState("today");
  const [reportsTypeList, setReportsTypeList] = useState([]);
  const [avgSpeedDetail, setAvgSpeedDetail] = useState([]);
  const [avgSpeedDetailApiLoading, setAvgSpeedDetailApiLoading] = useState(false);
  const vehicleSelectedNo = route.params.vehicleNo;
  const tableLable = [];
  const tableDataSets = [];
  useEffect(() => {
    getSystemList('ReportFilter').then(setReportsTypeList)
    if (vehicleSelectedNo) {
      getAvgSpeedDetail(vehicleSelectedNo, reportsTypeValue).then(data => {
         console.log("Hourly Stats",data);
        if (!_.isEmpty(data)) {
          setAvgSpeedDetail(data)
          setAvgSpeedDetailApiLoading(true)
        }
      }).catch(e => {
        console.log("Error in Hourly Starts:", e);
      })
      //console.log("Hourly Stats",dataBars);
    }
  }, [reportsTypeValue])
  for (let i = 0; i < avgSpeedDetail.length; i += 1) {
    tableLable.push(avgSpeedDetail[i].speed);
    tableDataSets.push(avgSpeedDetail[i].distance);
  }
  const dataBars={
    labels: tableLable,
    datasets: [
      {
        data: tableDataSets
      }
    ]
  };

  const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#fff",
    fillShadowGradient:'#1479de',
    fillShadowGradientOpacity:1,
    color: (opacity = 10) => `rgba(0, 0, 0)`,
    strokeWidth: 20, // optional, default 3
    barPercentage: 0.5,
    data: dataBars.datasets,
    useShadowColorFromDataset: false // optional
  };
  return (
    <>
      <View style={MemberStyles.headerReportBox}>
        <MtBodyText style={MemberStyles.reportsDetaislHeading}>{vehicleSelectedNo}</MtBodyText>
        <MtDropDown
          items={reportsTypeList}
          onValueChangeHandler={value => {
            setReportsTypeValue(value)
          }}
          selectedvalue={reportsTypeValue}
          name="timeDropDown"
        />
      </View>
      {avgSpeedDetailApiLoading &&
      <ScrollView style={MemberStyles.scrollViewBox}>
        {!_.isEmpty(avgSpeedDetail) &&
          <View>
            <View style={styles.graphicart}>
              <BarChart
                data={dataBars}
                width={screenWidth}
                height={200}
                yAxisLabel="KMS"
                chartConfig={chartConfig}
                verticalLabelRotation={0}
                horizontalLabelRotation={0}
              />
            </View>
            <View style={styles.graphicart} >
              <View style={{ flexDirection: 'row', backgroundColor: '#7dbeff', padding: 5 }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <MtBodyText style={MemberStyles.cardTitle}>Speed</MtBodyText>
                </View>
                <View style={{ flex: 1, justifyContent: 'center',color:'#000' }}>
                  <MtBodyText style={MemberStyles.cardTitle}>Distance</MtBodyText>
                </View>
              </View>
              {
                avgSpeedDetail.map((list,key) => (
                  <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#000', padding: 5 }} key={key} >
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                      <MtBodyText>{list.speed}</MtBodyText>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                      <MtBodyText>{list.distance} km/s</MtBodyText>
                    </View>
                  </View>

                ))
              }
            </View>
          </View>
        }
        {_.isEmpty(avgSpeedDetail) &&
         <DefaultScreen
         headingMessage="Speed Reports Data Not Available"
         subheading="Search Data Not Available Right Now" />
        }
      </ScrollView>
    }
    {!avgSpeedDetailApiLoading &&
          <MtActivityIndicator />
    }
    </>
  );
};
const styles = StyleSheet.create({
  graphicart: {
    bottom: 0,
    marginTop:10,
    padding: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    alignSelf: "center",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 5,
  },
});
