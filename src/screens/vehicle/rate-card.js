import React, { useEffect, useState } from 'react';
import {
    View, ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import { GlobalStyles, MemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import { MtBodyText, MtDropDown, MtButton } from '../../components/ui-elements';
import { getSystemList, findRateCard, getSystemListType, getDestance } from '../../services/api/api-service';
import Autocomplete from 'react-native-autocomplete-input';
import reducer from '../../store/_reducer/localData';
import _ from 'lodash';
export const RateCard = (props) => {
    const [cityList, setCityList] = useState([]);
    const [rateCard, setRateCard] = useState(0);
    const [resultCheck, setResultCheck] = useState(false);
    //
    const [vehicleTypeValue, setVehicleTypeValue] = useState(null);
    const [vehicleTypeList, setVehicleTypeList] = useState([]);
    // For Filtered Data
    const [filteredFilms, setFilteredFilms] = useState([]);
    const [filteredDestination, setFilteredDestination] = useState([]);
    // For Selected Data
    const [selectedValue, setSelectedValue] = useState({});
    const [selectedDestinationValue, setSelectedDestinatioValue] = useState({});
    const [kmValue, setKmValue] = useState(0);
    const [transportationValue, setTransportationValue] = useState(0);
    const [durationValue, setDurationValue] = useState(0);
    const msToTime = (duration) => {
        const sec = parseInt(duration, 10) * 2; // convert value to number if it's string
        let hours = Math.floor(sec / 3600); // get hours
        let minutes = Math.floor((sec - (hours * 3600)) / 60); // get minutes
        let seconds = sec - (hours * 3600) - (minutes * 60); //  get seconds
        // add 0 if value < 10; Example: 2 => 02
        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes;}
        if (seconds < 10) { seconds = "0" + seconds;}
        //console.log(`hours ${hours}:${minutes}:duration:${duration}`);
        setDurationValue(`${hours} hours ${minutes} Minutes`)
    }
    const findFilm = (query) => {
        // Method called every time when we change the value of the input
        if (query) {
            // Making a case insensitive regular expression
            const regex = new RegExp(`${query.trim().indexOf(")") < 0 ? query.trim().indexOf("(") > 0 ? query.trim() + ")" : query.trim() : query.trim() + ")"}`, 'i');
            // Setting the filtered film array according the query
            setFilteredFilms(
                //console.log(cityList[0].label)
                cityList.filter((cityList) => cityList.label.search(regex) >= 0)
            );
        } else {
            // If the query is null then return blank
            setFilteredFilms([]);
        }
    };
    const findDestination = (query) => {
        // Method called every time when we change the value of the input
        if (query) {
            // Making a case insensitive regular expression
            const regex = new RegExp(`${query.trim().indexOf(")") < 0 ? query.trim().indexOf("(") > 0 ? query.trim() + ")" : query.trim() : query.trim() + ")"}`, 'i');
            // Setting the filtered film array according the query
            setFilteredDestination(
                //console.log(cityList[0].label)
                cityList.filter((cityList) => cityList.label.search(regex) >= 0)
            );
        } else {
            // If the query is null then return blank
            setFilteredDestination([]);
        }
    };
    function findRateCardValue(itemData) {
        findRateCard(itemData, vehicleTypeValue).then(data => {
            setRateCard(data[0].charges);
        })
    };
    const handleSubmit = () => {
        findRateCardValue(selectedValue.state);
        console.log(vehicleTypeValue);
        console.log(selectedValue.value);
        console.log(selectedDestinationValue);
        console.log(`Rate Card:- ${rateCard}`);
        getDestance(selectedValue.value, selectedDestinationValue.value).then(data => {
            if (!_.isEmpty(data)) {
                console.log(data);
                setKmValue(data.data.distance.text);
                //setDurationValue(data.data.duration.value);
                msToTime(data.data.duration.value);
                const dataValue = (data.data.distance.value / 1000) * rateCard;
                setTransportationValue(dataValue);
                setResultCheck(true);
            }
        })
    }
    //=========================================================
    useEffect(() => {
        getSystemList('sub-locality').then(setCityList)
        getSystemListType('vehicleType').then(setVehicleTypeList)
    }, [])
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            <View style={styles.meanBox}>
                <MtBodyText style={MemberStyles.labeltext}>Vehicle Type</MtBodyText>
                <MtDropDown
                    items={vehicleTypeList}
                    onValueChangeHandler={value => {
                        setVehicleTypeValue(value)
                        setResultCheck(false);
                    }}
                    name="vehicleType"
                    selectedvalue={vehicleTypeValue}
                />
                <MtBodyText style={MemberStyles.labeltext}>Pickup Location</MtBodyText>
                <ScrollView>
                    <Autocomplete
                        autoCapitalize="none"
                        autoCorrect={true}
                        containerStyle={styles.autocompleteContainer}
                        inputContainerStyle={styles.inputContainer}
                        listContainerStyle={styles.listContainerStyle}
                        listStyle={styles.listStyleAutoSearch}
                        // Data to show in suggestion
                        data={filteredFilms}
                        // Default value if you want to set something in input
                        defaultValue={
                            JSON.stringify(selectedValue) === '{}' ? '' : selectedValue.label
                        }
                        onChangeText={(text) => findFilm(text)}
                        placeholder="Enter The From Location"
                        renderItem={({ item, key }) => (
                            // For the suggestion view
                            <TouchableOpacity
                                key={key}
                                onPress={() => {
                                    setSelectedValue(item);
                                    setFilteredFilms([]);
                                    findRateCardValue(item.state);
                                    setResultCheck(false);
                                }}>
                                <Text style={styles.itemText}>
                                    {item.label}
                                </Text>
                            </TouchableOpacity>
                        )}
                    />
                </ScrollView>



                <MtBodyText style={MemberStyles.labeltext}>Destination</MtBodyText>
                <ScrollView>
                    <Autocomplete
                        autoCapitalize="none"
                        autoCorrect={true}
                        containerStyle={styles.autocompleteContainer}
                        inputContainerStyle={styles.inputContainer}
                        listContainerStyle={styles.listContainerStyle}
                        listStyle={styles.listStyleAutoSearch}
                        // Data to show in suggestion
                        data={filteredDestination}
                        // Default value if you want to set something in input
                        defaultValue={
                            JSON.stringify(selectedDestinationValue) === '{}' ? '' : selectedDestinationValue.label
                        }
                        onChangeText={(text) => findDestination(text)}
                        placeholder="Enter The From Location"
                        renderItem={({ item, key }) => (
                            // For the suggestion view
                            <TouchableOpacity
                                key={key}
                                onPress={() => {
                                    setSelectedDestinatioValue(item);
                                    setFilteredDestination([]);
                                }}>
                                <Text style={styles.itemText}>
                                    {item.label}
                                </Text>
                            </TouchableOpacity>
                        )}
                    />
                    <MtButton
                        title="Submit"
                        onPressHandler={handleSubmit}
                        btnWidth='95%'
                    />
                </ScrollView>
            </View>
            {resultCheck &&
                <>
                    <View style={styles.meanBox}>
                        <MtBodyText style={styles.titleBoldCenter}>Transportation Charges</MtBodyText>
                        <MtBodyText style={styles.valueboldCenter}>{`${Math.floor(transportationValue)} Rs.`}</MtBodyText>
                        <View style={styles.bodyLayout}>
                            <View style={styles.flexCol}>
                                <MtBodyText style={styles.titleCenter}>Distance</MtBodyText>
                                <MtBodyText style={styles.valueCenter}>{`${kmValue}`}</MtBodyText>
                            </View>
                            <View style={styles.flexCol}>
                                <MtBodyText style={styles.titleCenter}>Time Duraction</MtBodyText>
                                <MtBodyText style={styles.valueCenter}>{`${durationValue}`}</MtBodyText>
                            </View>
                           
                        </View>
                        <MtButton
                        title="Find transporter"
                        btnWidth='95%'
                        onPressHandler={() => props.navigation.push(AppRoute.TRANSPORTER, { stateProps: selectedValue })}
                    />
                    </View>
                   </>
            }
        </ScreenLayout>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1,
        padding: 16,
        marginTop: 40,
    },
    autocompleteContainer: {
        backgroundColor: '#ffffff',
        marginLeft: 10,
        marginEnd: 10,
        zIndex: 1
    },
    itemText: {
        fontSize: 15,
        paddingTop: 7,
        paddingBottom: 7,
        margin: 4,
    },
    inputContainer: {
        paddingLeft: 10,
    },
    listContainerStyle:
    {
        marginLeft: 0,
        marginEnd: 0,
        backgroundColor: "#fff",
        padding: 0,
    },
    listStyleAutoSearch:
    {
        marginLeft: 0,
        marginEnd: 0,
        backgroundColor: "#fff",
        padding: 10,
    },
    titleCenter:
    {
        fontSize: 13,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 8,
        backgroundColor: '#d5d5d5',
    },
    titleBoldCenter:
    {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 8,
        backgroundColor: '#d5d5d5',
    },
    valueCenter:
    {
        fontSize: 13,
        fontWeight: '200',
        color: '#000',
        textAlign: 'center',
        padding: 5,
        marginLeft:10,
        marginRight:10,
    },
    valueboldCenter: {
        fontSize: 25,
        fontWeight: '200',
        color: '#000',
        textAlign: 'center',
        padding: 5,
        fontWeight: 'bold',
        marginLeft:10,
        marginRight:10,
    },
    meanBox: {
        flex: 1,
        backgroundColor: "#fff",
        width: '100%',
        borderRadius: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        marginTop: 10,
        marginBottom: 5,
        padding: 2,
    },
    bodyLayout: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 5,
        
    },
    flexCol: {
        flex: 1,
        flexDirection: 'column',
    },

});