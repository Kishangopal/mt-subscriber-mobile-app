import React, { useEffect, useState } from 'react';
import { Linking, View, StyleSheet, ScrollView, TouchableOpacity, Text } from 'react-native';
import { GlobalStyles, MemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import { AppRoute } from '../../navigation/app-routes';
import _ from "lodash"
import { getNeedLoadList, getSystemList, getAllVehicle } from '../../services/api/api-service';
import { MtBodyText, MtButton, MtActivityIndicator } from '../../components/ui-elements';
import { Icon } from 'react-native-elements'
import { DefaultScreen } from '../../screens/vehicle/vehicleForm';
import Autocomplete from 'react-native-autocomplete-input';
import Toast from 'react-native-toast-message';
import { cos } from 'react-native-reanimated';
export const NeedLoadList = (props) => {
  const [needLoadList, setNeedLoadList] = useState([]);
  const [listStatus, setListStatus] = useState(false);
  const [cityList, setCityList] = useState([]);
  const [cityName, setCityName] = useState([]);
  const [selectedCity, setSelectedCity] = useState("All");
  const [vehicleList, setVehicleList] = useState([]);
  //====================AutoFilter=================================
  const [films, setFilms] = useState([]);
  // For Filtered Data
  const [filteredFilms, setFilteredFilms] = useState([]);
  // For Selected Data
  const [selectedValue, setSelectedValue] = useState({});
  const findFilm = (query) => {
    // Method called every time when we change the value of the input
    if (query) {
      // Making a case insensitive regular expression
      const regex = new RegExp(`${query.trim().indexOf(")") < 0 ? query.trim().indexOf("(") > 0 ? query.trim() + ")" : query.trim() : query.trim() + ")"}`, 'i');
      // Setting the filtered film array according the query
      setFilteredFilms(
        cityList.filter((cityList) => cityList.label.search(regex) >= 0)
      );
    } else {
      // If the query is null then return blank
      setFilteredFilms([]);
    }
  };
  function getVehicleList() {
    getAllVehicle().then(vehicleListData => {
      if (!_.isEmpty(vehicleListData)) {
        setVehicleList(vehicleListData);
        console.log(vehicleList);
      }
    }).catch(ee => {
      console.log("Error in Vehicle List list:", ee);
    })
  }
  //=========================================================
  function loadNeedLoadList(citySelect) {
    const markers = [];
    getSystemList('city').then(citylist => {
      if (!_.isEmpty(citylist)) {
        setCityList(citylist)
        getNeedLoadList(citySelect).then(data => {
          data.forEach(dataValue => {
            let cityDisplayName = _.get(_.find(citylist, { value: dataValue.fromLocation }), "label")
            let VehicleTypeLabel = _.get(_.find(vehicleList, { vehicleNumber: dataValue.vehicleNumber }), "vehicleType")
            console.log(VehicleTypeLabel);
            markers.push({
              fromLocation: dataValue.fromLocation,
              toLocation: dataValue.toLocation,
              vehicleNumber: dataValue.vehicleNumber,
              itemWeight: dataValue.itemWeight,
              contactNumber: dataValue.contactNumber,
              contactName: dataValue.contactName,
              mtUserId: dataValue.mtUserId,
              id: dataValue.id,
              cityDisplayName,
              VehicleTypeLabel
            })
          })
          console.log(markers);
          setNeedLoadList(markers);
          setListStatus(true);
        })
      }
    }).catch(e => {
      console.log("Error in State list:", e);
    })
  }
  useEffect(() => {
    getVehicleList();
    loadNeedLoadList(selectedCity);
  }, [cityList])
  return (
    <>
      <View style={MemberStyles.FixHeadBox}>
        <View style={MemberStyles.fixHeadresultBox}>
          <MtBodyText style={MemberStyles.labelHeadtext}>Result {needLoadList.length} </MtBodyText>
        </View>
        <View style={styles.FilterHeadBox}>
          <View style={styles.rowBox}>
            <ScrollView>
              <Autocomplete
                autoCapitalize="none"
                autoCorrect={true}
                containerStyle={stylesAutoSearch.autocompleteContainer}
                inputContainerStyle={stylesAutoSearch.inputContainer}
                listContainerStyle={stylesAutoSearch.listContainerStyle}
                listStyle={stylesAutoSearch.listStyleAutoSearch}
                // Data to show in suggestion
                data={filteredFilms}
                // Default value if you want to set something in input
                defaultValue={
                  JSON.stringify(selectedValue) === '{}' ? '' : selectedValue.label
                }
                onChangeText={(text) => findFilm(text)}
                placeholder="Search Location"
                renderItem={({ item, key }) => (
                  // For the suggestion view
                  <TouchableOpacity
                    key={key}
                    onPress={() => {
                      // getNeedLoadList(item.value).then(data => {
                      //   setNeedLoadList(data)

                      // })
                      loadNeedLoadList(item.value)
                      setSelectedValue(item);
                      setFilteredFilms([]);
                    }}>
                    <Text style={stylesAutoSearch.itemText}>
                      {item.label}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </ScrollView>
          </View>
        </View>
      </View>
      <ScreenLayout style={MemberStyles.scrollViewListFixBox}>
        {listStatus &&
          <View style={{ marginTop: 30 }}>
            {
              needLoadList.map((item, key) => (
                <View style={MemberStyles.cardBox} key={key}>
                  <MtBodyText style={MemberStyles.cardTitleStyle}>{item.vehicleNumber}({item.VehicleTypeLabel})</MtBodyText>
                  <View style={MemberStyles.cardbodystyle}>
                    <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome' name="map-marker" size={15} color="black" /> {item.cityDisplayName}</MtBodyText>
                    <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome' name="arrow-circle-o-right" size={15} color="black" /> {item.toLocation}(Destination)</MtBodyText>
                    <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome' name="user" size={15} color="black" /> {item.contactName}</MtBodyText>
                    <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome' name="phone" size={15} color="black" /> {item.contactNumber}</MtBodyText>
                    <View style={MemberStyles.cardFooterColumn}>
                      <View style={MemberStyles.footetItem}>
                        <MtButton title="Call" onPressHandler={() => { Linking.openURL('tel:' + item.contactNumber); }} btnWidth='95%' />
                      </View>
                    </View>
                  </View>
                </View>
              ))}
            {_.isEmpty(needLoadList) &&
              <DefaultScreen
                headingMessage="Post Load Data Not Available"
                subheading="Post Load Data Not Available Right Now" />
            }
          </View>
        }
        {!listStatus &&
          <MtActivityIndicator />
        }
      </ScreenLayout>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  cardHeadBox:
  {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 1,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
  },
  FilterHeadBox:
  {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 1,
  },
  rowBox: {
    flex: 1,
    padding: 1,
    zIndex: 0,
    marginVertical: 10,
  }
});

const stylesAutoSearch = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    padding: 16,
    marginTop: 40,
  },
  autocompleteContainer: {
    backgroundColor: '#ffffff',
    marginLeft: 10,
    marginEnd: 10,
    zIndex: 2
  },
  itemText: {
    fontSize: 15,
    paddingTop: 7,
    paddingBottom: 7,
    margin: 4,
  },
  inputContainer: {
    paddingLeft: 10,
  },
  listContainerStyle:
  {
    marginLeft: 0,
    marginEnd: 0,
    backgroundColor: "#fff",
    padding: 0,
  },
  listStyleAutoSearch:
  {
    marginLeft: 0,
    marginEnd: 0,
    backgroundColor: "#fff",
    padding: 10,
    zIndex: 1,
  }
});