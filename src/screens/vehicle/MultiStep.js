import React, {useState} from "react";
import {Formik} from "formik";
import {VehicleRegisterStep1, VehicleRegisterStep2, VehicleRegisterStep3} from "./vehicleForm";
import {VehicleValidationSchema} from '../../components/validations'
import {CreateVehicleRegister} from '../../services/api/api-service'
import {SafeAreaView, View} from 'react-native';

const renderStep = (step, values, errors, touched, handleChange, handleSubmit, next, back, signupSuccess, handleBlur) => {
  switch (step) {
    case 1:
      return (
        <VehicleRegisterStep1
          values={values}
          errors={errors}
          touched={touched}
          handleChange={handleChange}
          next={next}
          handleBlur={handleBlur}
        />);
    case 2:
      return (
        <VehicleRegisterStep2
          values={values}
          errors={errors}
          touched={touched}
          handleChange={handleChange}
          back={back}
          next={next}
          handleSubmit={handleSubmit}
          handleBlur={handleBlur}
        />);
    case 3:
      return <VehicleRegisterStep3
        values={values}
        errors={errors}
        touched={touched}
        handleChange={handleChange}
        back={back}
        handleSubmit={handleSubmit}
        handleBlur={handleBlur}
      />;
    default:
      return <VehicleRegisterStep1 errors={errors} touched={touched} />;
  }
};

const MultiStep = () => {
  const [step, setStep] = useState(1);
  const [signupSuccess, setSignupSuccess] = useState(false)
  const formData = {
    vehicleNumber: "RJ01DD5652",
    vehicleType: "PickUp",
    modelNumber: "2020",
    chassisNumber: "56525623252363",
    engineNo: "5652525652",
    nationPermitNumber: "2525223636",
    insuranceNumber: "565652565",
    fitnessNumber: "45655656",
    loadCapacity: "",
    ownerName: "",
    panNumber: "",
    rcNumber: "",
    ownerContactNumber: "",
    status: "Unverified",
    mtUserId: "10001"
  }
  const next = () => {
    // update state.step by adding to previous state
    setStep(s => s + 1)
  }

  // process to previous step
  const back = () => {
    // update state.step by minus 1 from previous state
    setStep(s => s - 1)
  }
  const handleSubmit = (values, actions) => {
    console.log(JSON.stringify(values))
    CreateVehicleRegister(values).then(result => {
      actions.setSubmitting(false);
    }).catch(alert);
  }
  return (
    <SafeAreaView>
      <View>
        <Formik
          enableReinitialize
          initialValues={{ ...formData}}
          validationSchema={VehicleValidationSchema}
          onSubmit={(values, actions) => {handleSubmit(values, actions) }}
        >
          {({ values, errors, touched, handleChange, handleSubmit, handleBlur }) => (
            <>
              {renderStep(step, values, errors, touched, handleChange, handleSubmit, next, back, signupSuccess, handleBlur)}
            </>
          )}
        </Formik>
      </View>
    </SafeAreaView>
  );
};
export default MultiStep