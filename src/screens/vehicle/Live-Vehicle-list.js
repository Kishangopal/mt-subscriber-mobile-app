import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {MemberStyles} from '../../styles';
import {MtActivityIndicator, MtBodyText} from '../../components/ui-elements';
import {AppRoute} from '../../navigation/app-routes';
import _ from 'lodash'
import {getVehicleLocations, getVehicleLocationStats} from '../../services/api/api-service';
import { Icon } from 'react-native-elements'

export const LiveVehiclelist = (props) => {
  const [vehicleCordinates, setVehicleCordinates] = useState([]);
  const [vehicleApiLoading, setVehicleApiLoading] = useState(true);
  const [vehicleStats, setVehicleStats] = useState({
    all:0,
    running:0,
    stopped:0,
    no_data:0,
    no_network:0
  });
  const setVehicleLocationsArr=(filter)=>{
    const markers = [];
    getVehicleLocations(filter).then(response => {
     // console.log(response);
      response.forEach(d => {
        markers.push({
          title: d.vehicle_number,
          device_id: d.device_id,
          address: d.address,
          motion_status: d.motion_status,
          motion_state: d.motion_state,
          speed: d.speed,
        })
      })
      setVehicleCordinates(markers)
      setVehicleApiLoading(false)
    })

  }

  useEffect(() => {
    setVehicleLocationsArr("all");
    getVehicleLocationStats().then(data=>{
      setVehicleStats(data)
    })
  }, [])
  return (
    <>
      <View style={MemberStyles.headerFilterBox}>
        <TouchableOpacity onPress={()=>setVehicleLocationsArr("all")} style={
          {...MemberStyles.filterButton,...{backgroundColor:'#C4C4C4'}}
          }>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>All</MtBodyText>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>{vehicleStats.all}</MtBodyText>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>setVehicleLocationsArr("running")} style={ {...MemberStyles.filterButton,...{backgroundColor:'#a5d6a7'}}}>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>Running</MtBodyText>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>{vehicleStats.running}</MtBodyText>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>setVehicleLocationsArr("stopped")} style={ {...MemberStyles.filterButton,...{backgroundColor:'#ef5350'}}}>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>Stopped</MtBodyText>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>{vehicleStats.stopped}</MtBodyText>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>setVehicleLocationsArr("no_network")} style={ {...MemberStyles.filterButton,...{backgroundColor:'#ffb100'}}}>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>Offline</MtBodyText>
            <MtBodyText style={LiveVehicleStyle.filterButtonText}>{vehicleStats.no_network}</MtBodyText>
        </TouchableOpacity>
      </View>
      <ScrollView style={{ marginHorizontal: 10, marginTop: 50, marginBottom: 10, }}>
        {!_.isEmpty(vehicleCordinates) &&
        <View style={{ marginBottom: 10, }}>
          {
            vehicleCordinates.map((item,key) => (
              <TouchableOpacity onPress={() =>
                props.navigation.push(AppRoute.LIVE_VEHICLE,{vehicleId: item.title}) }  key={key}>
              <View style={MemberStyles.listBox}>
                <MtBodyText style={MemberStyles.notificationTitle}>{item.title}</MtBodyText>
                <MtBodyText style={MemberStyles.notificationDescription}>{item.address}</MtBodyText>
                <View style={MemberStyles.footerRight}>
                  <MtBodyText style={item.motion_status === "moving" ? MemberStyles.greentext : item.motion_status === "stopped" ? MemberStyles.redtext : MemberStyles.yellowtext}>{item.motion_status[0].toUpperCase()+item.motion_status.slice(1)}</MtBodyText>
                  <MtBodyText style={MemberStyles.notificationtime}><Icon type='ionicon' name="md-speedometer" size={20} color={"#74788d"} /> {item.speed} /km</MtBodyText>
                </View>
              </View>
              </TouchableOpacity>
            ))
          }
        </View>
        }
        {vehicleApiLoading &&
          <MtActivityIndicator/>
        }
      </ScrollView>

      <View style={MemberStyles.footerBox}>
      <TouchableOpacity onPress={() =>props.navigation.push(AppRoute.VEHICLE_ON_MAP)}>
        <Icon type="font-awesome-5" name="map-marked" size={20} color={"#fff"} style={{
          padding: 10,
          bottom: 0
        }} />

      </TouchableOpacity>
      </View>
    </>
  );
};
export const LiveVehicleStyle = StyleSheet.create({
  filterButtonText:{
    fontFamily:'montserrat-bold',
    color:'#444'
  }
})