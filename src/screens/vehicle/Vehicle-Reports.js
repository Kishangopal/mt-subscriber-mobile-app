import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {AppRoute} from '../../navigation/app-routes';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtDropDown} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {Card, DefaultScreen} from './vehicleForm'
import _ from "lodash"
import {
    getAvgSpeedStats,
    getHourlyStats,
    getMovementStats,
    getStoppageStats,
    getVehicleActiveList
} from '../../services/api/api-service'

export const VehicleReports = (props) => {
  const [vehicleValue, setVehicleValue] = useState(null);
  const [vehicleList, setVehicleList] = useState([]);
  const [hourlyStatsList, setHourlyStatsList] = useState([]);
  const [avgSpeedList, setAvgSpeedList] = useState([]);
  const [stoppageList, setStoppageList] = useState([]);
  const [movementsList, setMovementsList] = useState([]);
  useEffect(() => {
    getVehicleActiveList().then(d=>{
      const allVehicleListData=[]
      d.forEach(element => {
        allVehicleListData.push({label:element.vehicleNumber,value:element.vehicleNumber})
      });
      setVehicleList(allVehicleListData)
    })
    if(vehicleValue)
    {
      console.log(vehicleValue);
      getHourlyStats(vehicleValue).then(data => {
       // console.log("Hourly Stats",data);
        if(!_.isEmpty(data)){
          setHourlyStatsList(data)

        }
      }).catch(e => {
        console.log("Error in Hourly Starts:", e);
      })
      getAvgSpeedStats(vehicleValue).then(data => {
        //console.log("Avg Stats",data);
        if(!_.isEmpty(data)){
          setAvgSpeedList(data)
        }
      }).catch(e => {
        console.log("Error in Avg Speed Stats:", e);
      })
      getStoppageStats(vehicleValue).then(data => {
        //console.log("stoppage Stats",data);
        if(!_.isEmpty(data)){
          setStoppageList(data)
        }
      }).catch(e => {
        console.log("Error in Stoppage Stats:", e);
      })
      getMovementStats(vehicleValue).then(data => {
        //console.log("Movements Stats",data);
        if(!_.isEmpty(data)){
          setMovementsList(data)
        }
      }).catch(e => {
        console.log("Error in Movement Stats:", e);
      })

    }

  }, [vehicleValue])
  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      <View style={MemberStyles.headbox}>
        <MtDropDown
          items={vehicleList}
          onValueChangeHandler={value => {
            setVehicleValue(value)
          }}
          value={vehicleValue}
          dropSelectStyle={MemberStyles.dropSelectStyle}
          name="vehicleNumber"
        />
      </View>
      {vehicleValue && <View>
        <View style={MemberStyles.bodyLayout}>
          <Card
            title="Hourly"
            cardValue={hourlyStatsList.totalDistanceTravelled || 0}
            speedValue={`Max Speed ${hourlyStatsList.max_speed||0} (km/hr)`}
            timeValue="1 hour"
            OnPressHandle={() => props.navigation.push(AppRoute.HOURLY_REPORTS,{vehicleNo: vehicleValue})} />
          <Card
            title="AVG.SPEED"
            cardValue={`${avgSpeedList.avg_speed|| 0} (km/hr)`}
            timeValue="30 days"
            OnPressHandle={() => props.navigation.push(AppRoute.SPEED_REPORTS,{vehicleNo: vehicleValue})} />
        </View>
        <View style={MemberStyles.bodyLayout}>
          <Card
            title="STOPPAGE"
            cardValue={`${stoppageList.stoppage ||0} hours`}
            timeValue="24 hour"
            OnPressHandle={() => props.navigation.push(AppRoute.STOPPAGE_REPORT,{vehicleNo: vehicleValue})} />
          <Card
            title="MOVEMENTS"
            cardValue={movementsList.movements ||0}
            timeValue="7 days"
            OnPressHandle={() => props.navigation.push(AppRoute.MOVEMENTS_REPORTS,{vehicleNo: vehicleValue})} />
        </View>
      </View>}
      {!vehicleValue && <View>
        <DefaultScreen
        headingMessage="Your Reports Will be shown here."
        subheading="search your vehicle to generate reports"/>
      </View>}
    </ScreenLayout>
  );
};