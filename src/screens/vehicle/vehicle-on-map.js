import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import {GlobalStyles} from '../../styles';
import {MtDropDown} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import MapView, {Callout, PROVIDER_GOOGLE} from 'react-native-maps';
import {getVehicleActiveList, getVehicleLocations} from '../../services/api/api-service';

var _mapView;
export const VehicleOnMapScreen = () => {

  const [vehicleCordinates, setVehicleCordinates] = useState([]);
  const [newCordinate, setNewCordinate] = useState({
    latitude: 26.4499,
    longitude: 74.6399,
    latitudeDelta: .005,
    longitudeDelta: .005
  });
  const [vehicleValue, setVehicleValue] = useState(null);
  const [vehicleList, setVehicleList] = useState([]);
  _mapView = React.createRef();
  const animate = (_newCordinates) => {
    if (_mapView.current) {
      if(_newCordinates){
        _mapView.current.animateToRegion(_newCordinates, 1000);
      }else{
        console.log("_newCordinates is blank")
      }
    }else{
      console.log("_mapView.current is blank",_mapView)
    }
  }
  useEffect(() => {
    getVehicleActiveList().then(d => {
      const allVehicleListData = []
      d.forEach(element => {
        allVehicleListData.push({ label: element.vehicleNumber, value: element.vehicleNumber })
      });
      setVehicleList(allVehicleListData)
    })
    const markers = [];
    if(vehicleValue){
      getVehicleLocations(undefined, vehicleValue).then(response => {
        console.log("getVehicleLocations called");
        let _newCordinates={
          latitude: response[0].cordinate.lat,
          longitude: response[0].cordinate.lng,
          latitudeDelta: .005,
          longitudeDelta: .005
        }
        setNewCordinate(_newCordinates)
        animate(_newCordinates)
        response.forEach(data => {
          markers.push({
            title: data.vehicle_number,
            device_id: data.device_id,
            address: data.address,
            coordinates: {
              latitude: data.cordinate.lat,
              longitude: data.cordinate.lng,
            },
          })
        })
        setVehicleCordinates(markers)
      })
    }



    // _mapView.current.animateToRegion(newCordinate, 5000);
  }, [vehicleValue])

  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      <View style={styles.container}>
        <View style={styles.searchBox}>
          <MtDropDown
            items={vehicleList}
            onValueChangeHandler={value => {
              setVehicleValue(value)
            }}
            selectedvalue={vehicleValue}
            name="vehicleNumber"
          />
        </View>
        <MapView
          ref={_mapView}
          provider={PROVIDER_GOOGLE}
          initialRegion={newCordinate}
          zoomEnabled={true}
          showsUserLocation={true}
          minZoomLevel={5}
          zoomControlEnabled={true}
          style={styles.mapStyle}>
          {
            vehicleCordinates.map(item => (
              <MapView.Marker
                key={item.title}
                image={require('../../../assets/mover-truck.png')}
                coordinate={item.coordinates}
                title={item.title}
              >
                <Callout tooltip>
                  <View>
                    <View style={styles.bubble}>
                      <Text style={styles.title}>{item.title}</Text>
                      <Text style={styles.title}>Location:</Text>
                      <Text style={styles.valueTex}>{item.address}</Text>
                    </View>
                    <View style={styles.arrowBorder} />
                    <View style={styles.arrow} />
                  </View>
                </Callout>
              </MapView.Marker>
            ))
          }
        </MapView>

      </View>

    </ScreenLayout>
  )
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: -10,
  },
  searchtextbox:
  {
    flex: 1,
    padding: 0,
    fontSize: 15,
    fontFamily: 'montserrat',
    alignContent: 'flex-start'
  },
  mapStyle: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  searchBox: {
    flex: 1,
    position: 'absolute',
    marginTop: 20,
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 1,
    padding: 0,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  // Callout bubble
  bubble: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 6,
    width: 250,
    padding: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 8,
  },
  //Arrow betow the bubble
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#fff',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#007a87',
    borderWidth: 16,
    alignSelf: 'center',
    margin: -0.5,
  },
  title:
  {
    fontSize: 16,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  valueText:
  {
    fontSize: 10,

  },
});