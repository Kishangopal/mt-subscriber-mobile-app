import React, {useEffect, useState} from 'react';
import {MemberStyles} from '../../../styles';
import {MtBodyText, MtDropDown, MtInput} from '../../../components/ui-elements';
import {EzButton} from "../../../components/ui-elements/EzButton"
import {getSystemList} from '../../../services/api/api-service'

export const VehicleRegisterStep1 = props => {
  const { values, handleChange, handleSubmit, back,errors, touched, handleBlur,next} = props;
  const [vehicleTypeValue, setVehicleTypeValue] = useState(null);
  const [vehicleTypeList, setVehicleTypeList] = useState([]);
  useEffect(()=>{
    getSystemList('vehicleType').then(setVehicleTypeList)
  },[])
  return (
        <>
            <MtBodyText style={MemberStyles.labeltext}>Vehicle Number</MtBodyText>
            <MtInput
              placeholder="Vehicle Number"
              name="vehicleNumber"
              onChangeText={handleChange('vehicleNumber')}
              value={values.vehicleNumber}
              errorMessage={touched.vehicleNumber && errors.vehicleNumber}
              onBlur={handleBlur('vehicleNumber')}
            />
            <MtBodyText style={MemberStyles.labeltext}>Vehicle Type</MtBodyText>
            <MtDropDown
              items={vehicleTypeList}
              onValueChangeHandler={value => {
                setVehicleTypeValue(value)
              }}
              value={vehicleTypeValue}
              name="vehicleType"
            />
            <MtBodyText style={MemberStyles.labeltext}>Model Number</MtBodyText>
            <MtInput
              placeholder="Model Number"
              name="modelNumber"
              onChangeText={handleChange('modelNumber')}
              value={values.modelNumber}
              errorMessage={touched.modelNumber && errors.modelNumber}
              onBlur={handleBlur('modelNumber')}
            />
        <MtBodyText style={MemberStyles.labeltext}>Chassis Number</MtBodyText>
        <MtInput
          placeholder="Chassis Number"
          name="chassisNumber"
          onChangeText={handleChange('chassisNumber')}
          value={values.chassisNumber}
          errorMessage={touched.chassisNumber && errors.chassisNumber}
          onBlur={handleBlur('chassisNumber')}
        />
         <MtBodyText style={MemberStyles.labeltext}>Engine No</MtBodyText>
        <MtInput
          placeholder="Engine No"
          name="engineNo"
          onChangeText={handleChange('engineNo')}
          value={values.engineNo}
          errorMessage={touched.engineNo && errors.engineNo}
          onBlur={handleBlur('engineNo')}
        />
         <MtBodyText style={MemberStyles.labeltext}>Load Capacity</MtBodyText>
        <MtInput
          placeholder="Load Capacity"
          name="loadCapacity"
          onChangeText={handleChange('loadCapacity')}
          value={values.loadCapacity}
          errorMessage={touched.loadCapacity && errors.loadCapacity}
          onBlur={handleBlur('loadCapacity')}
        />
        <MtBodyText style={MemberStyles.labeltext}>Owner Name</MtBodyText>
        <MtInput
          placeholder="Owner Name"
          name="ownerName"
          onChangeText={handleChange('ownerName')}
          value={values.ownerName}
          errorMessage={touched.ownerName && errors.ownerName}
          onBlur={handleBlur('ownerName')}
        />
        <MtBodyText style={MemberStyles.labeltext}>Owner contact number</MtBodyText>
        <MtInput
          placeholder="Owner contact number"
          name="ownerContactNumber"
          onChangeText={handleChange('ownerContactNumber')}
          value={values.ownerContactNumber}
          errorMessage={touched.ownerContactNumber && errors.ownerContactNumber}
          onBlur={handleBlur('ownerContactNumber')}
        />
         <EzButton
            buttonType='outline'
            OnPressHandler={next}
            title='Next'
            buttonColor='#039BE5'
        />
          </>

  );
};
