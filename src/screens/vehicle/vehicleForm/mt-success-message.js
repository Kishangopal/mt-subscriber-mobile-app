import React from 'react';
import {View} from 'react-native';
import {MemberStyles} from '../../../styles';
import {MtBodyText} from '../../../components/ui-elements';
import { Icon } from 'react-native-elements'
export const MtSuccessMessage = (props) => {
  return (
    <>
       <View style={MemberStyles.DefaultScreenBox}>
       <Icon type="antdesign" name="checkcircle" size={120} color="#66ccff" />
         <MtBodyText style={MemberStyles.cardTitleStyle}>{props.headingMessage}</MtBodyText>
        </View>
    </>
  );
};