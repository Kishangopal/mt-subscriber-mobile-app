import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {MemberStyles} from '../../../styles';
import {MtBodyText} from '../../../components/ui-elements';

export const Card = (props) => {
  return (
    <>
     <TouchableOpacity onPress={props.OnPressHandle}  style={MemberStyles.card}>
      <View>
        <View style={MemberStyles.cardBody}>
          <MtBodyText style={MemberStyles.cardTitle}>{props.title}</MtBodyText>
          <MtBodyText style={MemberStyles.cardValue}>{props.cardValue}</MtBodyText>
          <MtBodyText style={MemberStyles.subCardValue}>{props.speedValue}</MtBodyText>
        </View>
        <View style={MemberStyles.cardFooter}>
          <MtBodyText style={MemberStyles.footertext}>In Last {props.timeValue}</MtBodyText>
        </View>
      </View>
      </TouchableOpacity>
    </>
  );
};
