import React from 'react';
import {View} from 'react-native';
import {MemberStyles} from '../../../styles';
import {MtBodyText} from '../../../components/ui-elements';
import { Icon } from 'react-native-elements'
export const DefaultScreen = (props) => {
  return (
    <>
       <View style={MemberStyles.DefaultScreenBox}>
       <Icon type="antdesign" name="barschart" size={60} color="#66ccff" />
         <MtBodyText style={MemberStyles.cardTitleStyle}>{props.headingMessage}</MtBodyText>
        <MtBodyText>{props.subheading}</MtBodyText>
        </View>
    </>
  );
};
