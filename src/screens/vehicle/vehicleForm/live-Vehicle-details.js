import React, {useEffect, useState} from 'react';
import {
    Alert,
    Linking,
    Modal,
    ScrollView,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {MtActivityIndicator, MtBodyText, MtButton, MtDateTimePicker} from '../../../components/ui-elements';
import { Icon } from 'react-native-elements'
import {
    getMovementDetail,
    getStoppageDetail,
    getVehicleLocations,
    getVehicleSummary
} from '../../../services/api/api-service';
import SegmentedControlTab from "react-native-segmented-control-tab";

export const LiveVehicleDetails = (props) => {
  const vehicleSelectedNo = props.vehicleNumber;
  //console.log("vehicle Number:- "+vehicleNo);
  const [customStyleIndex, setCustomStyleIndex] = useState(0);
  const handleCustomIndexSelect = (index) => {
    // Tab selection for custom Tab Selection
    setCustomStyleIndex(index);
  };
  const [vehiceleLocation, setVehicleLocation] = useState(null);
  const [shareLink, setshareLink] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [movementDetail, setMovementDetail] = useState([]);
  const [stoppageDetail, setStoppageDetail] = useState([]);
  const [movementReportStatus, setMovementReportStatus] = useState(true);
  const [stoppageDetailStatus, setStoppageDetailStatus] = useState(true);
  const _tableData = [];
  const _tableDataStoppage = [];
  useEffect(() => {
    if (vehicleSelectedNo) {
      getVehicleSummary(vehicleSelectedNo).then(response => {
          setVehicleLocation(response.location)
      })
      getMovementDetail(vehicleSelectedNo,'today').then(movementDetail =>
        {
          //console.log(movementDetail)
          for (let i = 0; i < movementDetail.movementDetails.length; i += 1) {
            _tableData.push({
              startLocation: movementDetail.movementDetails[i].startLocation,
              endLocation:movementDetail.movementDetails[i].endLocation,
              startTime: movementDetail.movementDetails[i].startTime,
              endTime: movementDetail.movementDetails[i].endTime,
              duration: movementDetail.movementDetails[i].duration,
              kms: movementDetail.movementDetails[i].kms,
            })
          }
          //console.log(movementDetail)
          setMovementDetail(_tableData)
          setMovementReportStatus(false)
        })
      getStoppageDetail(vehicleSelectedNo,'today').then(data => {
        //console.log("hello-"+data)
        for (let i = 0; i < data.stopDetails.length; i += 1) {
          _tableDataStoppage.push({
            location: data.stopDetails[i].location,
            startTime:data.stopDetails[i].startTime,
            endTime: data.stopDetails[i].endTime,
            duration: data.stopDetails[i].duration,
          })
        }
        //console.log("hello -"+_tableDataStoppage)
        setStoppageDetail(_tableDataStoppage)
        setStoppageDetailStatus(false)
      })
      getVehicleLocations(undefined,vehicleSelectedNo).then(data => {
        setshareLink(data[0].share_link)
      })
    }
  }, []);
  const openGoogleMap = () => {
    console.log("Opening google map")
    var url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination="+vehiceleLocation;
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }
  //console.log("travelStats:-"+travelStats.travelTime);
  return (
    <>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modelHeading}>
              <Text style={styles.modalText}>Share Location</Text>
            </View>
            <View style={styles.modelBody}>
              <MtBodyText style={{ ...styles.smallText, marginLeft: 10 }} >Set expiry date</MtBodyText>
              <MtDateTimePicker dateValue={new Date()} onChangehandler={(date) => console.log(date)} />
              <MtBodyText style={{fontSize: 10,color:'red', marginLeft: 10 }} >{shareLink}</MtBodyText>

              <MtButton title="Generate Link" btnWidth='95%' />
             </View>
            <View style={styles.modelFooter}>
              <TouchableHighlight
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.textStyle}>Cancel</Text>
              </TouchableHighlight>
            </View>

          </View>
        </View>
      </Modal>
      <View style={styles.Mainbox}>
        <View style={styles.cardColumn}>
          {/* <TouchableHighlight
            onPress={() => {
              setModalVisible(true);
            }}
            style={styles.listView}
          >
            <View >
              <Icon type="font-awesome-5" style={styles.iconButtonbox} name="share-alt" color={"#50a5f1"} size={16} />
              <MtBodyText style={styles.textbuttonbox} >Share Location</MtBodyText>
            </View>
          </TouchableHighlight> */}
          <TouchableHighlight
           onPress={props.nearestVehicleOnPress}
            style={styles.listView}
          >
            <View >
              <Icon type="font-awesome-5" style={styles.iconButtonbox} name="truck" color={"#50a5f1"} size={16} />
              <MtBodyText style={styles.textbuttonbox}>Nearest Vehicle</MtBodyText>
            </View>
          </TouchableHighlight>
          <TouchableOpacity
            onPress={() => {
              openGoogleMap();
            }}
            style={styles.listView}
          >

              <Icon type="fontisto" style={styles.iconButtonbox} name="navigate" color={"#50a5f1"} size={16} />
              <MtBodyText style={styles.textbuttonbox} >Navigate</MtBodyText>

          </TouchableOpacity>
        </View>
        <View style={styles.cardColumn}>
          <View style={styles.listView}>
          <MtBodyText  style={styles.iconButtonbox} >Distance</MtBodyText>
            <MtBodyText  style={styles.textbuttonbox} >{`${props.vehicleDistanceTravelled|| 0} km`}</MtBodyText>
          </View>
          <View style={styles.listView}>
          <MtBodyText  style={styles.iconButtonbox} >Travel Time</MtBodyText>
            <MtBodyText  style={styles.textbuttonbox}>{`${props.vehicleTravelTime|| 0} (hr/mm)`}</MtBodyText>
          </View>
          <View style={styles.listView}>
          <MtBodyText  style={styles.iconButtonbox} >Stopped Time</MtBodyText>
            <MtBodyText  style={styles.textbuttonbox} >{`${props.vehicleStopTime|| 0} (hr/mm)`}</MtBodyText>
          </View>
        </View>
        <View style={styles.titlehead}>
          <MtBodyText style={styles.textbuttonbox} >Today</MtBodyText>
        </View>
        <View style={styles.containerView}>
          <SegmentedControlTab
            values={['Movements', 'Stoppages']}
            selectedIndex={customStyleIndex}
            onTabPress={handleCustomIndexSelect}
            borderRadius={0}
            tabsContainerStyle={{
              height: 30,
              backgroundColor: '#F2F2F2'
            }}
            tabStyle={{
              backgroundColor: '#F2F2F2',
              borderWidth: 0,
              borderColor: 'transparent',
            }}
            activeTabStyle={{ backgroundColor: 'white', marginTop: 2 }}
            tabTextStyle={{ color: '#444444', fontWeight: 'bold', fontSize: 12, }}
            activeTabTextStyle={{ color: '#888888' }}
          />
          {customStyleIndex === 0 && (
            <ScrollView>
              {
                movementDetail.map((l,key) => (
                    <View style={tabstyles.listbox}key={key}>
                      <MtBodyText style={tabstyles.titleName}>Movement of {l.kms} km in {l.duration} mins</MtBodyText>
                      <MtBodyText style={tabstyles.smallText}><Icon type="font-awesome" name="map-marker" size={13} color="blue" />   {l.startTime}</MtBodyText>
                      <MtBodyText style={tabstyles.bigText}>{l.startLocation}</MtBodyText>
                      <MtBodyText style={tabstyles.smallText}><Icon type='entypo' name="dot-single" size={13} color="blue" />   {l.endTime}</MtBodyText>
                      <MtBodyText style={tabstyles.bigText}>{l.endLocation}</MtBodyText>
                    </View>
                ))
              }
              {movementReportStatus &&
                <MtActivityIndicator/>
              }
            </ScrollView>
          )}
          {customStyleIndex === 1 && (
            <ScrollView>
              {
                stoppageDetail.map((l,key) => (
                    <View style={tabstyles.listbox} key={key}>
                      <MtBodyText style={tabstyles.titleName}>Stopped for {l.duration} min</MtBodyText>
                      <MtBodyText style={tabstyles.smallText}><Icon type='entypo' name="dot-single" size={13} color="red" /> {l.endTime}</MtBodyText>
                      <MtBodyText style={tabstyles.bigText}>{l.location}</MtBodyText>
                    </View>
                ))
              }
              {stoppageDetailStatus &&
                <MtActivityIndicator/>
              }
            </ScrollView>
          )}
        </View>

      </View>
    </>
  );
};
const styles = StyleSheet.create({
  Mainbox: {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    marginTop: 10,
  },
  cardColumn:
  {
    width: '100%',
    height: 60,
    padding: 5,
    paddingTop: 10,
    flexDirection: 'row',
    borderBottomColor: '#d5d5d5',
    backgroundColor: "#fff",
    borderBottomWidth: 1,
  },
  listView:
  {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconButtonbox:
  {
    alignSelf: 'center',
    padding: 5,
  },
  textbuttonbox:
  {
    padding: 5,
    fontWeight: "bold",
  },
  titlehead:
  {
    width: '100%',
    height: 30,
    padding: 2,
    flexDirection: 'row',
    borderBottomColor: '#d5d5d5',
    backgroundColor: "#d5d5d5",
    borderBottomWidth: 1,
  },
  BoxView:
  {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    paddingBottom: 30,
  },
  containerView:
  {
    width: '100%',
    backgroundColor: "#fff",
  },
  //modelbox
  centeredView: {
    flex: 1,
    backgroundColor: "#0c01018c",
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 1,
    width: '80%',
    alignItems: "flex-start",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textStyle: {
    color: "#0039ae",
    fontWeight: "bold",
  },
  modalText: {
    marginBottom: 15,
    color: '#fff',
  },
  modelHeading:
  {
    width: '100%',
    height: 40,
    backgroundColor: '#0039ae',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 1,
  },
  modelBody:
  {
    paddingHorizontal: 1,
    paddingTop: 5,
    paddingBottom: 10,
  },
  modelFooter:
  {
    width: '100%',
    height: 30,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 10,
    alignItems: 'flex-end'
  },
});
const tabstyles = StyleSheet.create({
  headerText: {
    padding: 8,
    fontSize: 14,
    color: '#444444',
    textAlign: 'center',
  },
  tabContent: {
    color: '#444444',
    fontSize: 18,
    margin: 24,
  },
  seperator: {
    marginHorizontal: -10,
    alignSelf: 'stretch',
    borderTopWidth: 1,
    borderTopColor: '#888888',
    marginTop: 24,
  },
  tabStyle: {
    borderColor: '#D52C43',
  },
  activeTabStyle: {
    backgroundColor: '#D52C43',
  },
  listbox:
  {
    width: '100%',
    backgroundColor: '#fff',
    marginTop: 10,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#888888',
  },
  bigText:
  {
    fontSize: 13,
    fontWeight: 'bold',
    marginHorizontal: 20,
    fontFamily: 'montserrat-bold',
    padding: 2,
  },
  smallText:
  {
    fontSize: 11,
    color: '#5d5d5d',
    fontFamily: 'montserrat',
    padding: 2,
  },
  titleName:
  {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 2,
    marginBottom: 7,
    fontFamily: 'montserrat-bold',
  },

});