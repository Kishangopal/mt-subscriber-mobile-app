import React, {useState} from 'react';
import {MemberStyles} from '../../../styles';
import {MtBodyText} from '../../../components/ui-elements';

export const GetDay = (props) => {
    const [dayStatus,setDayStatus]=useState("Today")
    const currentdate=new Date(props.msDate)
    console.log(currentdate)
    const diff_ms = new Date().getTime()-new Date(currentdate).getTime();
    let days = Math.floor(diff_ms / (1000 * 60 * 60 * 24));
    return(
        <MtBodyText style={MemberStyles.notificationtime}> {days>1?"Last "+ days+" Day Ago":"Today"} </MtBodyText>
    );
};
