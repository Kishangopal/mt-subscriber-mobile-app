import React from 'react';
import {View} from 'react-native';
import {EzButton} from "../../../components/ui-elements/EzButton"
import {MemberStyles} from '../../../styles';
import {MtBodyText} from '../../../components/ui-elements';

export const VehicleRegisterStep3 = props => {
    const { values, handleSubmit, back } = props;
    return (
        <>
            <MtBodyText style={MemberStyles.subHeadingStyle}>Vehicle Details</MtBodyText>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Vehicle Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Vehicle Type:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleType}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Model No:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.modelNumber}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Chassis Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.chassisNumber}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Engine No:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.engineNo}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Load Capacity:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.loadCapacity}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Owner Name:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.ownerName}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Owner Contact Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.ownerContactNumber}</MtBodyText>
                </View>
            </View>
            <MtBodyText style={MemberStyles.subHeadingStyle}>Document Details</MtBodyText>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>PAN Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.panNumbe}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>RC Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.rcNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Vehicle Registration Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Nation Permit Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.nationPermitNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Nation Permit Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Nation Permit Expiry Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Insurance Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.insuranceNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Insurance Start Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>
            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Insurance Expiry Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Fitness Number:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.fitnessNumber}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Fitness Start Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>

            <View style={MemberStyles.rowflex}>
                <View style={MemberStyles.lableView}>
                    <MtBodyText style={MemberStyles.userText}>Fitness Expiry Date:</MtBodyText>
                </View>
                <View style={MemberStyles.lableData}>
                    <MtBodyText style={MemberStyles.userSubText}>{values.vehicleNumber}</MtBodyText>
                </View>
            </View>
            <EzButton
            buttonType='outline'
            OnPressHandler={back}
            title='Back'
        />
        <EzButton
            buttonType='outline'
            OnPressHandler={handleSubmit}
            title='Submit'
            buttonColor='#039BE5'
        />


        </>
    );
};