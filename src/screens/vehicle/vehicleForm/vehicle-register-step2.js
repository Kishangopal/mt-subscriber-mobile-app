import React from 'react';
import {MemberStyles} from '../../../styles';
import {EzButton} from "../../../components/ui-elements/EzButton"
import {MtBodyText, MtDateTimePicker, MtImagePicker, MtInput} from '../../../components/ui-elements';

export const VehicleRegisterStep2 = props => {
  const { values, handleChange, handleSubmit, back, errors, touched, handleBlur, next } = props;
  return (
     <>
      <MtBodyText style={MemberStyles.labeltext}>PAN Number</MtBodyText>
        <MtInput
          placeholder="PAN Number"
          name="panNumber"
          onChangeText={handleChange('panNumber')}
          value={values.panNumber}
          errorMessage={touched.panNumber && errors.panNumber}
          onBlur={handleBlur('panNumber')}
        />
         <MtBodyText style={MemberStyles.labeltext}>RC Number</MtBodyText>
        <MtInput
          placeholder="RC Number"
          name="rcNumber"
          onChangeText={handleChange('rcNumber')}
          value={values.rcNumber}
          errorMessage={touched.rcNumber && errors.rcNumber}
          onBlur={handleBlur('rcNumber')}
        />
        <MtBodyText style={MemberStyles.labeltext}>Vehicle Registration Date</MtBodyText>
        <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
        <MtBodyText style={MemberStyles.labeltext}>Nation Permit Number</MtBodyText>
        <MtInput
          placeholder="Nation Permit Number"
          name="nationPermitNumber"
          onChangeText={handleChange('nationPermitNumber')}
          value={values.nationPermitNumber}
          errorMessage={touched.nationPermitNumber && errors.nationPermitNumber}
          onBlur={handleBlur('nationPermitNumber')}
        />

         <MtBodyText style={MemberStyles.labeltext}>Nation Permit Date</MtBodyText>
         <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
        <MtBodyText style={MemberStyles.labeltext}>Nation Permit Expiry Date</MtBodyText>
        <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
         <MtBodyText style={MemberStyles.labeltext}>Insurance Number</MtBodyText>
        <MtInput
          placeholder="Insurance Number"
          name="insuranceNumber"
          onChangeText={handleChange('insuranceNumber')}
          value={values.insuranceNumber}
          errorMessage={touched.insuranceNumber && errors.insuranceNumber}
          onBlur={handleBlur('insuranceNumber')}
        />

         <MtBodyText style={MemberStyles.labeltext}>Insurance Start Date</MtBodyText>
         <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
        <MtBodyText style={MemberStyles.labeltext}>Insurance Expiry Date</MtBodyText>
        <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
         <MtBodyText style={MemberStyles.labeltext}>Fitness Number</MtBodyText>
        <MtInput
          placeholder="Fitness Number"
          name="fitnessNumber"
          onChangeText={handleChange('fitnessNumber')}
          value={values.fitnessNumber}
          errorMessage={touched.fitnessNumber && errors.fitnessNumber}
          onBlur={handleBlur('fitnessNumber')}
        />
        <MtBodyText style={MemberStyles.labeltext}>Fitness Start Date</MtBodyText>
        <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
        <MtBodyText style={MemberStyles.labeltext}>Fitness Expiry Date</MtBodyText>
        <MtDateTimePicker dateValue={new Date()} onChangehandler={(date)=>console.log(date)}/>
        <MtBodyText style={MemberStyles.headingTitleStyle}>Upload Document</MtBodyText>
        <MtBodyText style={MemberStyles.labeltext}>Pan Card</MtBodyText>
        <MtImagePicker/>
        <MtBodyText style={MemberStyles.labeltext}>RC</MtBodyText>
        <MtImagePicker/>
        <MtBodyText style={MemberStyles.labeltext}>National Permit </MtBodyText>
        <MtImagePicker/>
        <MtBodyText style={MemberStyles.labeltext}>Insurance</MtBodyText>
        <MtImagePicker/>
        <MtBodyText style={MemberStyles.labeltext}>Fitness</MtBodyText>
        <MtImagePicker/>
        <MtBodyText style={MemberStyles.labeltext}>Driving Licence</MtBodyText>
        <MtImagePicker/>
      <EzButton
        buttonType='outline'
        OnPressHandler={back}
        title='Back'
      />
      <EzButton
        buttonType='outline'
        OnPressHandler={next}
        title='Preview'
        buttonColor='#039BE5'
      />
          </>
    );
};