import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import _ from "lodash"
import { GlobalStyles, MemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import { MtBodyText, MtDropDown, MtButton } from '../../components/ui-elements';
import { Icon } from 'react-native-elements'
import { Formik } from 'formik';
import { getLocalData } from '../../services/global-storage';
import {MtSuccessMessage} from '../vehicle/vehicleForm'
import {getBusinessList, getSystemList, getVehicleActiveList, getUniqueLocations, getBusinessProfileView, createNeedLoad } from '../../services/api/api-service';
export const NeedLoad = () => {
    const [vehicleValue, setVehicleValue] = useState(null);
    const [vehicleList, setVehicleList] = useState([]);
    const [stateValue, setStateValue] = useState(null);
    const [stateList, setStateList] = useState([]);
    const [cityList, setCityList] = useState([]);
    const [cityValue, setCityValue] = useState(null);
    const [weightList, setWeightList] = useState([]);
    const [weightValue, setWeightValue] = useState(null);
    const [needDatas, setNeedDatas] = useState(null);
    const [successStatus, setSuccessStatus] = useState(false);
    const [packersAndMoversListCity,setPackersAndMoversListCity]=useState([]);
    const handleSubmit = (values, actions) => {
        const dataValue = { ...needDatas[0], ...values }
        console.log(dataValue);
        createNeedLoad(dataValue).then(result => {
            actions.setSubmitting(false);
            setSuccessStatus(true)
            Toast.show({
                text1: 'Successfully',
                text2: 'Need Load Post Successfully'
              });
        }).catch(alert);
        getBusinessList('Packers And Movers', cityValue).then(data => {
            if (!_.isEmpty(data)) {
                setPackersAndMoversListCity(data)
            }
            console.log(packersAndMoversListCity[0])
        })
    }
    useEffect(() => {
        getBusinessProfileView(getLocalData('userId')).then(d => {
            const getUserData = []
            d.forEach(element => {
                getUserData.push({
                    contactNumber: element.ownerPhone,
                    contactName: element.ownerName,
                    mtUserId: element.id
                })
            });
            setNeedDatas(getUserData)
        })
        getVehicleActiveList().then(d => {
            const allVehicleListData = []
            d.forEach(element => {
                allVehicleListData.push({ label: element.vehicleNumber, value: element.vehicleNumber })
            });
            setVehicleList(allVehicleListData)
        })
        getSystemList('state').then(data => {
            if (!_.isEmpty(data)) {
                setStateList(data)
            }
        }).catch(e => {
            console.log("Error in State list:", e);
        })
        getSystemList('Weight Type').then(data => {
            if (!_.isEmpty(data)) {
                setWeightList(data)
            }
        }).catch(e => {
            console.log("Error in Wight Type list:", e);
        })
        getSystemList('city').then(data => {
            if (!_.isEmpty(data)) {
                setCityList(data)
            }
        }).catch(e => {
            console.log("Error in State list:", e);
        })
    }, [])
    
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            <Formik
                initialValues={{ fromLocation: '', toLocation: '', vehicleNumber: '', itemWeight: '' }}
                //validationSchema={ProfileValidationSchema}
                onSubmit={(values, actions) => { handleSubmit(values, actions) }}
            >
                {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur, setFieldValue }) => (
                    <>
                        {!successStatus &&
                            <View style={MemberStyles.meanBox}>
                                <MtBodyText style={MemberStyles.headingTitleStyle}>Need Load</MtBodyText>
                                <MtBodyText style={MemberStyles.labeltext}>From</MtBodyText>
                                <MtDropDown
                                    items={cityList}
                                    onValueChangeHandler={value => {
                                        setCityValue(value)
                                        setFieldValue('fromLocation', value)
                                    }}
                                    name="fromLocation"
                                    selectedvalue={cityValue}
                                />
                                <MtBodyText style={MemberStyles.labeltext}>To</MtBodyText>
                                <MtDropDown
                                    items={stateList}
                                    onValueChangeHandler={value => {
                                        setStateValue(value)
                                        setFieldValue('toLocation', value)
                                    }}
                                    selectedvalue={stateValue}
                                    name="toLocation"
                                />
                                <MtBodyText style={MemberStyles.labeltext}>Vehicle Number</MtBodyText>
                                <MtDropDown
                                    items={vehicleList}
                                    onValueChangeHandler={value => {
                                        setVehicleValue(value)
                                        setFieldValue('vehicleNumber', value)
                                    }}
                                    selectedvalue={vehicleValue}
                                    name="vehicleNumber"
                                />
                                <MtBodyText style={MemberStyles.labeltext}>Weight</MtBodyText>
                                <MtDropDown
                                    items={weightList}
                                    onValueChangeHandler={value => {
                                        setWeightValue(value)
                                        setFieldValue('itemWeight', value)
                                    }}
                                    selectedvalue={weightValue}
                                    name="itemWeight"
                                />
                                <MtButton
                                    title="Submit"
                                    onPressHandler={handleSubmit}
                                    btnWidth='95%'
                                    isDisabled={!isValid || isSubmitting}
                                    loading={isSubmitting}
                                />
                            </View>
                        }
                        {successStatus &&
                            <MtSuccessMessage
                            headingMessage="Need Post Successfully Inserted" />
                        }
                    </>
                )}
            </Formik>
        </ScreenLayout>
    );
};