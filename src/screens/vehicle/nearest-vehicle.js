import React, {useEffect, useState} from 'react';
import {Dimensions, ScrollView, StyleSheet, Text, View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import {MtActivityIndicator, MtBodyText} from '../../components/ui-elements';
import MapView, {Callout, PROVIDER_GOOGLE} from 'react-native-maps';
import _ from "lodash"
import SlidingUpPanel from 'rn-sliding-up-panel';
import {getNearbyVehicle} from '../../services/api/api-service';

const { height } = Dimensions.get('window')
export const NearestVehicle = ({ route }) => {
    const [vehicleCordinates, setVehicleCordinates] = useState([]);
    const [cordinatesValue, setCordinatesValue] = useState({
        latitude: 26.4499,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
        longitude: 74.6399
    })
    const [cordinatesApiLoading, setCordinatesApiLoading] = useState(true);
    const vehicleSelectedNo = route.params.vehicleNo;
    useEffect(() => {
        if (vehicleSelectedNo) {
            const markers = [];
            getNearbyVehicle(vehicleSelectedNo).then(response => {
                console.log("API Response:", response)
                response.forEach(data => {
                    //console.log("API Response:",data)
                    markers.push({
                        title: data.vehicle_number,
                        device_id: data.device_id,
                        address: data.address,
                        coordinates: {
                            latitude: data.cordinate.lat,
                            longitude: data.cordinate.lng,
                        },
                        city: data.city,
                        state: data.state,
                        eta: data.eta,
                        distance: data.distance
                    })
                })
                setCordinatesValue({
                    latitude: response[0].cordinate.lat,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                    longitude: response[0].cordinate.lng
                })
                setVehicleCordinates(markers)
                setCordinatesApiLoading(false)
            })
        }
    }, [])
    return (
        <>
            <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
                {!_.isEmpty(vehicleCordinates) &&
                    <View style={styles.container}>
                        <View style={MemberStyles.headerReportBox}>
                            <MtBodyText style={MemberStyles.reportsDetaislHeading}>{vehicleSelectedNo}</MtBodyText>
                        </View>
                        <MapView
                            initialRegion={cordinatesValue}
                            provider={PROVIDER_GOOGLE}
                            zoomEnabled={true}
                            showsUserLocation={true}
                            minZoomLevel={5}
                            zoomControlEnabled={true}
                            style={styles.mapStyle}>
                            {

                                vehicleCordinates.map(item => (
                                    <MapView.Marker
                                        key={item.device_id}
                                        image={require('../../../assets/mover-truck.png')}
                                        coordinate={item.coordinates}
                                        title={item.title}
                                    >
                                        <Callout tooltip>
                                            <View>
                                                <View style={styles.bubble}>
                                                    <Text style={styles.title}>{item.title}</Text>
                                                    <Text style={styles.title}>Location:</Text>
                                                    <Text style={styles.valueTex}>{item.address}</Text>
                                                </View>
                                                <View style={styles.arrowBorder} />
                                                <View style={styles.arrow} />
                                            </View>
                                        </Callout>
                                    </MapView.Marker>
                                ))
                            }
                        </MapView>

                        <SlidingUpPanel ref={c => (_panel = c)}
                            draggableRange={{ top: height / 1.10, bottom: 150 }}
                            showBackdrop={false} >
                            <View style={styles.sliderContainer}>
                                <View style={{ flexDirection: 'row', backgroundColor: '#d5d5d5' }}>
                                    <View style={{ flex: 1, padding: 5, }}>
                                        <MtBodyText style={styles.textbuttonbox} >Vehicle Number</MtBodyText>
                                    </View>
                                    <View style={{ flex: 1, padding: 5, }}>
                                        <MtBodyText style={styles.textbuttonbox} >Distance</MtBodyText>
                                    </View>
                                    <View style={{ flex: 1, padding: 5, }}>
                                        <MtBodyText style={styles.textbuttonbox} >ETA</MtBodyText>
                                    </View>
                                </View>
                                <ScrollView>
                                    {
                                        vehicleCordinates.map(data => (
                                            <>
                                                <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#888888', }} key={data.key}>
                                                    <View style={{ flex: 1, padding: 5, }}>
                                                        <MtBodyText style={tabstyles.smallText}> {data.title}</MtBodyText>
                                                    </View>
                                                    <View style={{ flex: 1, padding: 5, }}>
                                                        <MtBodyText style={tabstyles.smallText}>{data.distance}</MtBodyText>
                                                    </View>
                                                    <View style={{ flex: 1, padding: 5, }}>
                                                        <MtBodyText style={tabstyles.smallText}>{data.eta}</MtBodyText>
                                                    </View>
                                                </View>
                                            </>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </SlidingUpPanel>
                    </View>
                }
                {cordinatesApiLoading &&
                    <MtActivityIndicator />
                }
            </ScreenLayout>
        </>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: -10,
    },
    sliderContainer: {
        flex: 1,
        backgroundColor: 'white',
    },
    searchtextbox:
    {
        flex: 1,
        padding: 0,
        fontSize: 15,
        fontFamily: 'montserrat',
        alignContent: 'flex-start'
    },
    mapStyle: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    searchBox: {
        flex: 1,
        position: 'absolute',
        marginTop: 20,
        flexDirection: "row",
        backgroundColor: '#fff',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 1,
        padding: 0,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    // Callout bubble
    bubble: {
        flexDirection: 'column',
        alignSelf: 'flex-start',
        backgroundColor: '#fff',
        borderRadius: 6,
        width: 250,
        padding: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 8,
    },
    //Arrow betow the bubble
    arrow: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        borderTopColor: '#fff',
        borderWidth: 16,
        alignSelf: 'center',
        marginTop: -32,
    },
    arrowBorder: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        borderTopColor: '#007a87',
        borderWidth: 16,
        alignSelf: 'center',
        margin: -0.5,
    },
    title:
    {
        fontSize: 16,
        marginBottom: 5,
        fontWeight: 'bold',
    },
    valueText:
    {
        fontSize: 10,

    },
    titlehead:
    {
        width: '100%',
        height: 30,
        padding: 2,
        flexDirection: 'row',
        borderBottomColor: '#d5d5d5',
        backgroundColor: "#d5d5d5",
        borderBottomWidth: 1,
    },
    textbuttonbox:
    {
        padding: 5,
        fontWeight: "bold",
    },
});
const tabstyles = StyleSheet.create({
    headerText: {
        padding: 8,
        fontSize: 14,
        color: '#444444',
        textAlign: 'center',
    },
    tabContent: {
        color: '#444444',
        fontSize: 18,
        margin: 24,
    },
    seperator: {
        marginHorizontal: -10,
        alignSelf: 'stretch',
        borderTopWidth: 1,
        borderTopColor: '#888888',
        marginTop: 24,
    },
    tabStyle: {
        borderColor: '#D52C43',
    },
    activeTabStyle: {
        backgroundColor: '#D52C43',
    },
    listbox:
    {
        width: '100%',
        backgroundColor: '#fff',
        marginTop: 10,
        padding: 10,
    },
    bigText:
    {
        fontSize: 10,
        fontWeight: 'bold',
        fontFamily: 'montserrat-bold',
        padding: 2,
    },
    smallText:
    {
        fontSize: 12,
        color: '#5d5d5d',
        fontFamily: 'montserrat',
        marginHorizontal: 15,
        padding: 2,
    },
    titleName:
    {
        fontSize: 16,
        fontWeight: 'bold',
        marginHorizontal: 2,
        marginBottom: 7,
        fontFamily: 'montserrat-bold',
    },

});