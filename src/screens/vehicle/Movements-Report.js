import React, {useEffect, useState} from 'react';
import {Dimensions, ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import {MtActivityIndicator, MtBodyText, MtDropDown} from '../../components/ui-elements';
import MapView from 'react-native-maps';
import { Icon } from 'react-native-elements'
import _ from "lodash"
import MapViewDirections from 'react-native-maps-directions';
import SlidingUpPanel from 'rn-sliding-up-panel';
import {getMovementDetail, getSystemList} from '../../services/api/api-service';
import { DefaultScreen } from '../../screens/vehicle/vehicleForm';
const { height } = Dimensions.get('window')
export const MovementsReports = ({ route }) => {
  const [reportsTypeValue, setReportsTypeValue] = useState('today');
  const [kmsValue, setKmsValue] = useState(0);
  const [vehicleStartCordinates, setVehicleStartCordinates] = useState({
    latitude: 26.4499,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
    longitude: 74.6399
  });
  const [reportsTypeList, setReportsTypeList] = useState([]);
  const [movementDetailApiLoading, setMovementDetailApiLoading] = useState(false);
  const [cordinateValue, setCordinateValue] = useState([]);
  const vehicleSelectedNo = route.params.vehicleNo;
  const [tableData, setTableData] = useState([]);
  const _tableData = [];
  let _mapView = React.createRef();
  const GOOGLE_MAPS_APIKEY = 'AIzaSyBe5SZ89DkOwUC2jxo9XNP9mUJk0u7kJPA';

  const animate = () => {
    if(_mapView.current){
      _mapView.current.animateToRegion(vehicleStartCordinates, 1000);
    }
  }
  useEffect(() => {
    getSystemList('ReportFilter').then(setReportsTypeList)
    if (vehicleSelectedNo) {
      let timeVal = 0;
      getMovementDetail(vehicleSelectedNo, reportsTypeValue).then(movementDetail => {
        console.log(movementDetail)
        for (let i = 0; i < movementDetail.movementDetails.length; i += 1) {
          _tableData.push({
            startLocation: movementDetail.movementDetails[i].startLocation,
            endLocation: movementDetail.movementDetails[i].endLocation,
            startTime: movementDetail.movementDetails[i].startTime,
            endTime: movementDetail.movementDetails[i].endTime,
            duration: movementDetail.movementDetails[i].duration,
            kms: movementDetail.movementDetails[i].kms,
            cordinates: [{
              latitude: movementDetail.movementDetails[i].startCordinates[0],
              longitude: movementDetail.movementDetails[i].startCordinates[1],
            },
            {
              latitude: movementDetail.movementDetails[i].endCordinates[0],
              longitude: movementDetail.movementDetails[i].endCordinates[1],
            }]
          })
          timeVal = timeVal + parseFloat(movementDetail.movementDetails[i].kms);
        }
        
        setTableData(_tableData)
        setKmsValue(Math.round(timeVal))
        //console.log(tableData)
        setMovementDetailApiLoading(true)
      })
      
    }
  }, [reportsTypeValue])
  const setDirectionHendler = (cordinateValues) => {
    setVehicleStartCordinates({
      latitude: cordinateValues[0].latitude,
      latitudeDelta: .005,
      longitudeDelta: .005,
      longitude: cordinateValues[0].longitude,
    })
    setCordinateValue(cordinateValues)
    console.log(cordinateValues)
    animate()
  }
  return (
    <>
         <View style={MemberStyles.headerReportBox}>
                <MtBodyText style={MemberStyles.reportsDetaislHeading}>{vehicleSelectedNo}</MtBodyText>
                <MtDropDown
                  items={reportsTypeList}
                  onValueChangeHandler={value => {
                    setMovementDetailApiLoading(false)
                    setReportsTypeValue(value)
                  }}
                  selectedvalue={reportsTypeValue}
                  name="timeDropDown"
                />
              </View>
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
          {movementDetailApiLoading &&
            <View style={styles.container}>
              <MapView
                ref={ _mapView}
                initialRegion={vehicleStartCordinates}
                zoomEnabled={true}
                showsUserLocation={true}
                minZoomLevel={5}
                zoomControlEnabled={true}
                style={styles.mapStyle}>
                <MapViewDirections
                  origin={cordinateValue[0]}
                  destination={cordinateValue[1]}
                  apikey={GOOGLE_MAPS_APIKEY}
                  strokeWidth={3}
                  strokeColor="hotpink"
                />
              </MapView>
              <SlidingUpPanel ref={c => (_panel = c)}
                    draggableRange={{top: height/1.10, bottom: 300}}
                    showBackdrop={true}>
                <View style={styles.sliderContainer}>
                  <View style={{ flexDirection: 'row', backgroundColor: '#d5d5d5' }}>
                    <View style={{ flex: 3 }}>
                      <MtBodyText style={styles.textbuttonbox} >{vehicleSelectedNo} Movement({tableData.length})</MtBodyText>
                    </View>
                    <View style={{ flex: 1 }}>
                      <MtBodyText style={styles.textbuttonbox} >{kmsValue} KMS</MtBodyText>
                    </View>
                  </View>
                  {!_.isEmpty(tableData) &&
                  <ScrollView>
                  {
                    tableData.map((l,key) => (
                      <View key={key}>
                        <TouchableOpacity onPress={() => setDirectionHendler(l.cordinates)}>
                          <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#888888', }} key={l.key}>
                            <View style={{ flex: 3 }}>
                              <View style={tabstyles.listbox}>
                                <MtBodyText style={tabstyles.bigText}><Icon type="entypo" name="dot-single" size={13} color="green" />{l.startLocation}</MtBodyText>
                                <MtBodyText style={tabstyles.smallText}>{l.startTime}</MtBodyText>
                                <MtBodyText style={tabstyles.bigText}><Icon type="entypo" name="dot-single" size={13} color="red" />{l.endLocation}</MtBodyText>
                                <MtBodyText style={tabstyles.smallText}>{l.endTime}</MtBodyText>
                              </View>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                              <MtBodyText style={tabstyles.smallText}>{l.duration} S</MtBodyText>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                              <MtBodyText style={tabstyles.smallText}>{l.kms} KMS</MtBodyText>
                            </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                    ))
                  }
                  </ScrollView>
                 }
                  {_.isEmpty(tableData) &&
                     <DefaultScreen
                     headingMessage="Movements Reports Data Not Available"
                     subheading="Search Data Not Available Right Now" />
                  }
                </View>
              </SlidingUpPanel>
            </View>
          }
          {!movementDetailApiLoading &&
            <MtActivityIndicator />
          }
        </ScreenLayout>


    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: -10,
  },
  sliderContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  searchtextbox:
  {
    flex: 1,
    padding: 0,
    fontSize: 15,
    fontFamily: 'montserrat',
    alignContent: 'flex-start'
  },
  mapStyle: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  searchBox: {
    flex: 1,
    position: 'absolute',
    marginTop: 20,
    flexDirection: "row",
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 1,
    padding: 0,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  // Callout bubble
  bubble: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 6,
    width: 250,
    padding: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 8,
  },
  //Arrow betow the bubble
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#fff',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#007a87',
    borderWidth: 16,
    alignSelf: 'center',
    margin: -0.5,
  },
  title:
  {
    fontSize: 16,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  valueText:
  {
    fontSize: 10,

  },
  titlehead:
  {
    width: '100%',
    height: 30,
    padding: 2,
    flexDirection: 'row',
    borderBottomColor: '#d5d5d5',
    backgroundColor: "#d5d5d5",
    borderBottomWidth: 1,
  },
  textbuttonbox:
  {
    padding: 5,
    fontWeight: "bold",
  },
});
const tabstyles = StyleSheet.create({
  headerText: {
    padding: 8,
    fontSize: 14,
    color: '#444444',
    textAlign: 'center',
  },
  tabContent: {
    color: '#444444',
    fontSize: 18,
    margin: 24,
  },
  seperator: {
    marginHorizontal: -10,
    alignSelf: 'stretch',
    borderTopWidth: 1,
    borderTopColor: '#888888',
    marginTop: 24,
  },
  tabStyle: {
    borderColor: '#D52C43',
  },
  activeTabStyle: {
    backgroundColor: '#D52C43',
  },
  listbox:
  {
    width: '100%',
    backgroundColor: '#fff',
    marginTop: 10,
    padding: 10,
  },
  bigText:
  {
    fontSize: 10,
    fontWeight: 'bold',
    fontFamily: 'montserrat-bold',
    padding: 2,
  },
  smallText:
  {
    fontSize: 10,
    color: '#5d5d5d',
    fontFamily: 'montserrat',
    marginHorizontal: 15,
    padding: 2,
  },
  titleName:
  {
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 2,
    marginBottom: 7,
    fontFamily: 'montserrat-bold',
  },

});