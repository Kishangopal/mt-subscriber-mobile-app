import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import {MtBodyText, MtButton} from '../../components/ui-elements';
import { Icon } from 'react-native-elements'

const list = [
  {
    id: 1,Vehicle: "RJ01FS5356", vehicletype: "Pickup", Location: "Ajmer,Rajasthan 305001",destination: "All India",contactName:"Rakesh Sharma",contactNumber:"9101525155",
  },
]
export const VehicleTypeSearch =({ navigation }) => {
  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
    <View style={MemberStyles.cardHeadBox}>
      <MtBodyText style={MemberStyles.labelHeadtext}>Result 1 </MtBodyText>
    </View>
    {
  list.map((item, i) => (
     <View style={MemberStyles.cardBox}>
    <MtBodyText style={MemberStyles.cardTitleStyle}>{item.Vehicle}({item.vehicletype})</MtBodyText>
    <View style={MemberStyles.cardbodystyle}>
      <MtBodyText style={MemberStyles.cardtext}><Icon type="font-awesome-5" name="map-marker-alt" size={16} color="black" /> {item.Location}</MtBodyText>
      <MtBodyText style={MemberStyles.cardtext}><Icon type="font-awesome" name="location-arrow" size={16} color="black" /> {item.destination}</MtBodyText>
      <MtBodyText style={MemberStyles.cardtext}><Icon type="font-awesome" name="user" size={16} color="black" /> {item.contactName}</MtBodyText>
     <MtButton
                          title="Find Contact"
                          btnWidth='95%'
                      />
    </View>
    </View>
     ))}
    </ScreenLayout>
  );
  };