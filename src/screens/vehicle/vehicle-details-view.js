import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {VehicleRegisterStep3} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';

export const EditVehicleDetails = () => (
  <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
       <View style={MemberStyles.meanBox}>
          <VehicleRegisterStep3/>
       </View>
    </ScreenLayout>
);