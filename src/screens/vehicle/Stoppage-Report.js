import React, {useEffect, useState} from 'react';
import _ from "lodash"
import {ScrollView, StyleSheet, View} from 'react-native';
import {Row, Table} from 'react-native-table-component';
import {MemberStyles} from '../../styles';
import {MtActivityIndicator, MtBodyText, MtDropDown} from '../../components/ui-elements';
import {getStoppageDetail, getSystemList} from '../../services/api/api-service';
import { DefaultScreen } from '../../screens/vehicle/vehicleForm';
export const StoppageReport = ({ route }) => {
  const vehicleSelectedNo = route.params.vehicleNo;
  const [reportsTypeValue, setReportsTypeValue] = useState('today');
  const [reportsTypeList, setReportsTypeList] = useState([]);
  // const [stoppageDetail, setStoppageDetail] = useState([]);
  const [tableData, setTableData] = useState([]);
  const _tableData = [];
  const [stoppageDetailApiLoading, setStoppageDetailApiLoading] = useState(false);
  useEffect(() => {
    getSystemList('ReportFilter').then(setReportsTypeList)
    if (vehicleSelectedNo) {
      getStoppageDetail(vehicleSelectedNo, reportsTypeValue).then(stoppageDetail => {
        //console.log(stoppageDetail)
        for (let i = 0; i < stoppageDetail.stopDetails.length; i += 1) {
          const rowData = [];
          rowData.push(stoppageDetail.stopDetails[i].location);
          rowData.push(stoppageDetail.stopDetails[i].startTime);
          rowData.push(stoppageDetail.stopDetails[i].endTime);
          rowData.push(stoppageDetail.stopDetails[i].duration);
          _tableData.push(rowData);
        }
        setTableData(_tableData)
      })
      setStoppageDetailApiLoading(true)
    }
  }, [reportsTypeValue])
  const state = {
    tableHead: ['Location', 'Start Time', 'End Time', 'Duration'],
    widthArr: [150, 80, 80, 80]
  }


  return (
    <>
      <View style={MemberStyles.headerReportBox}>
        <MtBodyText style={MemberStyles.reportsDetaislHeading}>{vehicleSelectedNo}</MtBodyText>

        <MtDropDown
          items={reportsTypeList}
          onValueChangeHandler={value => {
            setReportsTypeValue(value)
            setStoppageDetailApiLoading(true)
          }}
          selectedvalue={reportsTypeValue}
          name="timeDropDown"
        />
      </View>
      {stoppageDetailApiLoading &&
      <ScrollView style={MemberStyles.scrollViewBox}>
        {!_.isEmpty(tableData) &&
          <View>
            <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text} />
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                {
                  tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data={rowData}
                      widthArr={state.widthArr}
                      style={[styles.row, index % 2 && { backgroundColor: '#F7F6E7' }]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        }
        {_.isEmpty(tableData) &&
                     <DefaultScreen
                     headingMessage="Stoppage Reports Data Not Available"
                     subheading="Search Data Not Available Right Now" />
        }
      </ScrollView>
      }
      {!stoppageDetailApiLoading &&
          <MtActivityIndicator />
      }
    </>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  header: { height: 40, backgroundColor: '#A4B0BD' },
  text: { textAlign: 'center', fontSize: 10, fontWeight: '100', padding: 1, },
  dataWrapper: { marginTop: -1 },
  row: {backgroundColor: '#EAF0F1', }
});