import React, {useEffect, useState} from 'react';
import _ from "lodash"
import {ScrollView, StyleSheet, View} from 'react-native';
import {Row, Table} from 'react-native-table-component';
import {MemberStyles} from '../../styles';
import {MtActivityIndicator, MtBodyText, MtDropDown} from '../../components/ui-elements';
import {getHourlyDetail, getSystemList} from '../../services/api/api-service';
import { DefaultScreen } from '../../screens/vehicle/vehicleForm';
export const HourlyReports = ({ route }) => {
  const [reportsTypeValue, setReportsTypeValue] = useState('today');
  const [reportsTypeList, setReportsTypeList] = useState([]);
  const [hourlyDetailsListApiLoading, setHourlyDetailsListApiLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const _tableData = [];
  const vehicleSelectedNo = route.params.vehicleNo;
  // console.log(hourlyDetailsList)
  useEffect(() => {
    getSystemList('ReportFilter').then(setReportsTypeList)
    if (vehicleSelectedNo) {
      getHourlyDetail(vehicleSelectedNo,reportsTypeValue).then(data =>{
        //console.log(data)
        for (let i = 0; i < data.length; i += 1) {
          const rowData = [];
          rowData.push(data[i].startTimeFormat);
          rowData.push(data[i].endTimeFormat);
          rowData.push(data[i].distance);
          rowData.push(data[i].avgSpeed);
          rowData.push(data[i].location);
        _tableData.push(rowData);
        }
        setTableData(_tableData)
        setHourlyDetailsListApiLoading(true)
      })
    }
  }, [reportsTypeValue])
  const state = {
    tableHead: ['Start Time', 'End Time', 'Distance (Kms)', 'Avg.Speed (km/hr)', 'Loaction'],
    widthArr: [70, 70, 40, 40, 180]
  }
  return (
    <>
      <View style={MemberStyles.headerReportBox}>
        <MtBodyText style={MemberStyles.reportsDetaislHeading}>{vehicleSelectedNo}</MtBodyText>
        <MtDropDown
          items={reportsTypeList}
          onValueChangeHandler={value => {
            setHourlyDetailsListApiLoading(false)
            setReportsTypeValue(value)
          }}
          selectedvalue={reportsTypeValue}
          name="timeDropDown"
        />
      </View>
      <ScrollView style={MemberStyles.scrollViewBox}>
      {!hourlyDetailsListApiLoading &&
          <View>
            <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text} />
            </Table>
            {!_.isEmpty(tableData) &&
            <ScrollView style={styles.dataWrapper}>
                  <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                   {
                     tableData.map((rowData, index) => (
                       <Row
                         key={index}
                         data={rowData}
                         widthArr={state.widthArr}
                         style={[styles.row, index % 2 && { backgroundColor: '#F7F6E7' }]}
                         textStyle={styles.text}
                       />
                     ))
                   }
                   
                 </Table>
            </ScrollView>
            }
            {_.isEmpty(tableData) &&
                    <DefaultScreen
                    headingMessage="Hourly Reports Data Not Available"
                    subheading="Search Data Not Available Right Now" />
            }
          </View>
        }
        {hourlyDetailsListApiLoading &&
          <MtActivityIndicator />
        }
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: '#A4B0BD' },
  text: { textAlign: 'center', fontSize: 10, fontWeight: '100', padding: 1, },
  dataWrapper: { marginTop: -1 },
  row: {backgroundColor: '#EAF0F1' }
});