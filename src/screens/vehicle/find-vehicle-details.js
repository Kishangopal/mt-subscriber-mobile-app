import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtBodyText} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {getVehicleDetailsList} from '../../services/api/api-service'

export const FindVehicleDetails = ({ route }) => {
    const [vehicleList, setVehicleList] = useState([]);
    useEffect(()=>{
      getVehicleDetailsList('RJ01GS5685').then(setVehicleList)
    },[])
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            {
            vehicleList.map(item => (
            <View style={MemberStyles.meanBox} key={item.id}>
     <MtBodyText style={MemberStyles.subHeadingStyle}>Vehicle Details</MtBodyText>
         <View style={MemberStyles.rowflex}>
             <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Vehicle Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.vehicleNumber}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Vehicle Type:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.vehicleType}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Model No:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.modelNumber}</MtBodyText>
              </View>
              </View>

              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Chassis Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.chassisNumber}</MtBodyText>
              </View>
              </View>

              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Engine No:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.engineNo}</MtBodyText>
              </View>
              </View>

              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Load Capacity:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.loadCapacity}</MtBodyText>
              </View>
              </View>

              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Owner Name:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.ownerName}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Owner Contact Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.ownerNumber}</MtBodyText>
              </View>
              </View>
              <MtBodyText style={MemberStyles.subHeadingStyle}>Document Details</MtBodyText>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>PAN Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.penNumber}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>RC Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.rcNumber}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Vehicle Reg. Date:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.vehicleRegDate}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Nat. Permit Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.nationPermitNumber}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Insurance Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.insuranceNumber}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Fitness Number:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.fitnessNumber}</MtBodyText>
              </View>
              </View>
     </View>
             ))}
        </ScreenLayout>
    )};