import React from "react"
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import EditMultiStep from './Edit-Multi-Step';

export const EditVehicleDetails = () => {
  return (
    <>
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
        <View style={MemberStyles.meanBox}>
          <EditMultiStep />
        </View>
      </ScreenLayout>
    </>
  )
};
