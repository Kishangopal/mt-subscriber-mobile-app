import React from "react"
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import MultiStep from './MultiStep';

export const VehicleRegister = () => {
  return (
    <>
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
        <View style={MemberStyles.meanBox}>
          <MultiStep />
        </View>
      </ScreenLayout>
    </>
  )
};