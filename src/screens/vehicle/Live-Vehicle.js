import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import {AppRoute} from '../../navigation/app-routes';
import {ScreenLayout} from '../screen-layout';
import {MemberStyles} from '../../styles';
import {MtActivityIndicator, MtBodyText} from '../../components/ui-elements';
import {LiveVehicleDetails} from './vehicleForm';
import MapView, {Callout, PROVIDER_GOOGLE} from 'react-native-maps';
import _ from "lodash"
import SlidingUpPanel from 'rn-sliding-up-panel';
import {getVehicleSummary} from '../../services/api/api-service';

const { height } = Dimensions.get('window')
export const LiveVehicle = (props) => {
  const vehicleNo = props.route.params.vehicleId;
  //console.log(vehicleNo)
  const [vehicleCordinates, setVehicleCordinates] = useState([]);
  const [vehicleApiLoading, setVehicleApiLoading] = useState(true);
  useEffect(() => {
    const markers = [];
    if (vehicleNo) {
      getVehicleSummary(vehicleNo).then(response => {
        //console.log("API Response:",response)
        //console.log(vehicleNo)
        //console.log("API Response:",response.location);
        markers.push({
          coordinates: {
            latitude: response.cordinate[0],
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
            longitude: response.cordinate[1]
          },
          address: response.location,
          lastChecked: response.lastChecked,
          lastStatus: response.lastStatus,
          speed: response.speed,
          distanceTravelled: response.distanceTravelled,
          stopTime: response.stopTime,
          travelTime: response.travelTime
        })
        setVehicleCordinates(markers)
        setVehicleApiLoading(false)
      })
    }
  }, [])
  //console.log(vehicleCordinates);
  return (
    <ScreenLayout>
      <View>
        {!_.isEmpty(vehicleCordinates) &&
          <View>

            {
              vehicleCordinates.map((item,key) => (
                <View key={key}>
                  <View style={styles.container} >
                    <View style={MemberStyles.headerMapBox}>
                      <MtBodyText style={{ fontSize: 10, color: '#5d5d5d', marginLeft: 10, }}>Last Location</MtBodyText>
                      <MtBodyText style={{ fontSize: 16, fontWeight: 'bold', marginLeft: 10, color: '#0073e6' }}>{vehicleNo}</MtBodyText>
                      <MtBodyText style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 10, }}>{item.address}</MtBodyText>
                      <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 4 }}>
                        <View style={{ flex: 1 }}>
                          <MtBodyText style={{ fontSize: 13, color: 'red', marginLeft: 10, alignSelf: 'flex-start' }}>{item.lastStatus}</MtBodyText>

                        </View>
                        <View style={{ flex: 1 }}>
                          <MtBodyText style={{ fontSize: 13, color: '#5d5d5d', marginRight: 13, alignSelf: 'flex-end' }}>{item.lastChecked}</MtBodyText>
                        </View>

                      </View>
                    </View>

                    <MapView
                    provider={PROVIDER_GOOGLE}
                      initialRegion={item.coordinates}
                      zoomEnabled={true}
                      showsUserLocation={true}
                      minZoomLevel={5}
                      zoomControlEnabled={true}
                      style={styles.mapStyle}>

                      <MapView.Marker
                        image={require('../../../assets/mover-truck.png')}
                        coordinate={item.coordinates}
                      >
                        <Callout tooltip>
                          <View>
                            <View style={styles.bubble}>
                              <Text style={styles.valueTex}>Speed</Text>
                              <Text style={styles.valueTex}>{item.speed} </Text>
                            </View>
                            <View style={styles.arrowBorder} />
                            <View style={styles.arrow} />
                          </View>
                        </Callout>

                      </MapView.Marker>
                    </MapView>
                    {/* <Button title='Show Reports' onPress={() => _panel.show()} /> */}
                    <SlidingUpPanel
                    ref={c => (_panel = c)}
                    draggableRange={{top: height/1.10, bottom: 300}}
                    showBackdrop={true}
                    // ref={c => _panel = c}
                    >
                      <View style={styles.sliderContainer}>
                        <LiveVehicleDetails
                          vehicleNumber={vehicleNo}
                          vehicleSpeed={item.speed}
                          vehicleDistanceTravelled={item.distanceTravelled}
                          vehicleStopTime={item.stopTime}
                          vehicleTravelTime={item.travelTime}
                          nearestVehicleOnPress={() =>
                            props.navigation.push(AppRoute.NEAREST_VEHICLE, { vehicleNo: vehicleNo })}
                        />
                        {/* <Button title='Hide' onPress={() => _panel.hide()} /> */}
                      </View>
                    </SlidingUpPanel>
                  </View>

                </View>
              ))
            }
          </View>
        }
        {vehicleApiLoading &&
          <MtActivityIndicator />
        }
      </View>
    </ScreenLayout>

  )
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: -10,
  },
  sliderContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    height: 200,
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 15,
  },
  searchtextbox:
  {
    flex: 1,
    padding: 0,
    fontSize: 15,
    fontFamily: 'montserrat',
    alignContent: 'flex-start'
  },
  mapStyle: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  searchBox: {
    flex: 1,
    position: 'absolute',
    marginTop: 20,
    flexDirection: "row",
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 1,
    padding: 0,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  // Callout bubble
  bubble: {
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 6,
    width: 100,
    padding: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 8,
  },
  //Arrow betow the bubble
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#fff',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#007a87',
    borderWidth: 16,
    alignSelf: 'center',
    margin: -0.5,
  },
  title:
  {
    fontSize: 16,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  valueText:
  {
    fontSize: 10,

  },
});