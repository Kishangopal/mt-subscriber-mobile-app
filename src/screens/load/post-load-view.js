import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Dimensions, Linking, View } from 'react-native';
import { GlobalStyles, MemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import _ from 'lodash'
import { MtActivityIndicator, MtBodyText, MtButton, MtLink } from '../../components/ui-elements';
import { getPostLoad, getSystemList } from '../../services/api/api-service';
export const PostLoadView = ({ route }) => {
  const [postListView, setPostListView] = useState({});
  const [profileStatus, setProfileStatus] = useState(true);
  const postId = route.params.postId;
  useEffect(() => {
    getSystemList('city').then(citylist => {
      getPostLoad(postId).then(data => {
        if (!_.isEmpty(data)) {
          data.fromCityName = _.get(_.find(citylist, { value: data.pickupLocation }), "label", data.pickupLocation)
          data.destinationCityName = _.get(_.find(citylist, { value: data.destination }), "label", data.destination)
          setPostListView(data)
          setProfileStatus(false)
        }
      })
    }).catch(e => {
      console.log("Error in State list:", e);
    })
      .catch(e => {
        console.log("Error in Transporter list:", e);
      })
  }, [])
  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      {!profileStatus &&
        <View style={MemberStyles.meanBox}>
          <MtBodyText style={MemberStyles.headingTitleStyle}>Require ({postListView.vehicleType})</MtBodyText>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Goods Type:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.goodsType}</MtBodyText>
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Loads Type:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.loadsType}</MtBodyText>
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Contact Name:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.contactName}</MtBodyText>
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Contact Number:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('tel:' + postListView.contactNumber); }}>{postListView.contactNumber.length > 0 ? postListView.contactNumber : "N/A"}</MtLink >
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Pickup Location:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.fromCityName}</MtBodyText>
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Destination:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.destinationCityName}</MtBodyText>
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Special Instruction:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.specialInstruction}</MtBodyText>
            </View>
          </View>
          <View style={MemberStyles.rowflex}>
            <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Item Weight:</MtBodyText>
            </View>
            <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{postListView.itemWeight}</MtBodyText>
            </View>
          </View>
          <MtButton title="Call" onPressHandler={() => { Linking.openURL('tel:'+`${_.get(postListView, "contactNumber")?postListView.contactNumber:""}`); }} btnWidth='95%' />
        </View>
      }
      {profileStatus &&
        <MtActivityIndicator />
      }
    </ScreenLayout>
  );
};