import React, { useEffect, useState } from 'react';
import {
    SafeAreaView, ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import _ from "lodash"
import { GlobalStyles, MemberStyles,NonMemberStyles } from '../../styles';
import { ScreenLayout } from '../screen-layout';
import { LoadValidationSchema } from '../../components/validations'
import { MtBodyText, MtDropDown, MtButton, MtInput, MtActivityIndicator } from '../../components/ui-elements';
import { Formik } from 'formik';
import { getBusinessProfileView, getSystemList, getSystemListType, createLoadPost } from '../../services/api/api-service';
import { getLocalData } from '../../services/global-storage';
import { MtSuccessMessage } from '../vehicle/vehicleForm'
import Autocomplete from 'react-native-autocomplete-input';
import Toast from 'react-native-toast-message';
export const PostLoad = () => {
    const [cityList, setCityList] = useState([]);
    const [apiLoading, setApiLoading] = useState(true);
    const [goodsTypeValue, setGoodsTypeValue] = useState(null);
    const [goodsTypeList, setGoodsTypeList] = useState([]);
    const [vehicleTypeValue, setVehicleTypeValue] = useState(null);
    const [vehicleTypeList, setVehicleTypeList] = useState([]);
    const [loadsTypeValue, setLoadsTypeValue] = useState(null);
    const [loadsTypeList, setLoadsTypeList] = useState([]);
    const [weightValue, setWeightValue] = useState(null);
    const [weightList, setWeightList] = useState([]);
    const [userDataString, setUserDataString] = useState([]);
    const [successStatus, setSuccessStatus] = useState(false);
    //====================AutoFilter=================================
    const [films, setFilms] = useState([]);
    // For Filtered Data
    const [filteredFilms, setFilteredFilms] = useState([]);
    const [filteredDestination, setFilteredDestination] = useState([]);
    // For Selected Data
    const [selectedValue, setSelectedValue] = useState({});
    const [selectedDestinationValue, setSelectedDestinatioValue] = useState({});
    const findFilm = (query) => {
        // Method called every time when we change the value of the input
        if (query) {
            // Making a case insensitive regular expression
            const regex = new RegExp(`${query.trim().indexOf(")") < 0 ? query.trim().indexOf("(") > 0 ? query.trim() + ")" : query.trim() : query.trim() + ")"}`, 'i');
            // Setting the filtered film array according the query
            setFilteredFilms(
                //console.log(cityList[0].label)
                cityList.filter((cityList) => cityList.label.search(regex) >= 0)
            );
        } else {
            // If the query is null then return blank
            setFilteredFilms([]);
        }
    };
    const findDestination = (query) => {
        // Method called every time when we change the value of the input
        if (query) {
            // Making a case insensitive regular expression
            const regex = new RegExp(`${query.trim().indexOf(")") < 0 ? query.trim().indexOf("(") > 0 ? query.trim() + ")" : query.trim() : query.trim() + ")"}`, 'i');
            // Setting the filtered film array according the query
            setFilteredDestination(
                //console.log(cityList[0].label)
                cityList.filter((cityList) => cityList.label.search(regex) >= 0)
            );
        } else {
            // If the query is null then return blank
            setFilteredDestination([]);
        }
    };
    //=========================================================

    //=========================================================
    const handleSubmit = (values, actions) => {
        if (!_.isEmpty(userDataString)) {
            const datavalue = { ...values, ...userDataString };
            //====================
            //console.log(datavalue)
            //===================
            createLoadPost(datavalue).then(result => {
                setSuccessStatus(true)
                Toast.show({
                    text1: 'Post Load',
                    text2: 'Post Load Successfully Update'
                });
            }).catch(alert);
        }
    }
    useEffect(() => {
        Promise.all([
            getSystemList('goodsType'),
            getSystemListType('vehicleType'),
            getSystemList('loadsType'),
            getSystemListType('Weight Type'),
            getSystemList('city')
        ])
            .then(result => {
                setGoodsTypeList(result[0])
                setVehicleTypeList(result[1])
                setLoadsTypeList(result[2])
                setWeightList(result[3])
                setCityList(result[4]);
                setApiLoading(false)
            }).catch(e => {
                console.log("Error in loading api data", e);
            })
        const userObject = getLocalData('userObject');
        setUserDataString({
            contactName: userObject.contactName,
            contactNumber: userObject.username,
            mtUserId: getLocalData('userId')
        });

    }, [])
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            {apiLoading ? <MtActivityIndicator /> : <Formik
                initialValues={{ vehicleType: '', goodsType: '', loadsType: '', pickupLocation: '', destination: '', specialInstruction: '', itemWeight: '' }}
                validationSchema={LoadValidationSchema}
                onSubmit={(values, actions) => { handleSubmit(values, actions) }}
            >
                {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur, setFieldValue }) => (
                    <>
                        {!successStatus &&
                            <View style={MemberStyles.meanBox}>
                                <MtBodyText style={MemberStyles.headingTitleStyle}>Post Load</MtBodyText>
                                <MtBodyText style={MemberStyles.labeltext}>Vehicle Type</MtBodyText>
                                <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                                <MtDropDown
                                    items={vehicleTypeList}
                                    onValueChangeHandler={value => {
                                        setVehicleTypeValue(value)
                                        setFieldValue("vehicleType", value)
                                    }}
                                    name="vehicleType"
                                    selectedvalue={vehicleTypeValue}
                                /></View>
                                <MtBodyText style={MemberStyles.labeltext}>Goods Type</MtBodyText>
                                <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                                <MtDropDown
                                    items={goodsTypeList}
                                    onValueChangeHandler={value => {
                                        setGoodsTypeValue(value)
                                        setFieldValue("goodsType", value)
                                    }}
                                    name="goodsType"
                                    selectedvalue={goodsTypeValue}
                                /></View>
                                <MtBodyText style={MemberStyles.labeltext}>Loads Type</MtBodyText>
                                <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                                <MtDropDown
                                    items={loadsTypeList}
                                    onValueChangeHandler={value => {
                                        setLoadsTypeValue(value)
                                        setFieldValue("loadsType", value)
                                    }}
                                    name="loadsType"
                                    selectedvalue={loadsTypeValue}
                                />
                                </View>
                                <MtBodyText style={MemberStyles.labeltext}>Pickup Location</MtBodyText>
                                <ScrollView>
                                    <Autocomplete
                                        autoCapitalize="none"
                                        autoCorrect={true}
                                        containerStyle={styles.autocompleteContainer}
                                        inputContainerStyle={styles.inputContainer}
                                        listContainerStyle={styles.listContainerStyle}
                                        listStyle={styles.listStyleAutoSearch}
                                        // Data to show in suggestion
                                        data={filteredFilms}
                                        // Default value if you want to set something in input
                                        defaultValue={
                                            JSON.stringify(selectedValue) === '{}' ? '' : selectedValue.label
                                        }
                                        onChangeText={(text) => findFilm(text)}
                                        placeholder="Enter The From Location"
                                        renderItem={({ item, key }) => (
                                            // For the suggestion view
                                            <TouchableOpacity
                                                key={key}
                                                onPress={() => {
                                                    setSelectedValue(item);
                                                    setFieldValue('pickupLocation', item.value)
                                                    setFilteredFilms([]);
                                                }}>
                                                <Text style={styles.itemText}>
                                                    {item.label}
                                                </Text>
                                            </TouchableOpacity>
                                        )}
                                    />
                                </ScrollView>

                                <MtBodyText style={MemberStyles.labeltext}>Destination</MtBodyText>
                                <ScrollView>
                                    <Autocomplete
                                        autoCapitalize="none"
                                        autoCorrect={true}
                                        containerStyle={styles.autocompleteContainer}
                                        inputContainerStyle={styles.inputContainer}
                                        listContainerStyle={styles.listContainerStyle}
                                        listStyle={styles.listStyleAutoSearch}
                                        // Data to show in suggestion
                                        data={filteredDestination}
                                        // Default value if you want to set something in input
                                        defaultValue={
                                            JSON.stringify(selectedDestinationValue) === '{}' ? '' : selectedDestinationValue.label
                                        }
                                        onChangeText={(text) => findDestination(text)}
                                        placeholder="Enter The From Location"
                                        renderItem={({ item, key }) => (
                                            // For the suggestion view
                                            <TouchableOpacity
                                                key={key}
                                                onPress={() => {
                                                    setSelectedDestinatioValue(item);
                                                    setFieldValue('destination', item.value)
                                                    setFilteredDestination([]);
                                                }}>
                                                <Text style={styles.itemText}>
                                                    {item.label}
                                                </Text>
                                            </TouchableOpacity>
                                        )}
                                    />
                                </ScrollView>
                                <MtBodyText style={MemberStyles.labeltext}>Weight</MtBodyText>
                                <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                                <MtDropDown
                                    items={weightList}
                                    onValueChangeHandler={value => {
                                        setWeightValue(value)
                                        setFieldValue("itemWeight", value)
                                    }}
                                    name="itemWeight"
                                    selectedvalue={weightValue}
                                /></View>
                                <MtBodyText style={MemberStyles.labeltext}>Special Instruction</MtBodyText>
                                <MtInput
                                    placeholder="Special Instruction"
                                    name="specialInstruction"
                                    multiline={true}
                                    onChangeText={handleChange('specialInstruction')}
                                    value={values.specialInstruction}
                                    numberOfLines={5}
                                    textAlignVertical={"top"}
                                    inputTextStyle={{ paddingTop: 10 }}
                                />
                                <MtButton
                                    title="Submit"
                                    onPressHandler={handleSubmit}
                                    btnWidth='95%'
                                    loading={isSubmitting}
                                />
                            </View>
                        }
                        {successStatus &&
                            <MtSuccessMessage
                                headingMessage="Post Load Successfully Inserted" />
                        }
                    </>
                )}
            </Formik>
            }

        </ScreenLayout>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1,
        padding: 16,
        marginTop: 40,
    },
    autocompleteContainer: {
        backgroundColor: '#ffffff',
        marginLeft: 10,
        marginEnd: 10,
        zIndex: 1
    },
    itemText: {
        fontSize: 15,
        paddingTop: 7,
        paddingBottom: 7,
        margin: 4,
    },
    inputContainer: {
        paddingLeft: 10,
    },
    listContainerStyle:
    {
        marginLeft: 0,
        marginEnd: 0,
        backgroundColor: "#fff",
        padding: 0,
    },
    listStyleAutoSearch:
    {
        marginLeft: 0,
        marginEnd: 0,
        backgroundColor: "#fff",
        padding: 10,
    }
});