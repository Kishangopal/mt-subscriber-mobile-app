import React, { useEffect, useState } from 'react';
import { MemberStyles } from '../../../styles';
import { MtBodyText, MtDropDown, MtInput } from '../../../components/ui-elements';
import { getSystemList, getSystemListType } from '../../../services/api/api-service'

export const LoadPost = (props) => {
  const [goodsTypeValue, setGoodsTypeValue] = useState(null);
  const [goodsTypeList, setGoodsTypeList] = useState([]);
  const [vehicleTypeValue, setVehicleTypeValue] = useState(null);
  const [vehicleTypeList, setVehicleTypeList] = useState([]);
  const [loadsTypeValue, setLoadsTypeValue] = useState(null);
  const [loadsTypeList, setLoadsTypeList] = useState([]);
  const [weightValue, setWeightValue] = useState(null);
  const [weightList, setWeightList] = useState([]);
  const { handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur, setFieldValue } = props;
  useEffect(() => {
    getSystemList('goodsType').then(setGoodsTypeList)
    getSystemListType('vehicleType').then(setVehicleTypeList)
    getSystemList('loadsType').then(setLoadsTypeList)
    getSystemListType('Weight Type').then(setWeightList)
  }, [])
  return (
    <>
      <MtBodyText style={MemberStyles.headingTitleStyle}>{props.title}</MtBodyText>
      <MtBodyText style={MemberStyles.labeltext}>Vehicle Type</MtBodyText>
      <MtDropDown
        items={vehicleTypeList}
        onValueChangeHandler={value => {
          setVehicleTypeValue(value)
          setFieldValue("vehicleType", value)
        }}
        name="vehicleType"
        selectedvalue={vehicleTypeValue}
      />
      <MtBodyText style={MemberStyles.labeltext}>Goods Type</MtBodyText>
      <MtDropDown
        items={goodsTypeList}
        onValueChangeHandler={value => {
          setGoodsTypeValue(value)
          setFieldValue("goodsType", value)
        }}
        name="goodsType"
        selectedvalue={goodsTypeValue}
      />
      <MtBodyText style={MemberStyles.labeltext}>Loads Type</MtBodyText>
      <MtDropDown
        items={loadsTypeList}
        onValueChangeHandler={value => {
          setLoadsTypeValue(value)
          setFieldValue("loadsType", value)
        }}
        name="loadsType"
        selectedvalue={loadsTypeValue}
      />
      {/* <MtBodyText style={MemberStyles.labeltext}>Contact Name</MtBodyText>
      <MtInput
        placeholder="Contact Name"
        name="contactName"
        value={values.contactName}
        onChangeText={props.handleChange('contactName')}
        errorMessage={props.touched.contactName && props.errors.contactName}
        onBlur={props.handleBlur('contactName')}
      />
      <MtBodyText style={MemberStyles.labeltext}>Contact Number</MtBodyText>
      <MtInput
        placeholder="Contact Number"
        name="contactNumber"
        value={values.contactNumber}
        onChangeText={props.handleChange('contactNumber')}
        errorMessage={props.touched.contactNumber && props.errors.contactNumber}
        onBlur={props.handleBlur('contactNumber')}
      /> */}
      <MtBodyText style={MemberStyles.labeltext}>Pickup Location</MtBodyText>
      <MtInput
        placeholder="Pickup Location"
        name="pickupLocation"
        onChangeText={props.handleChange('pickupLocation')}
        //value={values.pickupLocation}
        errorMessage={props.touched.pickupLocation && props.errors.pickupLocation}
        onBlur={props.handleBlur('pickupLocation')}
      />
      <MtBodyText style={MemberStyles.labeltext}>Destination</MtBodyText>
      <MtInput
        placeholder="Destination"
        name="destination"
        onChangeText={props.handleChange('destination')}
        //value={values.destination}
        errorMessage={props.touched.destination && props.errors.destination}
        onBlur={props.handleBlur('destination')}
      />
      <MtBodyText style={MemberStyles.labeltext}>Weight</MtBodyText>
      <MtDropDown
        items={weightList}
        onValueChangeHandler={value => {
          setWeightValue(value)
          setFieldValue("itemWeight", value)
        }}
        name="itemWeight"
        selectedvalue={weightValue}
      />
      <MtBodyText style={MemberStyles.labeltext}>Special Instruction</MtBodyText>
      <MtInput
        placeholder="Special Instruction"
        name="specialInstruction"
        multiline={true}
        onChangeText={props.handleChange('specialInstruction')}
        //value={values.specialInstruction}
        numberOfLines={5}
        textAlignVertical={"top"}
        inputTextStyle={{ paddingTop: 10 }}
      />

    </>
  );
};