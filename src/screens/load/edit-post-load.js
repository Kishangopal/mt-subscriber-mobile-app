import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtButton} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {Formik} from 'formik';
import {LoadPost} from './loadPostForm'
import {UpdateLoadPost} from '../../services/api/api-service'
import {LoadValidationSchema} from '../../components/validations';

export const EditPostLoad = ({route}) => {
  const postId = route.params.postId;
  const data={
    vehicleType: 'PickUp',goodsType:'House Hold',loadsType:'Part Load',contactName:'Ravi Sharma mt',contactNumber:'1234567890',pickupLocation:'Ajmer,Rajasthan',destination:'Jaipur,Rajasthan',weight:'1000',specialInstruction:'Post Valid',status:'Active'
  }
  const handleSubmit = (values, actions) => {
    console.log(JSON.stringify(values))
    UpdateLoadPost(values).then(result=>{
      actions.setSubmitting(false);
   }).catch(alert);
  }
 const postData={...data,...{id:postId}}
    return (
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      <View style={MemberStyles.meanBox}>
        <Formik
        initialValues={postData}
        validationSchema={LoadValidationSchema}
        onSubmit={(values, actions) => {handleSubmit(values, actions) }}
      >
        {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur }) => (
          <>
        <LoadPost
        title="Edit Post Vehicle Load"
        handleChange={handleChange}
        values={values}
        errors={errors}
        isValid={isValid}
        isSubmitting={isSubmitting}
        touched={touched}
        handleBlur={handleBlur}
        postData={postData}
         />
              <MtButton
                title="Submit"
                onPressHandler={handleSubmit}
                btnWidth='95%'
                isDisabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
               </>
        )}
      </Formik>
      </View>

      </ScreenLayout>
    );
  };