import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import _ from "lodash"
import { GlobalStyles, MemberStyles } from '../../styles';
import { AppRoute } from '../../navigation/app-routes';
import { ScreenLayout } from '../screen-layout';
import { getUserGetLoadPostList, getSystemList } from '../../services/api/api-service';
import { getLocalData } from '../../services/global-storage';
import { MtBodyText, MtButton } from '../../components/ui-elements';
import { Icon } from 'react-native-elements'
import Toast from 'react-native-toast-message';
export const MemberPostList = (props) => {
  const [loadPostList, setLoadPostList] = useState([]);
  useEffect(() => {
    console.log(getLocalData('userId') + ":-MtUserId")
    const postLoadData = [];
    getSystemList('city').then(citylist => {
      //console.log(citylist)
      if (!_.isEmpty(citylist)) {
        getUserGetLoadPostList(getLocalData('userId')).then(data => {
          data.forEach(dataValue => {
            let cityDisplayPickup = _.get(_.find(citylist, { value: dataValue.pickupLocation }), "label")
            let cityDisplaydestination = _.get(_.find(citylist, { value: dataValue.destination }), "label")
            postLoadData.push({
              vehicleType: dataValue.vehicleType,
              goodsType: dataValue.goodsType,
              loadsType: dataValue.loadsType,
              contactName: dataValue.contactName,
              contactNumber: dataValue.contactNumber,
              specialInstruction: dataValue.specialInstruction,
              itemWeight: dataValue.itemWeight,
              id: dataValue.id,
              mtUserId: dataValue.mtUserId,
              cityDisplayPickup,
              cityDisplaydestination
            })
          })
          setLoadPostList(postLoadData)
          console.log(postLoadData)
        })
      }
    }).catch(e => {
      console.log("Error in city list:", e);
    })
  }, [])
  return (
    <>
      <View style={MemberStyles.FixHeadBox}>
        <View style={MemberStyles.fixHeadresultBox}>
          <MtBodyText style={MemberStyles.labelHeadtext}>Result {loadPostList.length} </MtBodyText>
        </View>
      </View>
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
        {
          loadPostList.map(item => (
            <View style={MemberStyles.cardBox} key={item.id}>
              <MtBodyText style={MemberStyles.cardTitleStyle}>{item.vehicleType}({item.itemWeight})</MtBodyText>
              <View style={MemberStyles.cardbodystyle}>
                <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome-5' name="map-marker-alt" size={16} color="black" /> {item.cityDisplayPickup}</MtBodyText>
                <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome' name="location-arrow" size={16} color="black" /> {item.cityDisplaydestination}</MtBodyText>
                {/* <MtBodyText style={MemberStyles.cardtext}><Icon type='FontAwesome' name="calendar" size={16} color="black" /> {item.Date}</MtBodyText> */}
                <MtBodyText style={MemberStyles.cardtext}><Icon type='font-awesome-5' name="truck-loading" size={16} color="black" /> {item.goodsType}/{item.loadsType}</MtBodyText>
              </View>
            </View>
          ))}
      </ScreenLayout>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  cardHeadBox:
  {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 1,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
  },
  FilterHeadBox:
  {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 1,
  },
  rowBox: {
    flex: 1,
    padding: 1,
    zIndex: 0,
    marginVertical: 10,
  }
});
