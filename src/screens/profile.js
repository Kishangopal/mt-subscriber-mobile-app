import React, { useEffect, useState } from 'react';
import { Image, Linking, View } from 'react-native';
import _ from 'lodash'
import { AppRoute } from '../navigation/app-routes';
import { GlobalStyles, MemberStyles } from '../styles';
import { MtActivityIndicator, MtBodyText, MtGenericContainer, MtLink, MtButton } from '../components/ui-elements';
import { ScreenLayout } from './screen-layout';
import { Icon } from 'react-native-elements'
import { getBusinessProfileView } from '../services/api/api-service';
import { getLocalData } from '../services/global-storage';
export const ProfileScreen = (props) => {
  const [buinessProfileView, setPackersAndMoversView] = useState([]);
  const [dataStatus, setDataStatus] = useState(false);
  const [NewUser, setNewUser] = useState(true);
  const [mtUserId, setMtUserId] = useState(null);
  useEffect(() => {
    setMtUserId(getLocalData('userId'));
    getBusinessProfileView(getLocalData('userId')).then(data => {
      if (!_.isEmpty(data)) {
        setPackersAndMoversView(data)
        setDataStatus(true)
        setNewUser(false)
      }
    }).catch(e => {
      console.log("Error in Profile Starts:", e);
    })
  }, [])
  return (
    <>
      {!NewUser &&
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
          {
            buinessProfileView.map(item => (
              <View key={item.id}>
                <MtGenericContainer
                  headerTitle="User Profile"
                  coverImage={item.businessCard}
                  avatarBox={<Image style={MemberStyles.logoImage} source={require("../../assets/userProfile.png")} />}
                >
                  <View style={MemberStyles.boxBody}>
                    <MtBodyText style={MemberStyles.userText}>{item.companyName} <Icon type="font-awesome" name="check-circle" color={"#34c38f"} size={18} /></MtBodyText>
                    <MtBodyText style={MemberStyles.userSubText}>({item.businessTypeTag})</MtBodyText>
                  </View>
                </MtGenericContainer>
                <View style={MemberStyles.meanBox}>
                  <MtBodyText style={MemberStyles.headingTitleStyle}>User Detail</MtBodyText>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Company Name:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText}>{item.companyName}</MtBodyText>
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Contact Number:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('tel:' + _.get(item, "contactNumber") ? item.contactNumber : ""); }}>{_.get(item, "contactNumber") ? item.contactNumber : "N/A"}</MtBodyText>
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>E-Mail:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('mailto:' + _.get(item, "businessEmail") ? item.businessEmail : ""); }}>{_.get(item, "businessEmail") ? item.businessEmail : "N/A"}</MtLink >
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Website:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('http://' + _.get(item, "website") ? item.website : ""); }}>{_.get(item, "website") ? item.website : "N/A"}</MtLink >
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Owner Name:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtLink style={MemberStyles.userSubText}>{item.ownerName}</MtLink >
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Owner Phone:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtLink style={MemberStyles.userSubText} onPressHandler={() => { Linking.openURL('tel:' + `${_.get(item, "ownerPhone") ? item.ownerPhone : "N/A"}`); }}>{_.get(item, "ownerPhone") ? item.ownerPhone : "N/A"}</MtLink >
                    </View>
                  </View>
                  {/* <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>GST No.:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText}>{_.get(item, "GSTNo") ? item.GSTNo : "N/A"}</MtBodyText >
                    </View>
                  </View> */}
                  {/* <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Pan Card No.:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText}>{_.get(item, "penCardNo") ? item.penCardNo : "N/A"}</MtBodyText >
                    </View>
                  </View> */}
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Location:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText}>{item.address},{item.city},{item.state} {item.pin}</MtBodyText >
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Route Vehicle Loaded:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText}>{item.routeVehicleLoaded}</MtBodyText >
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userText}>Special Services:</MtBodyText>
                    </View>
                    <View style={MemberStyles.lableData}>
                      <MtBodyText style={MemberStyles.userSubText}>{item.specialServices}</MtBodyText >
                    </View>
                  </View>
                  <View style={MemberStyles.rowflex}>
                    <View style={MemberStyles.lableView}>
                      <MtBodyText style={MemberStyles.userRedText}>Note: If you need change your business directory information you can contact  moverstrip helpline number</MtBodyText>
                    </View>
                  </View>
                </View>
              </View>
            ))}
          {!dataStatus &&
            <MtActivityIndicator />
          }
        </ScreenLayout>
      }
      {NewUser &&
        <>
          <View style={MemberStyles.DefaultScreenBox}>
            <Icon type="material-community" name="face-profile" size={100} color="#2948df" />
            <MtBodyText style={MemberStyles.cardTitleStyle}>Create Your Business Profile</MtBodyText>
            <MtBodyText style={{ marginLeft: 20, textAlign: 'center', }}>Create Your Business Profile And Add In Moverstrip Business Listing</MtBodyText>
            <MtButton
              btnWidth='96%'
              title="Add Business Listing"
              style={{ marginLeft: 20, textAlign: 'center', }}
              onPressHandler={() => props.navigation.push(AppRoute.UPDATE_PROFILE, { Id: mtUserId })} />
          </View>
        </>
      }
    </>
  );
};
