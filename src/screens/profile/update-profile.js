import React, { useEffect, useState } from 'react';
import { View, StyleSheet,Alert } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import { GlobalStyles, MemberStyles,NonMemberStyles } from '../../styles';
import { MtBodyText, MtButton, MtDropDown, MtInput } from '../../components/ui-elements';
import { ScreenLayout } from '../screen-layout';
import { getBusinessProfileView, getSystemList, getSystemListCity, createBusinessListing, UpdateBusinessProfile } from '../../services/api/api-service';
import { Formik } from 'formik';
import _ from 'lodash';
import { ProfileValidationSchema } from '../../components/validations'
import { getLocalData } from '../../services/global-storage';
import { ImageUpload } from '../components';
import Spinner from 'react-native-loading-spinner-overlay';
import { Icon } from 'react-native-elements'
export const UpdateProfile = (props) => {
  const [visitingCard, setVisitingCard] = useState(null);
  const [spinner, setSpinner] = useState(false);
  const componentDidMount = () => {
    setInterval(() => {
      setSpinner(false);
    }, 1000);
  }
  const dataJson = {
    companyName: '',
    contactPerson: '',
    address: '',
    GSTNo: '',
    city: '',
    state: '',
    businessEmail: '',
    website: '',
    shopNo: '',
    businessTypeTag: '',
    pin: '',
    ownerName: '',
    ownerPhone: '',
    penCardNo: '',
    contactNumber: '',
    routeVehicleLoaded: '',
    specialServices: ''
  }
  const [serviceType, setServiceType] = useState(null);
  const [serviceTypeList, setServiceTypeList] = useState([]);
  //
  const [stateValue, setStateValue] = useState(null);
  const [stateList, setStateList] = useState([]);
  //
  const [cityValue, setCityValue] = useState(null);
  const [cityList, setCityList] = useState([]);
  //
  const [listingCategoryList, setListingCategoryList] = useState([]);
  const [listingValue, setListingValue] = useState(null);
  const [businessProfile, setBusinessProfile] = useState([]);
  const [mtUserId, setMtUserId] = useState(null);// Current user mtId
  const [businessId, setBusinessId] = useState(null);// Submit Business Profile Id
  const [profileStatus, setProfileStatus] = useState(true)//use profile form to Image Upload
  const [submitStatus, setSubmitStatus] = useState(true)// create profile page to successfuly screen

  const UplaodImageHandler = () => {
    setSpinner(true);
    componentDidMount();
    const Data = [];
    Data.push({
      businessCard: visitingCard
    })
    UpdateBusinessProfile(businessId, Data).then(data => {
      setProfileStatus(false);
    })
  }
  const handleSubmit = (values, actions) => {
    //console.log(values);
    setSpinner(true);
    componentDidMount();
    const mtBusinessType = [];
    if (values.businessTypeTag == null) {
      alert("Business type not selected");
    }
    else if(values.specialServices==null)
    {
      alert("Special service not selected");
    }
    else if(values.state==null)
    {
      alert("State not selected");
    }
    else if(values.city==null)
    {
      alert("City not selected");
    }
    else {
      if (values.businessTypeTag == "Packers And Movers,Transporter") {
        mtBusinessType[0] = "Packers And Movers";
        mtBusinessType[1] = "Transporter";
      }
      else {
        mtBusinessType[0] = `${values.businessTypeTag}`;
      }
      const datavalue = {
        companyName: values.companyName,
        contactPerson: values.contactPerson,
        address: values.address,
        GSTNo: "NULL",
        city: values.city,
        state: values.state,
        businessEmail: values.businessEmail,
        website: values.website,
        shopNo: "NULL",
        businessTypeTag: mtBusinessType,
        pin: values.pin,
        ownerName: values.contactPerson,
        ownerPhone: values.contactNumber,
        penCardNo: "NULL",
        contactNumber: values.contactNumber,
        mtUserId: mtUserId,
        routeVehicleLoaded: values.routeVehicleLoaded,
        specialServices: values.specialServices,
        status:"P"
      }
     // console.log(datavalue);
      createBusinessListing(datavalue).then(result => {
        setBusinessId(result.id);
        setSubmitStatus(false)
      }).catch(alert);
    }
    
  }
  useEffect(() => {
    setMtUserId(getLocalData('userId'));
    getSystemList('state').then(setStateList)
    getSystemList('listtype').then(setListingCategoryList)
    getSystemList('ServiceType').then(setServiceTypeList);
    getSystemListCity(stateValue).then(d => {
      const allListData = []
      d.forEach(element => {
        allListData.push({ label: element.dbCity, value: element.dbCity })
      });
      setCityList(allListData)
      console.log(allListData);
    })
    getBusinessProfileView(getLocalData('userId')).then(setBusinessProfile)
  }, [stateValue])
  return (
    <>
      {profileStatus &&
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
          {submitStatus &&
            <Formik
              initialValues={dataJson}
              validationSchema={ProfileValidationSchema}
              onSubmit={(values, actions) => { handleSubmit(values, actions) }}
            >
              {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur, setFieldValue }) => (
                <>
                  <View style={MemberStyles.meanBox}>
                    <MtBodyText style={MemberStyles.headingTitleStyle}>Add Business Listing</MtBodyText>
                    <MtBodyText style={MemberStyles.labeltext}>Listing Category</MtBodyText>
                    <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                    <MtDropDown
                      items={listingCategoryList}
                      onValueChangeHandler={value => {
                        setListingValue(value)
                        setFieldValue('businessTypeTag', value)
                      }}
                      value={listingValue}
                    />
                    </View>
                    <MtBodyText style={MemberStyles.labeltext}>Company Name</MtBodyText>
                    <MtInput
                      placeholder="Company Name"
                      name="companyName"
                      onChangeText={handleChange('companyName')}
                      value={values.companyName}
                      errorMessage={touched.companyName && errors.companyName}
                      onBlur={handleBlur('companyName')}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Contact Person</MtBodyText>
                    <MtInput
                      placeholder="Contact Person"
                      name="contactPerson"
                      onChangeText={handleChange('contactPerson')}
                      value={values.contactPerson}
                      errorMessage={touched.contactPerson && errors.contactPerson}
                      onBlur={handleBlur('contactPerson')}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Contact Number</MtBodyText>
                    <MtInput
                      placeholder="Contact Number"
                      name="contactNumber"
                      onChangeText={handleChange('contactNumber')}
                      value={values.contactNumber}
                      errorMessage={touched.contactNumber && errors.contactNumber}
                      onBlur={handleBlur('contactNumber')}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Mail Id</MtBodyText>
                    <MtInput
                      placeholder="Mail Id"
                      name="businessEmail"
                      onChangeText={handleChange('businessEmail')}
                      value={values.businessEmail}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Website Url</MtBodyText>
                    <MtInput
                      placeholder="Website Url"
                      name="website"
                      onChangeText={handleChange('website')}
                      value={values.website}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Address</MtBodyText>
                    <MtInput
                      placeholder="Address"
                      name="address"
                      onChangeText={handleChange('address')}
                      value={values.address}
                      multiline={true}
                      numberOfLines={5}
                      textAlignVertical={"top"}
                      inputTextStyle={{ paddingTop: 10 }}
                      errorMessage={touched.address && errors.address}
                      onBlur={handleBlur('address')}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>State</MtBodyText>
                    <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                    <MtDropDown
                      items={stateList}
                      name="state"
                      onValueChangeHandler={value => {
                        setStateValue(value)
                        setFieldValue('state', value)
                      }}
                      value={stateValue}
                    /></View>
                    <MtBodyText style={MemberStyles.labeltext}>City</MtBodyText>
                    <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                    <MtDropDown
                      items={cityList}
                      name="city"
                      onValueChangeHandler={value => {
                        setCityValue(value)
                        setFieldValue('city', value)
                      }}
                      value={cityValue}
                    />
                    </View>
                    <MtBodyText style={MemberStyles.labeltext}>Pin Code</MtBodyText>
                    <MtInput
                      placeholder="Pin Code"
                      name="pin"
                      value={values.pin}
                      onChangeText={handleChange('pin')}
                      errorMessage={touched.pin && errors.pin}
                      onBlur={handleBlur('pin')}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Route Vehicle Loaded</MtBodyText>
                    <MtInput
                      placeholder="Route Vehicle Loaded"
                      name="routeVehicleLoaded"
                      value={values.routeVehicleLoaded}
                      onChangeText={handleChange('routeVehicleLoaded')}
                      errorMessage={touched.routeVehicleLoaded && errors.routeVehicleLoaded}
                      onBlur={handleBlur('routeVehicleLoaded')}
                    />
                    <MtBodyText style={MemberStyles.labeltext}>Special Services</MtBodyText>
                    <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                    <MtDropDown
                      items={serviceTypeList}
                      onValueChangeHandler={value => {
                        setServiceType(value)
                        setFieldValue('specialServices', value)
                      }}
                      value={serviceType}
                    /></View>
                    <MtButton
                      title="Submit"
                      onPressHandler={handleSubmit}
                      btnWidth='95%'
                      loading={isSubmitting}
                    />
                  </View>
                </>
              )}
            </Formik>
          }
          {!submitStatus &&
            <View style={MemberStyles.meanBox}>
              <MtBodyText style={MemberStyles.headingTitleStyle}>Upload Document</MtBodyText>
              <ImageUpload titleLabel="Visiting Card Image"
                fileUrl={setVisitingCard}
                bucketName="moverstrip-prod"
                UploadPath="business-cards/" />
              <MtButton
                title="Submit"
                onPressHandler={UplaodImageHandler}
                btnWidth='95%'
              />
            </View>


          }
        </ScreenLayout>
      }
      {!profileStatus &&
        <>
          <View style={MemberStyles.DefaultScreenBox}>
            <Icon type="antdesign" name="checkcircle" size={100} color="green" />
            <MtBodyText style={MemberStyles.cardTitleStyle}>Successfully</MtBodyText>
            <MtBodyText style={{ marginLeft: 20, textAlign: 'center', }}>You business profile successfully created</MtBodyText>
            <MtButton
              btnWidth='96%'
              title="View Profile"
              style={{ marginLeft: 20, textAlign: 'center', }}
              onPressHandler={() => props.navigation.push(AppRoute.PROFILE)} />
          </View>
        </>
      }
      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={styles.spinnerTextStyle}
      />
    </>
  );
};
const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
});