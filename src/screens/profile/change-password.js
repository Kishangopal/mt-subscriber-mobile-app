import React, { useEffect, useState } from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtBodyText, MtButton, MtInput} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {Formik} from 'formik';
import {AuthContext} from "../../store/context";
import {changeUserPassword} from '../../services/api/api-service';
import {removeLocalData} from '../../services/global-storage';
import {PasswordValidationSchema} from '../../components/validations';
import Toast from 'react-native-toast-message';
export const ChangePassword = () => {
  const { signOut } = React.useContext(AuthContext);
  const handleSubmit = (values, actions) => {
    changeUserPassword({ oldPassword: values.password, newPassword: values.newPassword }).then(result => {
      // actions.setSubmitting(true);
      removeLocalData('accessToken')
      signOut();
       Toast.show({
                    text1: 'Password Is Successfully Change',
                    text2: 'Login Account Again'
                  });
    })
      .catch(alert);
  }
  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      <>
        <Formik
          initialValues={{ password: '', newPassword: '', confirmPassword: '' }}
          validationSchema={PasswordValidationSchema}
          onSubmit={(values, actions) => { handleSubmit(values, actions) }}
        >
          {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur }) => (
            <>
              <View style={MemberStyles.meanBox}>
                <MtBodyText style={MemberStyles.labeltext}>Current Password</MtBodyText>
                <MtInput
                  placeholder="Current Password"
                  name="password"
                  value={values.password}
                  errorMessage={touched.password && errors.password}
                  onBlur={handleBlur('password')}
                  onChangeText={handleChange('password')}
                />
                <MtBodyText style={MemberStyles.labeltext}>Enter New Password</MtBodyText>
                <MtInput
                  placeholder="Enter New Password"
                  name="newPassword"
                  onChangeText={handleChange('newPassword')}
                  secureTextEntry={true}
                  value={values.newPassword}
                  errorMessage={touched.newPassword && errors.newPassword}
                  onBlur={handleBlur('newPassword')}
                />
                <MtBodyText style={MemberStyles.labeltext}>Enter Confirm Password</MtBodyText>
                <MtInput
                  placeholder="Enter Confirm Password"
                  name="confirmPassword"
                  onChangeText={handleChange('confirmPassword')}
                  secureTextEntry={true}
                  value={values.confirmPassword}
                  errorMessage={touched.confirmPassword && errors.confirmPassword}
                  onBlur={handleBlur('confirmPassword')}
                />
                <MtButton
                  title="Submit"
                  onPressHandler={handleSubmit}
                  btnWidth='95%'
                  isDisabled={!isValid || isSubmitting}
                  loading={isSubmitting}
                />
              </View>
            </>
          )}
        </Formik>
      </>
    </ScreenLayout>
  );
};