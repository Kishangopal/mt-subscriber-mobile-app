import React from "react";
import {RefreshControl, ScrollView, View} from "react-native";
import {SafeAreaView} from 'react-native-safe-area-context';

import {GlobalStyles} from '../styles';

const wait = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}
export const ScreenLayout = (props) => {
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        wait(2000).then(() => setRefreshing(false));
    }, []);

    return (<SafeAreaView style={{ flex: 1 }}>
        <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
            <View style={{ ...GlobalStyles.pageContainer, ...props.style }}>{props.children}</View>
        </ScrollView>
    </SafeAreaView>)

};


