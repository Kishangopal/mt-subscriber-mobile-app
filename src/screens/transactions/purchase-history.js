import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtBodyText} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';

const listpurchase  = [
  {
    id: 1, planName: "Gold Plan", Date: "12 Jun 2020",clickAdded: "200",paymentMethod:"Card",Balance:"1000",
  },
]
export const PurchaseHistory = ({ navigation }) => {
    return (
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
    <View style={MemberStyles.cardHeadBox}>
         <View style={MemberStyles.rowflexbox}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Purchase History</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userText}>Result 20 </MtBodyText>
      </View>
      </View>
      </View>
    {
     listpurchase.map((itamlist, i) => (
       <View style={MemberStyles.cardBox}>
         <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Plan Name:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{itamlist.planName}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Date:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{itamlist.Date}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Click Added:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{itamlist.clickAdded}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Payment Method:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{itamlist.paymentMethod}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Balance:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{itamlist.Balance}</MtBodyText>
              </View>
              </View>
      </View>
       ))}

      </ScreenLayout>
    );
  };
