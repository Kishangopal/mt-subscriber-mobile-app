import React from 'react';
import {View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../../styles';
import {MtBodyText} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';

const list = [
  {
    id: 1, actionName: "Map", Date: "12 Jun 2020",clicksConsumed: "10",
  },
]
export const Transactions = ({ navigation }) => {
    return (
      <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
        <View style={MemberStyles.cardHeadBox}>
        <View style={MemberStyles.rowflexbox}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Transaction</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userText}>Result 20 </MtBodyText>
              </View>
              </View>

      </View>
    {
    list.map((item, i) => (
       <View style={MemberStyles.cardBox}>
         <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Action Name:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.actionName}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Date:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.Date}</MtBodyText>
              </View>
              </View>
              <View style={MemberStyles.rowflex}>
              <View style={MemberStyles.lableView}>
              <MtBodyText style={MemberStyles.userText}>Click Consumed:</MtBodyText>
              </View>
              <View style={MemberStyles.lableData}>
              <MtBodyText style={MemberStyles.userSubText}>{item.clicksConsumed}</MtBodyText>
              </View>
              </View>
      </View>
       ))}
      </ScreenLayout>
    );
  };
