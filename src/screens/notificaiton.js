import React, {useEffect, useState} from 'react';
import {Alert, Modal, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {GlobalStyles, MemberStyles} from '../styles';
import _ from 'lodash'
import {MtActivityIndicator, MtBodyText} from '../components/ui-elements';
import {ScreenLayout} from './screen-layout';
import {getNotificationList} from '../services/api/api-service'
import {GetDay} from '../screens/vehicle/vehicleForm/'
import Toast from 'react-native-toast-message';
export const Notification = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalTitle, setModelTitle] = useState(null);
  const [modalBody, setModelBody] = useState(null);
  const [notificationList, setNotificationList] = useState([]);
  const [dataStatus, setDataStatus] = useState(false);
  useEffect(() => {
    getNotificationList('read').then(data => {
      if (!_.isEmpty(data)) {
        setNotificationList(data)
        setDataStatus(true)
      }
    }).catch(e => {
      console.log("Error in Hourly Starts:", e);
    })
  }, [])
  return (
    <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modelHeading}>
              <Text style={styles.modalText}>{modalTitle}</Text>
            </View>
            <View style={styles.modelBody}>
              <MtBodyText style={{ ...styles.smallText, marginLeft: 10 }} >{modalBody}</MtBodyText>

            </View>
            <View style={styles.modelFooter}>
              <TouchableHighlight
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.textStyle}>Cancel</Text>
              </TouchableHighlight>
            </View>

          </View>
        </View>
      </Modal>
      <View>
        {
          notificationList.map((l,key) => (
              <TouchableHighlight
                onPress={() => {
                  setModalVisible(true);
                  setModelTitle(l.msgSection);
                  setModelBody(l.msgText);
                }}
                style={MemberStyles.notificaionBox}
                key={key}
              >
                <View>
                  <MtBodyText style={MemberStyles.notificationTitle}>{l.msgSection}</MtBodyText>
                  <MtBodyText style={MemberStyles.notificationDescription}>{l.msgText.length>30?l.msgText.substring(0, 30)+"...":l.msgText.substring(0, l.msgText.length)}</MtBodyText>
                  <View style={MemberStyles.footerleft}>
                  <GetDay msDate={l.date}/>
                  </View>                  
                </View>
              </TouchableHighlight>
          ))
        }
        {!dataStatus &&
          <MtActivityIndicator />
        }
      </View>
    </ScreenLayout>
  );
};
const styles = StyleSheet.create({
  //modelbox
  centeredView: {
    flex: 1,
    backgroundColor: "#0c01018c",
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 1,
    width: '90%',
    alignItems: "flex-start",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textStyle: {
    color: "#0039ae",
    fontWeight: "bold",
  },
  modalText: {
    marginBottom: 15,
    color: '#fff',
  },
  modelHeading:
  {
    width: '100%',
    height: 40,
    backgroundColor: '#0039ae',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 1,
  },
  modelBody:
  {
    paddingHorizontal: 1,
    paddingTop: 5,
    paddingBottom: 10,
  },
  modelFooter:
  {
    width: '100%',
    height: 30,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 10,
    alignItems: 'flex-end'
  },
});