import React, { useState } from 'react';
import { Image, View } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import _ from 'lodash'
import { WebView } from 'react-native-webview';
import { MemberStyles } from '../../styles';
import { MtBodyText, MtButton, MtGenericContainer, MtInput, MtLink } from '../../components/ui-elements';

export const TermsAndConditions = (props) => {
  const {fromPath} = props.route.params
  return (
    <>
      <View style={MemberStyles.headerTopFixedBox}>
        <View style={MemberStyles.checkboxContainer}>
          <MtBodyText style={MemberStyles.headTitleStyle} >Terms And Conditions</MtBodyText>
        </View>
      </View>
      <WebView style={{ marginTop: 50, marginBottom: 80 }} source={{ uri: 'https://moverstrip-prod.s3.us-east-1.amazonaws.com/static/terms-conditions.html' }} />
      <View style={MemberStyles.FooterFixedBox}>
        <MtButton
          title={(fromPath == 'login') ? "Back to Login" : "Back to Registration"  }
          btnWidth='95%'
          onPressHandler={() =>
            props.navigation.push((fromPath == 'login') ? AppRoute.SIGN_IN : AppRoute.SIGN_UP)

          }
        />
      </View>
    </>
  );
}
