import React, { useEffect, useState } from 'react';
import { Image, View } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import { MtBodyText, MtButton, MtGenericContainer, MtInput, MtLink, MtDropDown } from '../../components/ui-elements';
import { ScreenLayout } from '../screen-layout';
import { GlobalStyles, NonMemberStyles } from '../../styles';
import { Icon } from 'react-native-elements'
import { CheckBox } from 'react-native-elements';
import { Formik } from 'formik'
import _ from 'lodash'
import { SignUpSchema } from '../../components/validations'
import { sendOtp, getPublicList } from '../../services/api/api-service'
import { authenticationService } from '../../services/api/auth-service'
import Toast from 'react-native-toast-message';
import { array } from 'yup';
export const SignUpScreen = (props) => {
    const [isChecked, setIsChecked] = useState(false);
    const [isRegister, setRegister] = useState(false);
    //stateList
    const [stateValue, setStateValue] = useState(null);
    const [stateList, setStateList] = useState([]);
    //category
    const [listingCategoryList, setListingCategoryList] = useState([]);
    const [listingValue, setListingValue] = useState(null);
    const handleSubmit = (values, actions) => {
        if (isChecked) {
            const regDataValue = []
            if (!_.isEmpty(values)) {
                regDataValue.push({
                    contactName: values.contactName,
                    password: values.password,
                    contactNumber: values.contactNumber,
                    username: values.contactNumber,
                    isTermsAgreed: true,
                    userType: "MTSUBSCRIBE",
                    email: values.email,
                    state:values.state,
                    businessTypeTag:values.businessTypeTag,
                    referrald:values.referrald
                })
            }
            authenticationService.signup(regDataValue[0].contactName, regDataValue[0].contactNumber, regDataValue[0].password, regDataValue[0].email, regDataValue[0].username, regDataValue[0].isTermsAgreed, regDataValue[0].userType,regDataValue[0].state,regDataValue[0].businessTypeTag).then(data => {
                actions.setSubmitting(false);
                if (regDataValue[0].contactNumber) {
                    sendOtp(regDataValue[0].contactNumber, 'phone').then(data => {
                        if (!_.isEmpty(data)) {
                            Toast.show({
                                text1: 'OTP is Resend Successfully.',
                                text2: 'Send OTP In Your Mobile Number'
                            });
                        }
                    }).catch(e => {
                        console.log("Error in sendOTP:", e);
                    })
                }
                props.navigation.navigate(AppRoute.OTP_VERIFICITION, { contectNumber: regDataValue[0].contactNumber })
            }
            ).catch(err => {
                actions.setSubmitting(false);
                alert(`Error:${JSON.stringify(_.get(err.response.data, "error") ? err.response.data.error.details.messages : err)}`)
            })
        }
        else {
            alert("Please Check Terms And Condition")
        }
        //console.log(values)
        }
    const navigateSignIn = () => {
        props.navigation.navigate(AppRoute.SIGN_IN);
    };
    useEffect(() => {
        getPublicList('state').then(setStateList)
        getPublicList('listtype').then(setListingCategoryList)
    }, [])
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            <MtGenericContainer
                headerTitle="Register"
                subTitle="Get your Moverstrip account now."
                avatarBox={<Image style={NonMemberStyles.logoImage} source={require("../../../assets/smallIcon.png")} />}
            >
                <View style={NonMemberStyles.boxBody}>
                    <Formik
                        initialValues={{ contactName: '', contactNumber: '', email: '', password: '', confirmPassword: '',state:'',businessTypeTag:'',referrald:'' }}
                        onSubmit={(values, actions) => { handleSubmit(values, actions) }}
                        validationSchema={SignUpSchema}>
                        {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur,setFieldValue }) => (
                            <>
                                <View style={NonMemberStyles.boxform}>
                                    <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                                       <MtDropDown
                                            placeholder={{
                                                label: 'Select category',
                                                value: null,
                                                color: '#9EA0A4',
                                            }}
                                            items={listingCategoryList}
                                            onValueChangeHandler={value => {
                                                setListingValue(value)
                                                setFieldValue('businessTypeTag', value)
                                            }}
                                            value={listingValue}
                                        />
                                    </View>
                                    <View
                                        style={NonMemberStyles.dropdownBoxStyle}>
                                        <MtDropDown
                                            placeholder={{
                                                label: 'Select state',
                                                value: null,
                                                color: '#9EA0A4',
                                            }}
                                            items={stateList}
                                            name="state"
                                            onValueChangeHandler={value => {
                                                setStateValue(value)
                                                setFieldValue('state', value)
                                            }}
                                            value={stateValue}
                                        /></View>
                                    <MtInput
                                        placeholder="Contact Name"
                                        name="contactName"
                                        onChangeText={handleChange('contactName')}
                                        value={values.contactName}
                                        errorMessage={touched.username && errors.contactName}
                                        onBlur={handleBlur('contactName')}
                                        leftIcon={<Icon type='font-awesome-5' name="user" color={"#74788d"} size={16} />}
                                    />
                                    <MtInput
                                        placeholder="Contact Number"
                                        name="contactNumber"
                                        onChangeText={handleChange('contactNumber')}
                                        value={values.contactNumber}
                                        errorMessage={touched.contactNumber && errors.contactNumber}
                                        onBlur={handleBlur('contactNumber')}
                                        leftIcon={<Icon type='font-awesome' name="phone" color={"#74788d"} size={16} />}
                                    />
                                    <MtInput
                                        placeholder="Email"
                                        name="email"
                                        onChangeText={handleChange('email')}
                                        value={values.email}
                                        errorMessage={touched.email && errors.email}
                                        onBlur={handleBlur('email')}
                                        leftIcon={<Icon type="ionicons" name="email" color={"#74788d"} size={16} />}
                                    />
                                   
                                    <MtInput
                                        placeholder="Password"
                                        secureTextEntry={true}
                                        name="password"
                                        onChangeText={handleChange('password')}
                                        value={values.password}
                                        errorMessage={touched.password && errors.password}
                                        onBlur={handleBlur('password')}
                                        leftIcon={<Icon type='font-awesome-5' name="lock" color={"#74788d"} size={16} />}
                                    />
                                    <MtInput
                                        placeholder="Confirm Password"
                                        name="confirmPassword"
                                        onChangeText={handleChange('confirmPassword')}
                                        value={values.confirmPassword}
                                        errorMessage={touched.confirmPassword && errors.confirmPassword}
                                        onBlur={handleBlur('confirmPassword')}
                                        secureTextEntry={true}
                                        leftIcon={<Icon type='font-awesome-5' name="lock" color={"#74788d"} size={16} />}
                                    />
                                     <MtInput
                                        placeholder="referral Id"
                                        name="referrald"
                                        onChangeText={handleChange('referrald')}
                                        leftIcon={<Icon type='font-awesome-5' name="id-card-alt" color={"#74788d"} size={16} />}
                                    />
                                </View>
                                <View style={NonMemberStyles.checkboxContainerCenter}>
                                    <CheckBox
                                        checked={isChecked}
                                        onPress={() => isChecked ? setIsChecked(false) : setIsChecked(true)}
                                        containerStyle={NonMemberStyles.checkbox} />
                                    <MtBodyText>I agree to the </MtBodyText>
                                    <MtLink onPressHandler={() => props.navigation.navigate(AppRoute.TERMS_AND_CONDITIONS, { fromPath: 'registration' })}>Terms and Conditions</MtLink>
                                </View>
                                <MtButton
                                    title="Register"
                                    onPressHandler={handleSubmit}
                                    btnWidth='95%'
                                    loading={isSubmitting}
                                />
                            </>
                        )}
                    </Formik>
                </View>
            </MtGenericContainer>
            <View style={NonMemberStyles.footBox}>
                <MtBodyText>Already have an account?</MtBodyText>
                <MtLink onPressHandler={navigateSignIn}>Login</MtLink>
            </View>
        </ScreenLayout>
    );
}
