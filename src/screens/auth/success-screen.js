import React from 'react';
import {View} from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import {MemberStyles,NonMemberStyles} from '../../styles';
import {MtBodyText,MtLink} from '../../components/ui-elements';
import { Icon } from 'react-native-elements'
export const SuccessScreen = (props) => {
    const navigateSignIn = () => {
        props.navigation.navigate(AppRoute.SIGN_IN);
    };
  return (
    <>
       <View style={MemberStyles.DefaultScreenBox}>
       {props.icon}
         <MtBodyText style={MemberStyles.cardTitleStyle}>{props.headingMessage}</MtBodyText>
        <MtBodyText>{props.subheading}</MtBodyText>
        </View>
        <View style={NonMemberStyles.footBox}>
         <MtBodyText style={{fontSize: 20}}>Go Back </MtBodyText>
         <MtLink style={{fontSize: 20}} onPressHandler={props.onPressHandlerLink}>Login</MtLink>
         </View>
    </>
  );
};
