import React, {useState} from 'react';
import {Image, View} from 'react-native';
import {AppRoute} from '../../navigation/app-routes';
import _ from 'lodash'
import {MtBodyText, MtButton, MtGenericContainer, MtInput, MtLink} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {GlobalStyles, NonMemberStyles} from '../../styles';
import { Icon } from 'react-native-elements'
import {Formik} from 'formik'
import {ResetPasswordSchema} from '../../components/validations';
import {sendOtp} from '../../services/api/api-service';
import Toast from 'react-native-toast-message';
export const ResetPasswordScreen = (props) => {
    const [otpStatus, setOtpStatus] = useState([]);
    const handleSubmit = (values, actions) => {
        console.log(values.mobilenumber)
        if (values.mobilenumber) {
            sendOtp(values.mobilenumber,'phone').then(data => {
                if (!_.isEmpty(data)) {
                    setOtpStatus(data)
                    Toast.show({
                    text1: 'Successfully Send.',
                    text2: 'Send OTP In Your Mobile Number'
                  });
                }
                props.navigation.navigate(AppRoute.OTP_VERIFY_PASSWORD,{mobileNo:values.mobilenumber});
            }).catch(e => {
                console.log("Error in sendOTP:", e);
            })
        }
    }
    const navigateSignIn = () => {
        props.navigation.navigate(AppRoute.SIGN_IN);
    };
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>

            <MtGenericContainer
                headerTitle="Welcome Back !"
                subTitle="Moverstrip"
                avatarBox={<Image style={NonMemberStyles.logoImage} source={require("../../../assets/smallIcon.png")} />}
            >
                <View style={NonMemberStyles.boxBody}>
                    <Formik
                        initialValues={{ mobilenumber: '' }}
                        onSubmit={(values, actions) => { handleSubmit(values, actions) }}
                        validationSchema={ResetPasswordSchema}>
                        {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur }) => (
                            <>
                                <View style={NonMemberStyles.boxform}>
                                    <MtInput
                                        placeholder="Mobile Number"
                                        name="mobilenumber"
                                        onChangeText={handleChange('mobilenumber')}
                                        value={values.mobilenumber}
                                        errorMessage={touched.mobilenumber && errors.mobilenumber}
                                        onBlur={handleBlur('mobilenumbe')}
                                        leftIcon={<Icon type="font-awesome" name="phone" color={"#74788d"} size={16} />}
                                    />
                                    <MtButton
                                        title="Send OTP"
                                        onPressHandler={handleSubmit}
                                        btnWidth='95%'
                                        //isDisabled={!isValid || isSubmitting}
                                        loading={isSubmitting}
                                    />
                                </View>
                            </>
                        )}
                    </Formik>
                </View>
            </MtGenericContainer >
            <View style={NonMemberStyles.footBox}>
                <MtBodyText>Go Back </MtBodyText>
                <MtLink onPressHandler={navigateSignIn}>Login</MtLink>
            </View>
        </ScreenLayout>
    );
}
