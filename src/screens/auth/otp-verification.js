import React, { useState } from 'react';
import { Image, View } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import _ from 'lodash'
import { MtBodyText, MtButton, MtGenericContainer, MtInput, MtLink } from '../../components/ui-elements';
import { ScreenLayout } from '../screen-layout';
import { GlobalStyles, NonMemberStyles } from '../../styles';
import { Icon } from 'react-native-elements'
import { Formik } from 'formik'
import { SuccessScreen } from './success-screen'
import { OtpFormSchema } from '../../components/validations';
import { sendOtp, verifyOtp} from '../../services/api/api-service'
import Toast from 'react-native-toast-message';
export const OtpVerification = (props) => {
    const [verifyOtpCode, setVerifyOtpCode] = useState([]);
    const [successStatus, setSuccessStatus] = useState(false);
    const mobileNo = props.route.params.contectNumber;
    //alert(userId);
    const handleSubmit = (values, actions) => {
        verifyOtp(mobileNo,values.otp).then(data => {
            if (!_.isEmpty(data)) {
                setVerifyOtpCode(data)
                Toast.show({
                    text1: 'Successfully Send.',
                    text2: 'Send OTP In Your Mobile Number'
                });
                setSuccessStatus(true);
            }
        }).catch(e => {
            console.log("Error in sendOTP:", e);
        })
    }
    const navigateSignIn = () => {
        props.navigation.navigate(AppRoute.SIGN_IN);
    };
    const reSendOtpCode = () => {
        if (mobileNo) {
            sendOtp(mobileNo, 'phone').then(data => {
                if (!_.isEmpty(data)) {
                    setOtpStatus(data)
                    Toast.show({
                        text1: 'OTP is Resend Successfully.',
                        text2: 'Send OTP In Your Mobile Number'
                    });
                }
            }).catch(e => {
                console.log("Error in sendOTP:", e);
            })
        }
    };
    return (
        <>
            {!successStatus &&
                <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
                    <MtGenericContainer
                        headerTitle="Welcome Back !"
                        subTitle="Moverstrip"
                        avatarBox={<Image style={NonMemberStyles.logoImage} source={require("../../../assets/smallIcon.png")} />}
                    >
                        <View style={NonMemberStyles.boxBody}>
                        <Formik
                                initialValues={{ otp: '' }}
                                validationSchema={OtpFormSchema}
                                onSubmit={(values,actions) => { handleSubmit(values, actions);console.log("hallo")}}
                                >
                                {({handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur, setFieldValue }) => (
                                    <>
                                        <View style={NonMemberStyles.boxform}>
                                            <MtInput
                                                placeholder="Enter OTP"
                                                name="otp"
                                                onChangeText={handleChange('otp')}
                                                value={values.otp}
                                                errorMessage={touched.otp && errors.otp}
                                                onBlur={handleBlur('otp')}
                                                leftIcon={<Icon type='font-awesome-5' name="user-check" color={"#74788d"} size={16} />}
                                            />
                                        </View>
                                        <View style={NonMemberStyles.leftAlignBox}>
                                            <MtLink style={NonMemberStyles.footerLinkText} onPressHandler={reSendOtpCode} ><Icon type='font-awesome' name="send" color={"#74788d"} size={16} /> Resend OTP</MtLink>
                                        </View>
                                        <MtButton
                                            title="Submit"
                                            onPressHandler={handleSubmit}
                                            btnWidth='95%'
                                            //isDisabled={!isValid || isSubmitting}
                                            loading={isSubmitting}
                                        />
                                    </>
                                )}
                            </Formik>
                        </View>
                    </MtGenericContainer>
                    <View style={NonMemberStyles.footBox}>
                        <MtBodyText>Go Back </MtBodyText>
                        <MtLink >Login</MtLink>
                    </View>
                </ScreenLayout>
            }
            {successStatus &&
                <SuccessScreen
                    icon={<Icon type="antdesign" name="checkcircle" size={120} color="#66ccff" />}
                    headingMessage="Successfully Registration"
                    subheading="Moverstrip User Successfully Registeration"
                    onPressHandlerLink={navigateSignIn}
                />
            }
        </>
    );
}
