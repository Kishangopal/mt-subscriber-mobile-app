import React, {useState} from 'react';
import {Image, View} from 'react-native';
import {AppRoute} from '../../navigation/app-routes';
import _ from 'lodash'
import {MtBodyText, MtButton, MtGenericContainer, MtInput, MtLink} from '../../components/ui-elements';
import {ScreenLayout} from '../screen-layout';
import {GlobalStyles, NonMemberStyles} from '../../styles';
import { Icon } from 'react-native-elements'
import {Formik} from 'formik'
import {ChangeYourValidationSchema} from '../../components/validations';
import {resetUserPassword} from '../../services/api/api-service';
import Toast from 'react-native-toast-message';

export const ChangeYourPassword = (props) => {
    const [changepassword,setChangePassword]=useState([]);
    const mobileNo=props.route.params.mobileNo;
    const otp=props.route.params.otp;
    console.log(mobileNo);
    console.log(otp);
    const handleSubmit = (values, actions) => {
        //console.log(values);
        resetUserPassword(mobileNo,otp,values.newPassword).then(data => {
            if (!_.isEmpty(data)) {
                setChangePassword(data)
                Toast.show({
                text1: 'Password Is Successfully Change',
                text2: 'Login Your Account Again'
              });
              props.navigation.navigate(AppRoute.SIGN_IN);
            }
    })
}
const navigateSignIn = () => {
    props.navigation.navigate(AppRoute.SIGN_IN);
};
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            <MtGenericContainer
                headerTitle="Welcome Back !"
                subTitle="Moverstrip"
                avatarBox={<Image style={NonMemberStyles.logoImage} source={require("../../../assets/smallIcon.png")} />}
            >
                <View style={NonMemberStyles.boxBody}>
                    <Formik
                        initialValues={{newPassword: '', confirmPassword: '' }}
                        onSubmit={(values, actions) => { handleSubmit(values, actions) }}
                        validationSchema={ChangeYourValidationSchema}>
                        {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur }) => (
                            <>
                                <View style={NonMemberStyles.boxform}>
                                    <MtInput
                                        placeholder="New Password"
                                        name="newPassword"
                                        onChangeText={handleChange('newPassword')}
                                        value={values.newPassword}
                                        errorMessage={touched.newPassword && errors.newPassword}
                                        onBlur={handleBlur('newPassword')}
                                        leftIcon={<Icon type="font-awesome-5" name="lock" color={"#74788d"} size={16} />}
                                    />
                                    <MtInput
                                        placeholder="Confirm Password"
                                        name="confirmPassword"
                                        onChangeText={handleChange('confirmPassword')}
                                        value={values.confirmPassword}
                                        errorMessage={touched.confirmPassword && errors.confirmPassword}
                                        onBlur={handleBlur('confirmPassword')}
                                        leftIcon={<Icon type="font-awesome-5" name="lock" color={"#74788d"} size={16} />}
                                    />
                                </View>

                                <MtButton
                                    title="Submit"
                                    onPressHandler={handleSubmit}
                                    btnWidth='95%'
                                    //isDisabled={!isValid || isSubmitting}
                                    loading={isSubmitting}
                                />
                            </>
                        )}
                    </Formik>
                </View>
            </MtGenericContainer>
            <View style={NonMemberStyles.footBox}>
                <MtBodyText>Go Back </MtBodyText>
                <MtLink onPressHandler={navigateSignIn}>Login</MtLink>
            </View>
        </ScreenLayout>
    );

}
