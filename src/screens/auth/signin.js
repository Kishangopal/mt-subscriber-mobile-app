import React,{useEffect} from 'react';
import { Image, View } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import { MtBodyText, MtButton, MtGenericContainer, MtInput, MtLink } from '../../components/ui-elements';
import { AuthContext } from "../../store/context";
import { ScreenLayout } from '../screen-layout';
import { GlobalStyles, NonMemberStyles } from '../../styles';
import { Icon } from 'react-native-elements'
import { CheckBox } from 'react-native-elements';
import { Formik } from 'formik'
import { LoginValidationSchema } from '../../components/validations';
import { authenticationService } from '../../services/api/auth-service'
import Toast from 'react-native-toast-message';
//import firebase from 'react-native-firebase';
export const SignInScreen = (props) => {
    const { signIn } = React.useContext(AuthContext);
    const handleSubmit = (values, actions) => {
        if (values.username.length > 0 && values.password.length > 0) {
            authenticationService.login(values.username, values.password).then(token => {
                signIn(token);
            }).catch(err => {
                actions.setSubmitting(false);
                console.log(err)
                Toast.show({
                    type: 'error',
                    text1: 'Login Error',
                    text2: err.message
                });
            })
        } else {
            actions.setSubmitting(false);
            alert('validation issue');
        }
    }
    // useEffect(()=>{
    //     firebase.analytics().logEvent("screen_view", {name:'Sign In'})
    // },[])
    const navigateSignUp = () => {
        props.navigation.navigate(AppRoute.SIGN_UP);
    };
    const navigateResetPassword = () => {
        props.navigation.navigate(AppRoute.RESET_PASSWORD);
    };
    return (
        <ScreenLayout style={GlobalStyles.pageContainerWithoutHeader}>
            <MtGenericContainer
                headerTitle="Welcome Back !"
                subTitle="Moverstrip"
                avatarBox={<Image style={NonMemberStyles.logoImage} source={require("../../../assets/smallIcon.png")} />}
            >
                <View style={NonMemberStyles.boxBody}>
                    <Formik
                        initialValues={{ username: '', password: '' }}
                        onSubmit={(values, actions) => { handleSubmit(values, actions) }}
                        validationSchema={LoginValidationSchema}>
                        {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur }) => (
                            <>
                                <View style={NonMemberStyles.boxform}>
                                    <MtInput
                                        placeholder="Enter your mobile number"
                                        onChangeText={handleChange('username')}
                                        name="username"
                                        value={values.username}
                                        errorMessage={touched.username && errors.username}
                                        onBlur={handleBlur('username')}
                                        leftIcon={<Icon type="font-awesome-5" name="user" color={"#74788d"} size={16} />}
                                    />
                                    <MtInput
                                        placeholder="Enter password"
                                        name="password"
                                        onChangeText={handleChange('password')}
                                        value={values.password}
                                        errorMessage={touched.password && errors.password}
                                        onBlur={handleBlur('password')}
                                        secureTextEntry={true}
                                        leftIcon={<Icon type="ionicon" name="md-key" color={"#74788d"} size={16} />}
                                    />
                                </View>

                                <View style={NonMemberStyles.checkboxContainer}>
                                    <CheckBox containerStyle={NonMemberStyles.checkbox} />
                                    <MtBodyText style={NonMemberStyles.Checkboxlabel}>Remember me</MtBodyText>
                                </View>
                                <MtButton
                                    title="Login"
                                    onPressHandler={handleSubmit}
                                    btnWidth='95%'
                                    isDisabled={!isValid || isSubmitting}
                                    loading={isSubmitting}
                                />
                                <View style={{ ...NonMemberStyles.checkboxContainerCenter, marginTop: 10 }}>

                                    <MtBodyText>By Login you agree to the </MtBodyText>
                                    <MtLink onPressHandler={() => props.navigation.navigate(AppRoute.TERMS_AND_CONDITIONS, { fromPath: 'login' })}>Terms & Conditions</MtLink>
                                </View>
                            </>
                        )}
                    </Formik>
                    <View style={NonMemberStyles.footFormBox}>
                        <Icon type='ionicon' name="lock-closed-sharp" color={"#74788d"} size={16} />
                        <MtLink style={NonMemberStyles.footerLinkText} onPressHandler={navigateResetPassword} >Forgot your password?</MtLink>
                    </View>
                </View>
            </MtGenericContainer>
            <View style={NonMemberStyles.footBox}>
                <MtBodyText>Don't have an account?</MtBodyText>
                <MtLink style={{ paddingLeft: 5 }} onPressHandler={navigateSignUp}>Signup now</MtLink>
            </View>
        </ScreenLayout>
    );
}
