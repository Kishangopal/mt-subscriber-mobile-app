import React, { useEffect, useState } from 'react';
import { View, StyleSheet,Linking } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
// import { NavigationActions } from 'react-navigation'
import { MtBodyText,MtButton } from '../../components/ui-elements';
import { MemberStyles } from '../../styles';
import _ from 'lodash'
import { getSystemList } from '../../services/api/api-service';
import Toast from 'react-native-toast-message';
import { Icon } from 'react-native-elements'
export const UpdateApp = (props) => {
  const [linkValue,setLinkValue]=useState(null);
  useEffect(() => {
    getSystemList('MoverstripAppUpdate').then(data => {
      setLinkValue(data[0].value)
    }).catch(e => {
      console.log("Error in State list:", e);
    })
  }, [])
  return (
    <>
      <View style={MemberStyles.screenLayoutView}>
      <View style={MemberStyles.checkboxContainer}>
      <MtBodyText style={MemberStyles.titleHeading} >Update Moverstrip</MtBodyText>
      </View>
      <Icon type="entypo" name="download" size={100} style={{marginTop:50}} color="green" />
         <MtBodyText style={{marginLeft:20,marginBottom:50,marginTop:50,fontSize:16, textAlign: 'center',}}>Your current version of application is outdated now. Please download latest version of this app from Google Playstore using below button link.</MtBodyText>
         <MtButton
          title="Submit"
          btnWidth='95%'
          onPressHandler={() => {Linking.openURL(`${linkValue}`); }}
        />
      </View>
     
      </>
  );
}

