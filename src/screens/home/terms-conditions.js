import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
// import { NavigationActions } from 'react-navigation'
import { MtBodyText, MtActivityIndicator, MtLink, MtButton } from '../../components/ui-elements';
import { GlobalStyles, MemberStyles } from '../../styles';
import { CheckBox } from 'react-native-elements';
import { WebView } from 'react-native-webview';
import { AuthContext } from "../../store/context";
import _ from 'lodash'
import { getLocalData } from '../../services/global-storage';
import { getUser, UpdateUser } from '../../services/api/api-service';
import Toast from 'react-native-toast-message';
export const TermsConditions = (props) => {
  const [userDetails, setUserDetails] = useState([]);
  const [isChecked, setIsChecked] = useState(false);
  const { moveFromTerms } = React.useContext(AuthContext);
  const handleTermsAndConditions = () => {
    if (isChecked) {
      const userId = _.get(userDetails, "id") ? userDetails.id : "";
      const markers = [];
      if (!_.isEmpty(userDetails)) {
        markers.push({
          status: "A",
          isTermsAgreed: true
        })
      }
      UpdateUser(userId, markers[0]).then(result => {
        moveFromTerms();
      }).catch(alert);
    }
    else {
      Toast.show({
        text1: 'Terms and Condition',
        text2: 'Check the checkbox after reading terms and condition.'
      });
    }
  }
  useEffect(() => {
    getUser(getLocalData('userId')).then(data => {
      if (!_.isEmpty(data)) {
        setUserDetails(data[0])
      }
    }).catch(e => {
      console.log("Error Get User")
    })
  }, [])
  return (
    <>
      <View style={MemberStyles.headerTopFixedBox}>
        <View style={MemberStyles.checkboxContainer}>
          <MtBodyText style={MemberStyles.headTitleStyle} >Terms And Conditions</MtBodyText>
        </View>
      </View>
      <WebView style={{ marginBottom: 80, marginTop: 50, }} source={{ uri: 'https://moverstrip-prod.s3.us-east-1.amazonaws.com/static/terms-conditions.html' }} />
      <View style={MemberStyles.headerFixedBox}>
        <View style={MemberStyles.checkboxContainer}>
          <CheckBox
            checked={isChecked}
            onPress={() => isChecked ? setIsChecked(false) : setIsChecked(true)}
            containerStyle={MemberStyles.checkbox} />
          <MtBodyText style={MemberStyles.Checkboxlabel} >I accept the terms and conditions</MtBodyText>
        </View>
        <MtButton
          title="Submit"
          btnWidth='95%'
          onPressHandler={handleTermsAndConditions}
        />
      </View>
    </>
  );
}

