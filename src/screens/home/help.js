import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Linking, View, Image } from 'react-native';
import _, { isEmpty } from 'lodash'
import { MemberStyles } from '../../styles';
import { Formik } from 'formik';
import { HelpDesk } from '../../components/validations'
import { Icon } from 'react-native-elements'
import { getLocalData } from '../../services/global-storage';
import { getSystemList, createHelpDesks } from '../../services/api/api-service';
import { MtBodyText, MtButton, MtDropDown, MtImagePicker, MtInput } from '../../components/ui-elements';
export const Help = (props) => {
  const [formStatus, SetFormStatus] = useState(false);
  const [mtUserIdvalue, setMtUserId] = useState("");
  const [mtHelpDesk, setMtHelpDesk] = useState("");
  const dataJson = {
    title: '',
    detail: '',
    phoneNumber: '',
  }
  const handleSubmit = (values, actions) => {
    const mtUserObject = { mtUserId: mtUserIdvalue,userType:"MTSUBSCRIBER"}
    const dataValue = { ...values, ...mtUserObject }
    console.log(dataValue)
    createHelpDesks(dataValue).then(result => {
      SetFormStatus(true)
    }).catch(alert);
  }
  useEffect(() => {
    setMtUserId(getLocalData('userId'));
    //console.log("UserId:-" + mtUserIdvalue);
    getSystemList('MTHelpDeskNo').then(data => {
      if (!_.isEmpty(data)) {
        setMtHelpDesk(data[0].value)
      }
    }).catch(e => {
      console.log("Error in Help Desk Number:", e);
    })
    //console.log(mtHelpDesk);
  }, [])
  return (
    <>
      {!formStatus &&
        <ScrollView style={{ marginHorizontal: 10, marginTop: 10, marginBottom: 10, }}>
          <Formik
            initialValues={dataJson}
            validationSchema={HelpDesk}
            onSubmit={(values, actions) => { handleSubmit(values, actions) }}
          >
            {({ handleChange, values, handleSubmit, errors, isValid, isSubmitting, touched, handleBlur, setFieldValue }) => (
              <>
                <View style={MemberStyles.meanBox}>
                  <View style={MemberStyles.headBoxStyle}>
                    <MtBodyText style={MemberStyles.whiteText} >Help Desk</MtBodyText>
                  </View>
                  <MtInput
                    placeholder="Please Enter Your Issue*"
                    name="title"
                    onChangeText={handleChange('title')}
                    value={values.title}
                    errorMessage={touched.title && errors.title}
                    onBlur={handleBlur('title')}
                  />
                  <MtInput
                    placeholder="Where did you face this?"
                    name="detail"
                    onChangeText={handleChange('detail')}
                    value={values.detail}
                    numberOfLines={5}
                    multiline={true}
                    textAlignVertical={"top"}
                    inputTextStyle={{ paddingTop: 10 }}
                    errorMessage={touched.detail && errors.detail}
                    onBlur={handleBlur('detail')}
                  />
                  <MtInput
                    placeholder="Enter Phone Number"
                    name="phoneNumber"
                    onChangeText={handleChange('phoneNumber')}
                    value={values.phoneNumber}
                    errorMessage={touched.phoneNumber && errors.phoneNumber}
                    onBlur={handleBlur('phoneNumber')}
                  />
                  <MtButton
                    title="Submit"
                    onPressHandler={handleSubmit}
                    btnWidth='95%'
                    //isDisabled={!isValid || isSubmitting}
                    loading={isSubmitting}
                  />
                  <Image style={{ width: '100%', height: 10, marginTop: 15, marginBottom: 10 }} source={{
                    uri: 'https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/border-Image-or.png',
                  }} />
                  <MtButton
                    title="Call Now"
                    btnWidth='95%'
                    onPressHandler={() => { Linking.openURL('tel:' + `${mtHelpDesk.length > 0 ? mtHelpDesk : '9352777744'}`); }}
                  />
                </View>
              </>
            )}
          </Formik>
        </ScrollView >
      }
      {formStatus &&
        <View style={MemberStyles.DefaultScreenBox}>
          <Icon type="antdesign" name="checkcircle" size={120} color="#66ccff" />
          <MtBodyText style={MemberStyles.cardTitleStyle}>Successfully Send Your Request</MtBodyText>
          <MtBodyText style={{ marginHorizontal: 50 }}>Thanks For Connecting Moverstrip Help Desk</MtBodyText>
          <MtBodyText> We Are Contact Early Soon</MtBodyText>
        </View>
      }
    </>
  );
}