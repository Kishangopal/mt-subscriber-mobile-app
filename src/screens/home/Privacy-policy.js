import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {MtBodyText} from '../../components/ui-elements';
import {GlobalStyles, MemberStyles} from '../../styles';
import {ScreenLayout} from '../screen-layout';
import {getSystemList} from '../../services/api/api-service';
import { WebView } from 'react-native-webview';
export const PrivacyPolicy = (props) => {
  return (
    <WebView source={{ uri: 'https://moverstrip-prod.s3.us-east-1.amazonaws.com/static/Privacy-Policy.html' }} />
  );
}
