import React, {useEffect, useState } from 'react';
import {Dimensions, TouchableOpacity, View,Linking } from 'react-native';
import { AppRoute } from '../../navigation/app-routes';
import { Image } from 'react-native-elements';
import { ScreenLayout } from '../screen-layout';
import { Icon } from 'react-native-elements'
import _ from 'lodash'
import { MtActivityIndicator, MtBodyText, MtGradientButton } from "../../components/ui-elements"
import { MemberStyles } from '../../styles';
import { getSystemList, getUser } from '../../services/api/api-service';
import { getLocalData } from '../../services/global-storage';
import { SliderBox } from "react-native-image-slider-box";
export const HomeScreen = (props) => {
  const [sliderImage, setSliderImage] = useState([]);
  const [sliderImageApiLoading, setSliderImageApiLoading] = useState(true);
  const [sliderImageSecond, setSliderImageSecond] = useState([]);
  const [sliderSecondApiLoading, setSliderSecondApiLoading] = useState(true);
  const [bannerImage, setBannerImage] = useState([]);
  const [bannerImageApiLoading, setBannerImageApiLoading] = useState(true);
  const images = [];
  for (let i = 0; i < sliderImage.length; i += 1) {
    const v = sliderImage[i].value;
    images.push(v);
  }
  const ImageSlider = [];
  for (let i = 0; i < sliderImageSecond.length; i += 1) {
    const v = sliderImageSecond[i].value;
    ImageSlider.push(v);
  }
  const mtGpsBanner = [];
  for (let i = 0; i < bannerImage.length; i += 1) {
    const v = bannerImage[i].value;
    mtGpsBanner.push(v);
  }
  useEffect(() => {
    getUser(getLocalData('userId')).then(data => {
      if (!_.isEmpty(data)) {
        //console.log(data[0].id + "--" + data[0].isTermsAgreed)
        _.get(data[0], "isTermsAgreed") ? data[0].isTermsAgreed ? "" : props.navigation.push(AppRoute.TERMS_CONDITIONS) : props.navigation.push(AppRoute.TERMS_CONDITIONS)
      }
    }).catch(e => {
      console.log("Error Get User")
    })
    getSystemList('sliderImage').then(data => {
      if (!_.isEmpty(data)) {
        setSliderImage(data)
        setSliderImageApiLoading(false)
      }
    }).catch(e => {
      console.log("Error in sliderImage:", e);
    })
    getSystemList('sliderImage-2').then(data => {
      if (!_.isEmpty(data)) {
        setSliderImageSecond(data)
        setSliderSecondApiLoading(false)

      }
    }).catch(e => {
      console.log("Error in sliderImage:", e);
    })
    getSystemList('MtGpsTopBanner').then(data => {
      if (!_.isEmpty(data)) {
        setBannerImage(data)
        setBannerImageApiLoading(false)
      }
    }).catch(e => {
      console.log("Error in Company Image:", e);
    })
  }, [])

  return (
    <ScreenLayout>
      {!_.isEmpty(bannerImage) &&
        <View style={MemberStyles.sliderView}>
          <SliderBox
            images={mtGpsBanner}
            sliderBoxHeight={120}
            onCurrentImagePressed={() => {Linking.openURL('https://moverstrip.com/')}}
            dotColor="#FFEE58"
            inactiveDotColor="#90A4AE"
            paginationBoxVerticalPadding={10}
            autoplay
            circleLoop
            resizeMethod={'resize'}
            resizeMode={'cover'}
            paginationBoxStyle={{
              position: "absolute",
              bottom: 0,
              padding: 0,
              paddingRight: 15,
              alignItems: "center",
              alignSelf: "center",
              justifyContent: "center",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.20,
              shadowRadius: 1.41,
              elevation: 5,
            }}
            dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 0,
              padding: 0,
              margin: 0,
              backgroundColor: "rgba(128, 128, 128, 0.92)"
            }}
            ImageComponentStyle={{ borderRadius: 10, width: '93%', marginLeft: -20, marginTop: 2, marginBottom: 3, }}
            imageLoadingColor="#2196F3"
          />
        </View>
      }
      {bannerImageApiLoading &&
        <MtActivityIndicator />
      }
      <View style={MemberStyles.rowView}>
        <MtGradientButton
          onPressHandler={() => props.navigation.push(AppRoute.NEED_LOAD_LIST)}
          buttonIcon={<Icon type="fontisto" name="truck" size={25} style={{ padding: 11, }} color="#f75019" />}
          buttonLabel="Available Truck"
        />
        <MtGradientButton
          onPressHandler={() => props.navigation.push(AppRoute.RATECARD)}
          buttonIcon={<Icon type="font-awesome" name="money" size={25} style={{ padding: 11, }} color="#f75019" />}
          buttonLabel="Rate Card"
        />
      </View>
      <View style={MemberStyles.rowView}>
        <MtGradientButton
          onPressHandler={() => props.navigation.push(AppRoute.POST_LOAD_LIST)}
          buttonIcon={<Icon type="font-awesome-5" name="truck-loading" size={25} style={{ padding: 11, }} color="#f75019" />}
          buttonLabel="Available Load"
        />
        <MtGradientButton
          onPressHandler={() => props.navigation.push(AppRoute.POST_LAOD)}
          buttonIcon={<Icon type="font-awesome-5" name="truck-moving" size={25} style={{ padding: 11, }} color="#f75019" />}
          buttonLabel="Post Load"
        />
      </View>
      <View style={MemberStyles.rowView}>
        <MtGradientButton
          onPressHandler={() => props.navigation.push(AppRoute.TRANSPORTER)}
          buttonIcon={<Icon type="fontisto" name="truck" size={25} style={{ padding: 11, }} color="#f75019" />}
          buttonLabel="Transporters"
        />
        <MtGradientButton
          onPressHandler={() => props.navigation.push(AppRoute.PACKERS_AND_MOVERS)}
          buttonIcon={<Icon type="font-awesome-5" name="box" size={25} style={{ padding: 11, }} color="#f75019" />}
          buttonLabel="Packers And Movers"
        />
      </View>
      {!_.isEmpty(images) &&
       <View style={MemberStyles.sliderView}>
        <SliderBox
          images={images}
          sliderBoxHeight={120}
          onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
          dotColor="#FFEE58"
          inactiveDotColor="#90A4AE"
          paginationBoxVerticalPadding={10}
          autoplay
          circleLoop
          resizeMethod={'resize'}
          resizeMode={'cover'}
          paginationBoxStyle={{
            position: "absolute",
            bottom: 0,
            padding: 0,
            paddingRight: 10,
            alignItems: "center",
            alignSelf: "center",
            justifyContent: "center",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.20,
            shadowRadius: 1.41,
            elevation: 5,
          }}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 0,
            padding: 0,
            margin: 0,
            backgroundColor: "rgba(128, 128, 128, 0.92)"
          }}
          ImageComponentStyle={{ borderRadius: 10, width: '93%', marginLeft: -20, marginTop: 2, }}
          imageLoadingColor="#2196F3"
        />
        </View>
        }
      {sliderImageApiLoading &&
        <MtActivityIndicator />
      }

      {!_.isEmpty(ImageSlider) &&
       <View style={MemberStyles.sliderView}>
        <SliderBox
          images={ImageSlider}
          sliderBoxHeight={120}
          onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
          dotColor="#FFEE58"
          inactiveDotColor="#90A4AE"
          paginationBoxVerticalPadding={10}
          autoplay
          circleLoop
          resizeMethod={'resize'}
          resizeMode={'cover'}
          paginationBoxStyle={{
            position: "absolute",
            bottom: 0,
            padding: 0,
            paddingRight: 10,
            alignItems: "center",
            alignSelf: "center",
            justifyContent: "center",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.20,
            shadowRadius: 1.41,
            elevation: 5,
          }}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 0,
            padding: 0,
            margin: 0,
            backgroundColor: "rgba(128, 128, 128, 0.92)"
          }}
          ImageComponentStyle={{ borderRadius: 10, width: '93%', marginLeft: -20,}}
          imageLoadingColor="#2196F3"
        />
        </View>
        }
      {sliderSecondApiLoading &&
        <MtActivityIndicator />
      }
    </ScreenLayout>
  );
}
