import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import _ from 'lodash'
import {MtAccordian, MtActivityIndicator, MtBodyText} from '../../components/ui-elements';
import {getFaq} from '../../services/api/api-service';

export const Faq = (props) => {
  const [faqList, setFaqList] = useState([]);
  const [faqListApiLoad, setFaqListApiLoad] = useState(true);
  useEffect(() => {
    getFaq().then(data => {
      if (!_.isEmpty(data)) {
        setFaqList(data)
        setFaqListApiLoad(false)
      }
    }).catch(e => {
      console.log("Error in Transporter list:", e);
    })
  }, [])
  return (
    <>
     
      <ScrollView style={{ marginHorizontal: 10, marginTop:10, marginBottom: 10, }}>
        {!_.isEmpty(faqList) &&
          <View style={{ marginTop: 10 }}>
            {faqList.map(item => (

              <MtAccordian
                key={item.id}
                title={item.faqQuestion}
                data={item.faqAnswer}
              />
            ))
            }
          </View>
        }
        {faqListApiLoad &&
          <MtActivityIndicator />
        }
      </ScrollView >
    </>
  );
}
const styles = StyleSheet.create({
  searchBox: {
    flex: 1,
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 1,
    padding: 12,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 2,
  },
  titleText: {
    fontSize: 15,
    color: '#000',
  },
})