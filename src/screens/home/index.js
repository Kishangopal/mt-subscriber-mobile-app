export { AboutScreen } from './about';
export {Faq} from './faq';
export {PrivacyPolicy} from './Privacy-policy';
export {Help} from './help'
export {TermsConditions} from './terms-conditions';
export { UpdateApp } from './update-app';