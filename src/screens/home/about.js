import React from 'react';
import {Text, View} from 'react-native';
import {Button} from 'react-native-elements';

export const AboutScreen=(props)=> {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Profile"
                onPress={navigateProfile}
            />
            <Button
                title="Go to Vehicle On Map"
                onPress={navigateVehicleOnMap}
            />
        </View>
    );
}
