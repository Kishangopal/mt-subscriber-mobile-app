import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {MtBodyText} from '../components/ui-elements';
import {ScreenLayout} from './screen-layout';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

export const DetailsScreen = ({ route }) => (
    <ScreenLayout>
      <MtBodyText>Details Screen</MtBodyText>
      {route.params.name && <MtBodyText>{route.params.name}</MtBodyText>}
      <MapView
      provider={PROVIDER_GOOGLE}
      initialRegion={{
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }}
      style={styles.mapStyle} />
    </ScreenLayout>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: 500,
  },
});