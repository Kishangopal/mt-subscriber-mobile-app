import React, { useState } from 'react';
import { MemberStyles } from '../../styles';
import { Dimensions, View,ActivityIndicator,StyleSheet} from 'react-native';
import { Image, Icon, Button } from 'react-native-elements';
import { MtImagePicker, MtBodyText } from "../../components/ui-elements";
import { uploadFile } from "../../services/api/api-service";
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import { cos } from 'react-native-reanimated';
export const ImageUpload = (props) => {
    const visitingCard = "https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/default-image.jpg";
    const [selectFile, setSelectFile] = useState(null);
    const [successUpload, setSuccessUpload] = useState(false);
    const [errorValue, setErrorValue] = useState(null);
    const [spinner, setSpinner] = useState(false);
    const componentDidMount=()=> {
        setInterval(() => {
            setSpinner(false)
        }, 3000);
    }
    //console.log(selectFile.assets[0].uri);
    const handleUploadPhoto = () => {
        setSpinner(true);
        componentDidMount();
        // console.log(selectFile);
        // console.log("buketName:"+props.bucketName);
        // console.log("UploadPath:"+props.UploadPath);
        if (selectFile) {
          uploadFile(selectFile, props.bucketName, props.UploadPath).then(data => {
                props.fileUrl(data.data.result.files.myFile[0].providerResponse.location);
               setSuccessUpload(true);
           }).catch(e => {
               console.log("Error in File Upload:", e);
           })
        }
        else {
            setErrorValue("File Not select")
        }
    };
    return (<>
        <MtBodyText style={MemberStyles.labeltext}>{props.titleLabel}
            {successUpload &&
                <Icon type="font-awesome" name="check" style={{ paddingLeft: 10, }} size={20} color="green" />
            }
        </MtBodyText>
        <MtImagePicker fileSelectValue={setSelectFile} />
        <View style={{ textAlign: 'center', }}>
            <Image
                source={{ uri: _.get(selectFile, "assets") ? selectFile.assets[0].uri : visitingCard }}
                style={{ height: 150, width: Dimensions.get('window').width-40, resizeMode: 'stretch', margin: 10, }}
                PlaceholderContent={<ActivityIndicator />}
           />
            <Button title="Upload Image" onPress={handleUploadPhoto} buttonStyle={{ borderRadius: 0, backgroundColor: "#039be5",width:"95%",alignSelf: 'center',marginTop:5, }}></Button>
        </View>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
    </>);
};
const styles = StyleSheet.create({
    spinnerTextStyle: {
      color: '#FFF'
    },});

