import React from 'react';
import AppIntroSlider from "react-native-app-intro-slider";
import {StyleSheet, Text, View,Image} from "react-native";
import { storeLocalData} from '../services/global-storage'

const Intro = (props) => {
    const _renderItem = ({ item }) => {
        return (
            <View style={{...styles.container,...{backgroundColor:item.backgroundColor}}}>
                <Text style={styles.title}>{item.title}</Text>
                {/* <Icon type="font-awesome-5" name={item.icon} size={128} color="white" /> */}
                <Image source={{uri:item.image}} style={styles.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    }
    const _onDone = () => {
        // After user finished the intro slides. Show real app through
        // navigation or simply by controlling state
        props.setShowRealApp(true);
        storeLocalData('showRealApp',true)
    };
    const _onSkip = () => {
        // After user skip the intro slides. Show real app through
        // navigation or simply by controlling state
        props.setShowRealApp(true);
        storeLocalData('showRealApp',true)
    };
    return (
        <AppIntroSlider renderItem={_renderItem} showSkipButton={true} data={slides} onSkip={_onSkip} onDone={_onDone}/>
    );
};

export default Intro;
const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: '40%', 
        height: 160,
        marginBottom:5,
        borderRadius:10,
    },
    text: {
        color: '#fff',
        fontSize: 20,
        marginTop:10,
        textAlign: 'center',
        fontFamily:'montserrat'
    },
    title: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginTop: 16,
        marginBottom:20,
        fontFamily:'montserrat-bold'
    },
});

const slides = [
    {
        key: 's1',
        text: 'Check Near By Vehicle Available',
        title: 'Live Truck Location',
        titleStyle: styles.title,
        textStyle: styles.text,
        icon: 'map-marker',
        image:'https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/tracking.png',
        imageStyle: styles.image,
        backgroundColor: '#0062b8',
    },
    {
        key: 's3',
        title: 'Business Listing Available',
        titleStyle: styles.title,
        text: 'All India Packers And Movers, Transporater List Available',
        icon: 'truck-monster',
        image:'https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/profile.png',
        imageStyle: styles.image,
        backgroundColor: '#0062b8',
    },
    {
        key: 's4',
        title: 'Load Available',
        titleStyle: styles.title,
        text: 'We are Provide Available Load List',
        icon: 'ambulance',
        image:'https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/delivery-truck.png',
        imageStyle: styles.image,
        backgroundColor: '#364b7d',
    }
];