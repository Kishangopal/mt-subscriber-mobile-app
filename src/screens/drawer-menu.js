import React, { useEffect, useState } from 'react';
import {Dimensions, FlatList, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Avatar } from 'react-native-elements';
import { AuthContext } from "../store/context";
import { MtBodyText } from "../components/ui-elements"
import Toast from 'react-native-toast-message';
import _ from "lodash"
import { Icon } from 'react-native-elements'
import { getUser } from '../services/api/api-service';
import { getLocalData } from '../services/global-storage';
import DeviceInfo from 'react-native-device-info';
import Config from '../constants/config'
import reducer from '../store/_reducer/localData';
export const DrawerMenu = (props) => {
  let version = DeviceInfo.getVersion();
  const [userName, setUserName] = useState([]);
  useEffect(() => {
    getUser(getLocalData('userId')).then(data => {
      if (!_.isEmpty(data)) {
        setUserName(data[0].contactName)
      }
    }).catch(e => {
      console.log("Error in Profile Starts:", e);
    })
  }, [])
  const { signOut } = React.useContext(AuthContext);
  const uri = "https://moverstrip-dev.s3.us-east-1.amazonaws.com/system/usericon.png";
  const arrMenu = [
    { 'id': 0, name: 'Home', 'icon': 'home-outline', 'navScreen': 'Home' },
    //{ 'id': 1, name: 'Vehicle', 'icon': 'truck', 'navScreen': 'Vehicle on Map' },
    { 'id': 2, name: 'Profile', 'icon': 'face-profile', 'navScreen': 'Profile' },
    //{ 'id': 3, name: 'My Walet', 'icon': 'wallet', 'navScreen': 'MyWaletScreen' },
    // { 'id': 4, name: 'Help', 'icon': 'help', 'navScreen': 'HelpScreen' },
    // { 'id': 5, name: 'Pricing', 'icon': 'format-list-bulleted', 'navScreen': 'Pricing' },
    //{ 'id': 6, name: 'KYC', 'icon': 'format-list-bulleted', 'navScreen': 'Kyc' },
    { 'id': 7, name: 'Change Password', 'icon': 'lock', 'navScreen': 'Change Password' },
    { 'id': 8, name: 'Post Load', 'icon': 'truck-check', 'navScreen': 'Post Load' },
    //{ 'id': 9, name: 'Packers And Movers', 'icon': 'format-list-bulleted', 'navScreen': 'Packers And Movers' },
    // { 'id': 10, name: 'Vehicle Register', 'icon': 'truck', 'navScreen': 'Vehicle Register' },
    //{ 'id': 11, name: 'Vehicle List', 'icon': 'format-list-bulleted', 'navScreen': 'Vehicle List' },
    // { 'id': 12, name: 'Vehicle Type List', 'icon': 'format-list-bulleted', 'navScreen': 'Vehicle Type List' },
    //{ 'id': 13, name: 'Find Vehicle Details', 'icon': 'format-list-bulleted', 'navScreen': 'Find Vehicle Details' },
    // { 'id': 14, name: 'Edit Post Load', 'icon': 'format-list-bulleted', 'navScreen': 'Edit Post Load' },
    //{ 'id': 15, name: 'Purchase History', 'icon': 'format-list-bulleted', 'navScreen': 'Purchase History' },
    { 'id': 16, name: 'Member Post Load', 'icon': 'format-list-bulleted', 'navScreen': 'Member Post Load' },
    // { 'id': 17, name: 'Post Load List', 'icon': 'format-list-bulleted', 'navScreen': 'Post Load List' },
    // { 'id': 18, name: 'Transactions', 'icon': 'format-list-bulleted', 'navScreen': 'Transactions' },
    // { 'id': 19, name: 'Purchase History', 'icon': 'format-list-bulleted', 'navScreen': 'Purchase History' },
   // { 'id': 20, name: 'Rate Card', 'icon': 'format-list-bulleted', 'navScreen': 'Rate Card' },
    //{ 'id': 21, name: 'Live Vehicle list', 'icon': 'truck', 'navScreen': 'Live Vehicle list' },
    //{ 'id': 22, name: 'Transporter', 'icon': 'format-list-bulleted', 'navScreen': 'Transporter' },
    { 'id': 23, name: 'Notification', 'icon': 'bell-outline', 'navScreen': 'Notification' },
    { 'id': 24, name: 'Privacy Policy', 'icon': 'file-outline', 'navScreen': 'Privacy Policy' },
    { 'id': 25, name: 'Help', 'icon': 'help-circle', 'navScreen': 'Help' },
    //{ 'id': 26, name: 'Terms And Conditions', 'icon': 'file-outline', 'navScreen': 'Terms And Conditions' },
    
    { 'id': 27, name: 'FAQ', 'icon': 'file-question', 'navScreen': 'FAQ' },
    
    //{ 'id': 27, name: 'Vehicle Reports', 'icon': 'truck', 'navScreen': 'Vehicle Reports' },
    //{ 'id': 28, name: 'Vehicle Details Live', 'icon': 'truck', 'navScreen': 'Vehicle Details Live' },
    { 'id': 29, name: 'Log out', 'icon': 'logout', 'navScreen': 'signOut' },
  ]
  const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
  const renderFlatList = () => {
    return (
      <FlatList
        scrollEnabled={(screenHeight >= 400) ? true : false}
        data={arrMenu}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigateToScreen(item.navScreen)}>
            <View style={{
              height: 55,
              borderBottomWidth: 1, width: "90%",
              borderBottomColor: "#f6f6f6", flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'
            }}>
              <Icon type="material-community" name={item.icon} size={28} style={{ paddingLeft: 20, color: '#333' }} />
              <MtBodyText style={styles.menuText}>{item.name}</MtBodyText>
            </View>
          </TouchableOpacity>
        )}>
           
        </FlatList>
       
    );
  }

  const navigateToScreen = (navScreen) => {
    if (navScreen == 'signOut') {
      Toast.show({
        text1: 'User logged out',
        text2: 'User loggout successfully.'
      });
      
      signOut();
    }

    else props.navigation.navigate(navScreen)
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="blue" barStyle="light-content" />
      <View style={styles.headerContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
          <View style={{ marginLeft: 15 }}>
            <Avatar rounded source={{ uri: uri }} />
          </View>
          <MtBodyText style={{ fontSize: 20, fontWeight: 'bold', paddingLeft: 10 }}>{userName.length > 30 ? userName.substring(0, 30) + "..." : userName}</MtBodyText>
        </View>
      </View>
      <View style={styles.menuContainer}>
        {renderFlatList()}
      </View>
      <View style={{ marginBottom: 20 }}>

      </View>
      <View style={[styles.footerContainer, { bottom: (screenHeight > 400) ? 0 : 5 }]}>
      <MtBodyText style={styles.footerText}>Code Version: {version}/{Config.codepushVersion}</MtBodyText>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  headerContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  menuContainer: {
    flex: 6,
    justifyContent: 'center'
  },
  headerText: {
    fontSize: 30,
    color: '#333',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  menuText: {
    fontSize: 15,
    textAlign: 'center',
    marginLeft: 20
  },
  footerContainer: {
    flex: 1,
    justifyContent: 'center',
    height: 30,
    backgroundColor:'#fff',
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0
  },
  footerText: {
    fontSize: 12,
    color: '#999',
    textAlign: 'center',
    marginBottom: 8
  },
});