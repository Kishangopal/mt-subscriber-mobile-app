import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer, persistStore} from 'redux-persist';
import rootReducer from './_reducer';
import {applyMiddleware, compose, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from "redux-thunk";
import {Platform} from 'react-native'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['localData'],
};


const persistedReducer = persistReducer(persistConfig, rootReducer);
let middleware, composeEnhancers;
if (Platform.OS === 'ios') {
  composeEnhancers = (__DEV__) ? (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose) : compose;
  middleware = [
    thunk,
    __DEV__ && createLogger(),
  ].filter(Boolean);
} else {
  composeEnhancers = compose;
  middleware = [thunk]
}


const store = createStore(
  persistedReducer, composeEnhancers(applyMiddleware(...middleware)),
);
const persistedStore = persistStore(store);
export {store, persistedStore};
