import React, { useEffect, useState } from 'react';
// import * as Font from 'expo-font';
// import AppLoading from 'expo-app-loading'
import { ActivityIndicator } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { AuthContext } from "./store/context";
import { AppNavigator } from './navigation/app.navigator';
import { Provider } from 'react-redux';
import { persistedStore, store } from './store/store';
import { PersistGate } from 'redux-persist/integration/react';
import Intro from "./screens/Intro";
import _ from 'lodash'
import { getLocalData, removeLocalData, storeLocalData } from "./services/global-storage"
import apiKit from './services/api/axios-base';
import { getPublicList } from './services/api/api-service';
import SplashScreen from 'react-native-splash-screen'
import codePush from "react-native-code-push";
import UpdateApp from './screens/update-app'; 
import DeviceInfo from 'react-native-device-info';
import { number } from 'yup';
let version = DeviceInfo.getVersion();
const codePushDeploymentKeyAndroid = "_yJUfQFJR9fqUhvmSuyZ3IhhqYUoJFSkVRg-3"
const App = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [showRealApp, setShowRealApp] = useState(false);
  const [fontLoaded, setFontLoaded] = useState(false);
  const [userToken, setUserToken] = React.useState(null);
  const [currectAppVersionStatus, setCurrectAppVersionStatus] = useState(false);
  const [linkValue, setLinkValue] = useState(null);
  apiKit.interceptors.response.use(
    res => res,
    err => {
      if (err && err.response && err.response.status === 401) {
        removeLocalData('accessToken')
        setUserToken(null)
      }
      return Promise.reject(err)
    }
  );
  function getAppUpdate() {
    let userAppVersion = version.replace(/\./g,"");
    //console.log(`application version:-${userAppVersion}`)
    getPublicList('MoverstripAppUpdate').then(data => {
      if (!_.isEmpty(data)) {
        let updateVersion=data[0].label.replace(/\./g,"");
        // console.log("link:-"+data[0].value)
        // console.log("App version: ",Number(updateVersion))
        // console.log("user version: ",Number(userAppVersion))
        setLinkValue(data[0].value)
        if(Number(updateVersion)>Number(userAppVersion))
        {
          setCurrectAppVersionStatus(true);
          // console.log("Update Option 2");
          // console.log("CurrectAppVersion 2:-"+currectAppVersionStatus);
        }
        else
        {
          //console.log("Update Option 1");
          setCurrectAppVersionStatus(false);
          //console.log("CurrectAppVersion 1:-"+currectAppVersionStatus);
        }
        
      }
     }).catch(e => {
      console.log("Error in State list:", e);
    })
  }
  useEffect(() => {
    getAppUpdate();
    if (!fontLoaded) {
      // bootstrapAsync();
      // fetchFonts();
      setFontLoaded(true);
      try {
        SplashScreen.hide()
      } catch (e) {
        //Splash Issue
      }
    }
  }, [])
  const authContext = React.useMemo(() => {
    return {
      signIn: (token) => {
        console.log("Sign in...")
        setIsLoading(false);
        storeLocalData("userToken", token);
        setUserToken(token);
      },
      signUp: () => {
        setIsLoading(false);
        setUserToken("MyToken");
      },
      signOut: () => {
        setIsLoading(false);
        setUserToken(null);
        removeLocalData('accessToken')

      }
    };
  }, []);
  const bootstrapAsync = async () => {
    try {
      await setShowRealApp(getLocalData('showRealApp'))
      await setUserToken(getLocalData('userToken'))
      console.log("showRealApp", getLocalData('showRealApp'))
      console.log("userToken:", getLocalData('userToken'))
      
    } catch (e) {
      console.log("There are some issue with bootstrapAsync")
    }
  };
  // if (!ready || !fontsLoaded) {
  //   initAsync().then(() => setReady(true))
  //   return null
  // }

  return (
    (currectAppVersionStatus)?<UpdateApp linkValueLink={linkValue} />:
    <Provider store={store}>
      <PersistGate loading={<ActivityIndicator />}
        persistor={persistedStore}
        onBeforeLift={bootstrapAsync}
      ></PersistGate>
      {(showRealApp) ? <AuthContext.Provider value={authContext}>
        <SafeAreaProvider>
          <NavigationContainer>
            <AppNavigator userToken={userToken} />
          </NavigationContainer>
        </SafeAreaProvider>
      </AuthContext.Provider> :<Intro setShowRealApp={setShowRealApp} />}

    </Provider>
  );

}
export default codePush({ deploymentKey: codePushDeploymentKeyAndroid })(App);