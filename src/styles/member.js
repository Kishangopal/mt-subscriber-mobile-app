import { Dimensions, StyleSheet } from "react-native";

export const MemberStyles = StyleSheet.create({
  meanBox: {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 1,
    shadowColor: "#000",
    paddingBottom: 40,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
    marginBottom: 5,
  },
  boxHeader: {
    height: 120,
    borderTopEndRadius: 5,
    borderTopStartRadius: 5,
    backgroundColor: "rgba(85, 110, 230, 0.25) !important",
    width: '100%',
  },
  boxBody: {
    flex: 1,
    width: '100%',
    zIndex: 1,
    marginTop: 50,
  },
  Logobox: {
    flex: 1,
    backgroundColor: "#eff2f7",
    height: 80,
    width: 80,
    position: 'absolute',
    marginTop: 80,
    marginLeft: 10,
    borderRadius: 50,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImage:
  {
    width: 70,
    height: 70,
  },
  headerImage:
  {
    position: 'absolute',
    marginVertical: 10,
    height: 100,
    width: 100,
    alignSelf: 'flex-end',
  },
  headingTitle:
  {
    fontSize: 20,
    color: '#3949ab',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  userText:
  {
    fontSize: 16,
    marginHorizontal: 20,
    fontWeight: 'bold',
  },
  userRedText:
  {
    fontSize: 13,
    marginHorizontal: 20,
    fontWeight: 'bold',
    color: 'red',
  },
  userSubText:
  {
    fontSize: 12,
    marginHorizontal: 20,
  },
  rowflex:
  {
    //flex:1,
    flexDirection: 'row',
    borderBottomColor: '#eff2f7',
    borderBottomWidth: 1,
    marginBottom: 5,
  },
  lableView:
  {
    flexDirection: "column",
    flex: 1,
    padding: 2,
  },
  lableData:
  {
    flexDirection: "column",
    flex: 1,
    padding: 2,
  },
  headingTitleStyle:
  {
    fontSize: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    fontWeight: 'bold',
    borderBottomColor: '#eff2f7',
    borderBottomWidth: 1,
  },
  //Formlayout
  labeltext:
  {
    fontSize: 15,
    marginVertical: 5,
    marginHorizontal: 10,
  },
  //pricingCardStyle
  cardTitle:
  {
    fontSize: 30,
    fontFamily: 'montserrat-bold',
  },
  pringStyle:
  {
    fontSize: 35,
    fontFamily: 'montserrat-bold',
  },
  HeadingSection:
  {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  //list card
  cardBox: {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 10,
    shadowColor: "#000",
    paddingBottom: 1,
    paddingTop: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 5,
    borderBottomColor: '#343deb',
    borderBottomWidth: 2,
  },
  cardTitleStyle:
  {
    fontSize: 16,
    paddingHorizontal: 15,
    paddingVertical: 5,
    fontWeight: 'bold',
    borderBottomColor: '#eff2f7',
    borderBottomWidth: 1,
  },
  cardbodystyle:
  {
    paddingHorizontal: 10,
    paddingVertical: 5,

  },
  cardtext:
  {
    fontWeight: '200',
    fontSize: 15,
    marginVertical: 5,
    marginHorizontal: 10,
    color: "#37474f",
  },
  cardlefttext:
  {
    fontWeight: '200',
    fontSize: 15,
    marginVertical: 5,
    marginHorizontal: 10,
    color: "#37474f",
  },
  cardFooter:
  {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 20,
    paddingTop: 10,
    borderTopColor: '#eff2f7',
    borderTopWidth: 1,
  },
  cardFooterColumn:
  {
    flex: 1,
    flexDirection: 'row',
    borderTopColor: '#eff2f7',
    borderTopWidth: 1,
  },
  cardFooterNoBorder:
  {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 20,
    paddingTop: 10,
  },
  footetItem:
  {
    flex: 1,
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardHeadBox:
  {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    flexDirection: 'row',
    borderRadius: 1,
    shadowColor: "#000",
    paddingBottom: 10,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
  },
  resultBox:
  {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    flexDirection: 'row',
  },
  FixHeadresultBox:
  {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    flexDirection: 'row',
  },
  labelHeadtext:
  {

    fontSize: 18,
    fontWeight: 'bold',
    marginVertical: 1,
    marginHorizontal: 10,
    alignSelf: 'flex-start',
  },
  subHeadingStyle:
  {
    fontSize: 20,
    color: '#34c38f',
    marginVertical: 10,
    marginHorizontal: 20,
    fontWeight: 'bold',
  },
  rowflexbox:
  {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  notificationTitle:
  {
    fontSize: 18,
    fontWeight: 'bold',
    marginHorizontal: 10,
    marginVertical: 5,
  },
  notificationDescription:
  {
    fontSize: 15,
    marginHorizontal: 10,
    marginVertical: 2,
  },
  notificationtime:
  {
    fontSize: 15,
    marginHorizontal: 10,
    marginVertical: 2,
    paddingTop: 2,
    alignContent: 'flex-end',
    color: 'red',
  },
  footerleft:
  {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  footerRight:
  {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  notificaionBox:
  {
    flex: 1,
    fontFamily: 'montserrat-bold',
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 5,
    shadowColor: "#000",
    paddingBottom: 10,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 10,
  },
  //mapstyle
  greentext:
  {
    fontSize: 15,
    color: 'green',
    marginHorizontal: 10,
    marginVertical: 2,
    paddingTop: 2,
    alignContent: 'flex-end',
    fontWeight: 'bold',
  },
  redtext:
  {
    fontSize: 15,
    color: 'red',
    marginHorizontal: 10,
    marginVertical: 2,
    paddingTop: 2,
    alignContent: 'flex-end',
    fontWeight: 'bold',
  },
  yellowtext:
  {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#ffb100',
    marginHorizontal: 10,
    marginVertical: 2,
    paddingTop: 2,
    alignContent: 'flex-end',
  },
  containerMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerFilterBox:
  {
    flexDirection: 'row',
    position: 'absolute',
    backgroundColor: '#eee',
    height: 45,
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flex: 1,
    padding: 3,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 6,
  },
  footerBox: {
    position: 'absolute',
    backgroundColor: '#0984e3',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    bottom: 15,
    right: 10,
    flex: 1,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 15,
  },
  topFilterBox:
  {
    flex: 1,
    flexDirection: 'row',
  },
  movingButton:
  {
    backgroundColor: '#28ca00',
    paddingHorizontal: 5,
    marginHorizontal: 5,
  },
  offlineButton:
  {
    backgroundColor: '#f3a900',
    paddingHorizontal: 5,
    marginHorizontal: 5,
  },
  stoppedButton:
  {
    backgroundColor: '#f31600',
    paddingHorizontal: 5,
    marginHorizontal: 5,
  },
  filterButton:
  {
    borderColor: '#D3D3D3',
    borderWidth: 1,
    paddingHorizontal: 5,
    marginHorizontal: 2,
    borderRadius: 2,
    width: "23%",
    alignItems: 'center',
    justifyContent: 'center'
  },
  listBox:
  {
    flex: 1,
    fontFamily: 'montserrat-bold',
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 3,
    shadowColor: "#000",
    paddingBottom: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.11,
    elevation: 2,
    marginTop: 10,
  },
  //Dashboard
  valuetext:
  {
    fontSize: 16,
    marginHorizontal: 45,
    fontWeight: 'bold',
  },
  chartBox: {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 5,
    paddingTop: 5,
    marginBottom: 5,
  },
  rowView: {
    flex: 1,
    flexDirection: 'row',
  },
  rowcol: {
    flex: 1,
    flexDirection: 'column',
    //backgroundColor: "#0066cc",
    margin: 5,
    borderWidth: .10,
    borderColor: "#000",
    borderRadius: 5,
    shadowColor: "#0066cc",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.50,
    shadowRadius: 1.41,
    elevation: 5,
  },
  rowColNoShadow: {
    flex: 1,
    flexDirection: 'column',
    margin: 5,
  },
  centerText:
  {
    padding: 2,
    fontSize: 15,
    marginTop: 2,
    marginBottom: 10,
    fontWeight: 'bold',
    color: "#101e4a",
    justifyContent: "center",
    alignSelf: 'center',
    alignItems: 'center'
  },
  TouchableOpacitystyle: {
    flex: 1,
  },
  bodyLayout: {
    flex: 1,
    flexDirection: 'row',
  },
  card: {
    flex: 1,
    width: "45%",
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 5,
    flexDirection: 'column',
    margin: 3,

  },
  cardBody: {
    flex: 2,
    width: "100%",
    justifyContent: "center",
    alignSelf: 'center',
    alignItems: 'center',
    padding: 10,
  },
  cardFooter: {
    flex: 1,
    width: "100%",
    backgroundColor: '#dfe6e9',
    padding: 5,
    justifyContent: "center",
    alignSelf: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
  },
  cardTitle:
  {
    fontSize: 16,
    fontFamily: 'montserrat',
    marginBottom: 5,
  },
  cardValue:
  {
    fontSize: 20,
    marginBottom: 5,
    fontFamily: 'montserrat',
    color: "#0984e3"
  },
  subCardValue:
  {
    fontSize: 12,
    fontFamily: 'montserrat',
    color: "#636e72",
  },
  footertext:
  {
    fontSize: 14,
    fontFamily: 'montserrat',
  },
  headbox:
  {
    flex: 1,
    width: "100%",
    backgroundColor: '#fff',
    padding: 9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.2,
    elevation: 2,
    marginBottom: 10,

  },
  headerReportBox:
  {
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    flex: 1,
    padding: 3,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginBottom: 50,
  },
  FixHeadBox:
  {
    backgroundColor: '#fff',
    width: '100%',
    padding: 3,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    zIndex: 0,
  },
  headerMapBox:
  {
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    flex: 1,
    padding: 3,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 5,
  },
  //Default Screen layout
  DefaultScreenBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: Dimensions.get('window').height - 180,
    width: Dimensions.get('window').width,
    margin: -10,
  },
  //reports style
  reportsDetaislHeading:
  {
    fontSize: 17,
    marginHorizontal: 5,
    paddingBottom: 4,
    borderBottomColor: '#d5d5d5',
    borderBottomWidth: 1,
    marginBottom: 5,
  },
  scrollViewBox: {
    marginHorizontal: 1,
    marginTop: 80,
    marginBottom: 10,
  },
  scrollViewList: {
    marginHorizontal: 1,
    marginTop: 40,
    marginBottom: 30,
  },
  scrollViewListFixBox: {
    marginHorizontal: 1,
    marginTop: -30,
    marginBottom: 30,
  },
  //Terms conditons
  bodyLayoutWhite:
  {
    flex: 1,
    backgroundColor: '#fff',
    margin: -10,
    height: Dimensions.get('window').height - 10,
    width: Dimensions.get('window').width,
  },
  headTitleName:
  {
    fontSize: 15,
    color: '#000',
    paddingVertical: 10,
    paddingHorizontal: 10,
    fontFamily: 'montserrat-bold'
  },
  bodyText:
  {
    fontSize: 12,
    color: '#000',
    paddingHorizontal: 10,
    marginTop: 10,
    textAlign: 'justify',
    fontFamily: 'montserrat'
  },
  cardHead: {
    padding: 3,
    backgroundColor: '#d5d5d5'
  },
  //terms
  headerTopFixedBox: {
    //position: 'absolute',
    backgroundColor: '#556ee6',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.20,
    shadowRadius: 3.41,
    elevation: 10,
    marginBottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    right: 0,
    left: 0,
    flex: 1,
  },
  checkboxContainer: {
    flexDirection: "row",
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10,
  },
  headTitleStyle:
  {
    fontSize: 20,
    color: '#000',
    marginBottom: 10,
    color: '#fff',
    fontFamily: 'montserrat-bold'
  },
  FooterFixedBox:
  {
    backgroundColor: '#fff',
    padding: 3,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.20,
    shadowRadius: 3.41,
    elevation: 10,
    marginBottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 5,
    right: 0,
    left: 0,
    flex: 1,
  },
  headBoxStyle:
  {
    flex: 1,
    width: "100%",
    backgroundColor: '#3244a8',
    padding: 9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.2,
    elevation: 2,
    marginBottom: 10,

  },
  whiteText:
  {
    color: '#fff',
    fontSize: 20,
  },
  //Terms and  conditions
  headerFixedBox:
  {

    backgroundColor: '#fff',
    padding: 3,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.20,
    shadowRadius: 3.41,
    elevation: 10,
    marginBottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 5,
    right: 0,
    left: 0,
    flex: 1,
  },
  checkbox: {
    backgroundColor: '#556ee600',
    borderWidth: 0,
    padding: 0,
    margin: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Checkboxlabel: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  titleHeading:
  {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 20,
    paddingVertical: 10,
    fontFamily: 'montserrat-bold'
  },
  screenLayoutView: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  cityListType: {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 2,
    shadowColor: "#000",
    padding: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginTop: 8,
    borderLeftColor: '#343deb',
    borderLeftWidth: 5,
  },
  sliderView: {
    width: "100%",
    padding: 1,
    marginTop: 10,
    borderRadius: 10,
  },
  FixFooterBox:
  {
    width: '100%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex:1,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.50,
    shadowRadius: 1.41,
    elevation: 5,
    shadowColor: "#000",
    borderTopColor:"#d5d5d5",
    borderTopWidth:2,
  },
})