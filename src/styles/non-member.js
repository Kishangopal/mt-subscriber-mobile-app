import { StyleSheet } from "react-native";

export const NonMemberStyles = StyleSheet.create({
  mainbox: {
    flex: 1,
    backgroundColor: "#fff",
    width: '100%',
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    paddingBottom: 20,
    elevation: 2,
    marginTop: 10,
  },
  boxHeader: {
    height: 120,
    borderTopEndRadius: 5,
    borderTopStartRadius: 5,
    backgroundColor: "rgba(85, 110, 230, 0.25) !important",
    width: '100%',
  },
  boxBody: {
    flex: 1,
    width: '100%',
    zIndex: 1,
    marginTop: 50,
  },
  boxform:
  {
    alignItems: 'center',
    justifyContent: 'center',
  },
  Logobox: {
    flex: 1,
    backgroundColor: "#eff2f7",
    height: 80,
    width: 80,
    position: 'absolute',
    marginTop: 80,
    marginLeft: 10,
    borderRadius: 50,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImage:
  {
    width: 50,
    height: 50,
  },
  headerImage:
  {
    position: 'absolute',
    marginVertical: 10,
    height: 100,
    width: 100,
    alignSelf: 'flex-end',
  },
  headerCoverImage:
  {
    position: 'absolute',
    resizeMode: "stretch",
    alignSelf: "flex-start",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  headingTitle:
  {
    fontSize: 20,
    color: '#3949ab',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  headingSubTitle:
  {
    fontSize: 16,
    color: '#3949ab',
    paddingHorizontal: 20
  },
  footBox:
  {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 20,
    flexDirection: 'row'
  },
  footerLinkText:
  {
    paddingLeft: 5,
    color: '#74788d',
  },
  checkboxContainer: {
    flexDirection: "row",
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  checkboxContainerCenter: {
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkbox: {
    backgroundColor: '#556ee600',
    borderWidth: 0,
    padding: 0,
    margin: 0,
  },
  Checkboxlabel: {
    fontWeight: 'bold',
  },
  footFormBox:
  {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    flexDirection: 'row'
  },
  leftAlignBox:
  {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  labeltext:
  {
    fontSize: 15,
    marginVertical: 5,
    marginHorizontal: 10,
  },
  dropdownBoxStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop:10,
    paddingBottom:13,
    marginHorizontal: 10,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#e0e0e0',
    borderRadius: 1,
    marginBottom: 10,
  }
})