import {StyleSheet} from "react-native";
import Config from '../constants/config';
import {normalizeSize} from "../utils/utils";

export const GlobalStyles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  pageContainerWithoutHeader: {
    flex: 1,
    justifyContent: 'center',

  },
  recordContainer: {
    marginHorizontal: 10,
    paddingHorizontal: 8,
    marginVertical: 3,
    height: "auto",
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderWidth: 1,
    borderColor: '#d3d3d3',
    borderRadius: 3,
    padding: 5,
    shadowOffset: { width: 0, height: 1 },
    shadowColor: '#CCC',
    shadowOpacity: 0.9,
    backgroundColor: "#FFF",
    elevation: 1.5
  },
  linkText: {
    color: Config.linkTextColor,
    fontSize: normalizeSize(12),
    fontFamily: 'montserrat-bold'
  },
  titleText: { color: Config.bodyTextColor, fontSize: normalizeSize(14), fontFamily: 'montserrat-bold' },
  searchBarContainer: {
    backgroundColor: 'white',
    width: '100%',
    borderTopColor: 'white',
    borderBottomColor: '#E6E6D6',
    borderWidth: 0,
    padding: 0
  },
  pageHeaderText: {
    color: Config.bodyTextColor,
    fontSize: normalizeSize(16),
    fontFamily: 'montserrat-bold',
    paddingBottom: 10
  },
  searchBarInputText: { color: 'rgb(18, 125, 84)', fontSize: normalizeSize(14) },
  SearchBarInputContainer: { backgroundColor: 'white' },
  bodyText: {
    fontFamily: 'montserrat',
    color: Config.bodyTextColor,
    fontSize: normalizeSize(12)
  },
});
const styles = {
  mini: {
    fontSize: normalizeSize(13),
  },
  small: {
    fontSize: normalizeSize(16),
  },
  medium: {
    fontSize: normalizeSize(17),
  },
  large: {
    fontSize: normalizeSize(20),
  },
  xlarge: {
    fontSize: normalizeSize(24),
  },
};

