import * as Yup from 'yup';

export const ProfileValidationSchema = Yup.object().shape({
  companyName: Yup.string()
  .label('CompanyName')
  .required('Please enter a registered Company Name'),
  contactNumber: Yup.number()
  .label('ContactNumber')
  .required('Please enter a registered Contact Number'),
  address: Yup.string()
  .label('Address')
  .required('Please enter a registered Address'),
  pin: Yup.string()
  .label('Pin')
  .required('Please enter a registered Pin'),
  contactPerson: Yup.string()
  .label('ContactPerson')
  .required('Please enter a Contact Person Name'),
})