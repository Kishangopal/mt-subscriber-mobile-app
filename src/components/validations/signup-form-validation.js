import * as Yup from 'yup';
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
export const SignUpSchema = Yup.object().shape({
  contactName: Yup.string()
    .label('contactName')
    .required('Please enter a User Name'),
  contactNumber: Yup.string()
    .label('ContactNumber')
    .required('Please enter a Contact Number')
    .matches(/^[0-9]+$/, "Must be only Numbers")
    .min(10, 'Must be exactly 10 digits')
    .max(10, 'Must be exactly 10 digits'),
  email: Yup.string()
    .email('Enter Valid Email')
    .label('Email'),
  password: Yup.string()
    .label('Password')
    .required('Password is Short')
    .min(6, 'Seems a bit short...'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Confirm Password must matched Password')
    .min(6, 'Seems a bit short...')
    .required('Confirm Password is required')
})