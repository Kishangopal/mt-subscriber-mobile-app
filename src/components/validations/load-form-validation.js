import * as Yup from 'yup';

export const  LoadValidationSchema = Yup.object().shape({
    pickupLocation: Yup.string()
        .label('PickupLocation')
        .required('Please enter a Pickup Loaction'),
    destination: Yup.string()
        .label('Destination')
        .required('Please enter a Destination'),
    specialInstruction: Yup.string()
        .label('SpecialInstruction')
        .required('Please enter a SpecialInstruction')
})