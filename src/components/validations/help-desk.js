import * as Yup from 'yup';

export const HelpDesk = Yup.object().shape({
    title: Yup.string()
    .label('Title')
    .required('Please Enter Your Issue'),
     detail: Yup.string()
    .label('Detail')
    .required('Please Enter Your Detail'),
    phoneNumber: Yup.string()
    .label('PhoneNumber')
    .required('Please Enter Phone Number'),
})