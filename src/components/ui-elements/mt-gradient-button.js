import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { MemberStyles } from "../../styles";
import { AppRoute } from '../../navigation/app-routes';
import { MtBodyText } from "../../components/ui-elements"
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements'
export const MtGradientButton = (props) => {
  return (<>
      <LinearGradient
        start={{ x: 1, y: 1 }} //here we are defined x as start position
        end={{ x: 0, y: 0 }} //here we can define axis but as end position
        colors={['#fff', '#fff']} style={MemberStyles.rowcol}>
        <TouchableOpacity onPress={props.onPressHandler} >
          {props.buttonIcon}
          <MtBodyText style={MemberStyles.centerText}>{props.buttonLabel}</MtBodyText>
        </TouchableOpacity></LinearGradient>
  </>);
};