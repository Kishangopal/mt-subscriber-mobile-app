import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { MemberStyles } from "../../styles";
import { MtBodyText } from "../../components/ui-elements"
import LinearGradient from 'react-native-linear-gradient';
export const MtIconLinkButton = (props) => {
  return (<>
      <LinearGradient
        start={{ x: 1, y: 1 }} //here we are defined x as start position
        end={{ x: 0, y: 0 }} //here we can define axis but as end position
        colors={props.bgColor} style={MemberStyles.rowcol}>
        <TouchableOpacity onPress={props.onPressHandler} >
          {props.buttonIcon}
          {/* <MtBodyText style={MemberStyles.centerText}>{props.buttonLabel}</MtBodyText> */}
        </TouchableOpacity></LinearGradient>
  </>);
};