import React, { useEffect, useState } from 'react';
import { GlobalStyles } from "../../styles";
import { launchImageLibrary,launchCamera } from 'react-native-image-picker';
// import * as ImagePicker from 'expo-image-picker';
// import * as Permissions from 'expo-permissions';
import { BottomSheet, Button, ListItem, Icon,Text } from 'react-native-elements';
// import Constants from 'expo-constants';
export const MtImagePicker = (props) => {
    const [errorValue,setErrorValue]=useState(null);
    // const list = [
    //     {
    //         title: 'Pick from Camera Roll',
    //         onPress: () => {
    //             _pickImageCamera()
    //             setIsBottomVisible(false)
    //         },
    //         iconView:<Icon type="feather" name="camera" style={{paddingLeft:2}} size={20} color="#666" />,
    //         // containerStyle: { borderBottomWidth:1,borderBottomColor: '#d2d2d2' },
    //         titleStyle: {...GlobalStyles.bodyText,},
    //     },
    //     {
    //         title: 'Pick from Gallery',
    //         onPress: () => {
    //             _pickImageMedia()
    //             setIsBottomVisible(false)
    //         },
    //         iconView:<Icon type="font-awesome" name="file-image-o" style={{paddingLeft:2}} size={20} color="#666" />,

    //         // containerStyle: { borderBottomWidth:1,borderBottomColor: '#d2d2d2' },
    //         titleStyle: GlobalStyles.bodyText,
    //     },
    //     {
    //         title: 'Cancel',
    //         containerStyle: { backgroundColor: '#d2d2d2' },
    //         titleStyle: GlobalStyles.bodyText,
    //         iconView:<Icon type="MaterialIcons" name="cancel" style={{paddingLeft:2}} size={20} color="#666" />,
    //         onPress: () => setIsBottomVisible(false),
    //     },
    // ];
    // const getPermissionAsync = async () => {
    //     if (Constants.platform.ios) {
    //         const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    //         if (status !== 'granted') {
    //             alert('Sorry, we need camera roll permissions to make this work!');
    //         }
    //     }
    // }
    // useEffect(() => {
    //     getPermissionAsync();
    // }, []);

    // const _pickImageCamera = async () => {
    //     let result = await ImagePicker.launchCameraAsync({
    //         mediaTypes: ImagePicker.MediaTypeOptions.All,
    //         allowsEditing: true,
    //         aspect: [4, 3],
    //         quality: 1
    //     });

    //     console.log(result);

    //     if (!result.cancelled) {
    //         setUploadFile({ image: result.uri });
    //     }
    // };

    // const _pickImageMedia = async () => {
    //     let result = await ImagePicker.launchImageLibraryAsync({
    //         mediaTypes: ImagePicker.MediaTypeOptions.All,
    //         allowsEditing: true,
    //         aspect: [4, 3],
    //         quality: 1
    //     });

    //     console.log(result);

    //     if (!result.cancelled) {
    //         setUploadFile({ image: result.uri });
    //     }
    // };
    
    const [photo, setPhoto] = React.useState(null);
    const handleChoosePhoto = () => {
        setErrorValue(null);
        launchImageLibrary({ noData: true }, (response) => {
            //console.log(response["assets"][0].uri);
            if (response) {
                //setPhoto(response["assets"][0]);
                if (!response.assets[0].uri.match(/.(jpg|jpeg|png)$/i)) { setErrorValue("Image Extension No Valid") }
                else {
                    response.assets[0].fileSize >= 5120 && response.assets[0].fileSize <= 371680 ? (
                        props.fileSelectValue(response)
                    ) :
                        (
                            setErrorValue("Image minimum size can be 5kb and maximum size can be 300kb")
                        )
                }
                
            }
        });
    };
    return (<>
        {/* <BottomSheet isVisible={isBottomVisible}>
            {list.map((l, i) => (
                <ListItem key={i} bottomDivider containerStyle={l.containerStyle} onPress={l.onPress}>
                    {l.iconView}
                    <ListItem.Content>
                        <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                    </ListItem.Content>
                </ListItem>
            ))}
        </BottomSheet> */}
        <Button
            title={props.buttonTitle || "Upload your file/image"}
            icon={
                <Icon type="font-awesome" name="cloud-upload" style={{ paddingLeft: 10 }} size={24} color="#bdbdbd" />
            }
            iconRight
            titleStyle={GlobalStyles.bodyText}
            type="clear"
            containerStyle={props.containerStyle || { borderColor: '#f2f2f2', borderWidth: 1, width: "95%", marginLeft: 10, marginright: 10, }}
            onPress={handleChoosePhoto}
        />
        <Text style={{color:'red',fontSize:10,marginLeft:10,}}>{errorValue}</Text>
    </>);
};
