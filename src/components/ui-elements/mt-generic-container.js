import React from "react";
import {Image, View} from "react-native";
import {MtBodyText} from './mt-body-text';
import {NonMemberStyles} from '../../styles';
import _ from 'lodash';
export const MtGenericContainer = (props) => {
    return(
        <View style={NonMemberStyles.mainbox}>
        <View style={NonMemberStyles.Logobox}>
           {props.avatarBox}
        </View>
        <View style={NonMemberStyles.boxHeader}>
            {_.get(props,"coverImage")?props.coverImage?
        <Image style={NonMemberStyles.headerCoverImage} source={{uri:props.coverImage}} />
:<View>
<MtBodyText style={NonMemberStyles.headingTitle}>{props.headerTitle}</MtBodyText>
            <MtBodyText style={NonMemberStyles.headingSubTitle}>{props.subTitle}</MtBodyText>
            <Image style={NonMemberStyles.headerImage} source={require("../../../assets/truck.png")} />
     
</View>:<View>
<MtBodyText style={NonMemberStyles.headingTitle}>{props.headerTitle}</MtBodyText>
            <MtBodyText style={NonMemberStyles.headingSubTitle}>{props.subTitle}</MtBodyText>
            <Image style={NonMemberStyles.headerImage} source={require("../../../assets/truck.png")} />
     
</View>}

</View>
        {props.children}
    </View>

    )
   
};

